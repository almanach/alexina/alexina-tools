#!/usr/bin/env perl

$usage = <<USAGE;
Usage: cat [ilex_file] | morphoswitch.pl -r [ref_ilex_file] (-s [suffix]) > [new_ilex_file]
USAGE

$suffix = "";
$log = 1;
$trust_ref_file = 1;

while (1) {
  $_=shift;
  if (/^$/) {last;}
  elsif (/^--?r=(.*)$/) {$ref_ilex_file = $1;} elsif (/^--?r$/) {$ref_ilex_file = shift;}
  elsif (/^--?s=(.*)$/) {$suffix = $1;} elsif (/^--?s$/) {$suffix = shift;}
  elsif (/^--?ntr$/) {$trust_ref_file = 0;}
  elsif (/^--?i=(.*)$/) {$ignores = $1; for (split(/,/,$ignores)) {$ignore{$_} = 1}}
  elsif (/^--?i$/) {$ignores = shift; for (split(/,/,$ignores)) {$ignore{$_} = 1}}
  elsif (/^--?nl$/) {$log = 0;}
  elsif (/^--?ue$/) {$use_endings = 1;}
  elsif (/^--?lc$/) {$lowercase = 1;}
  elsif (/^--?h(?:elp)?$/) {
    die $usage;
  }
}

if ($suffix ne "") {$suffix = "___$suffix"}

open (REF, "<$ref_ilex_file") || die "Could not open $ref_ilex_file: $!";
while (<REF>) {
  chomp;
  s/(^|[^\\])#.*//;
  next if (/^\s*$/);
  next if (/^>/);
  /^(.*?)(?:___.*?)?\t(.*?)\t/ || die "Format error in $ref_ilex_file: $_";
  $ref_lemma2class{$1}{$2} = 1 unless (defined ($ignore{$2}));
}
close (REF);

while (<>) {
  chomp;
  s/(^|[^\\])#.*//;
  next if (/^\s*$/);
  if ($lowercase) {
    $_ = lc($_);
  }
  /^(.*?)(?:___(.*?))?\t(.*?)\t(.*)$/ || die "Format error in stdin: $_";
  $input_lemma2class{$1}{$3} = 1;
  $input{$1}{$3}{$4}{$2} = 1;
  $input_class_occ{$3}++;
}

for $l (keys %ref_lemma2class) {
  next unless defined ($input_lemma2class{$l});
  for $ref_c (keys %{$ref_lemma2class{$l}}) {
    for $input_c (keys %{$input_lemma2class{$l}}) {
      $input2ref_all{$input_c}{$ref_c}++;
      if ($use_endings) {
	$mod_l = "###$l";
	$mod_l =~ /(.)(.)(.)$/;
	$input_ending2ref_all{$input_c}{$1.$2.$3}{$ref_c}++;
	$input_ending2ref_all{$input_c}{$2.$3}{$ref_c}++;
	$input_ending2ref_all{$input_c}{$3}{$ref_c}++;
	$input_ending2ref_total{$input_c}{$1.$2.$3}++;
	$input_ending2ref_total{$input_c}{$2.$3}++;
	$input_ending2ref_total{$input_c}{$3}++;
      }
    }
  }
}

if ($use_endings) {
  for $input_c (keys %input_ending2ref_all) {
    for $ending (sort {length($b) <=> length($a)} keys %{$input_ending2ref_all{$input_c}}) {
      for $ref_c (keys %{$input_ending2ref_all{$input_c}{$ending}}) {
	if ($input_ending2ref_all{$input_c}{$ending}{$ref_c} > max($input_ending2ref_total{$input_c}{$ending} - 1 , 1)
	    || $input_ending2ref_all{$input_c}{$ending}{$ref_c} > 3) {
	  $input2ref_all{$input_c}{$ref_c} -= $input_ending2ref_all{$input_c}{$ending}{$ref_c};
#	  if ($input2ref_all{$input_c}{$ref_c} == 0) {
#	    delete($input2ref_all{$input_c}{$ref_c});
#	  }
	  $mod_ending = $ending;
	  while ($mod_ending =~ s/^.(.+)$/\1/) {
	    delete($input_ending2ref_all{$input_c}{$mod_ending}{$ref_c});
	  }
	} else {
	  delete($input_ending2ref_all{$input_c}{$ending}{$ref_c});
	}
      }
    }
  }
}

open (LOG, ">morphoswitch.log") || die "Could not open morphoswitch.log: $!";
for $input_c (sort {$input_class_occ{$b} <=> $input_class_occ{$a}} keys %input2ref_all) {
  $best_count = 0;
  $best_ref_c = "";
  $tmp = "";
  for $ref_c (keys %{$input2ref_all{$input_c}}) {
    $tmp .= "$ref_c ($input2ref_all{$input_c}{$ref_c})  ";
    if ($best_count < $input2ref_all{$input_c}{$ref_c}) {
      $best_ref_c = $ref_c;
      $best_count = $input2ref_all{$input_c}{$ref_c};
    }
  }
  $input2ref{$input_c} = $best_ref_c;
  print LOG "$input_c\t$best_ref_c\t$tmp\n";
  if ($use_endings) {
    for $ending (keys %{$input_ending2ref_all{$input_c}}) {
      $best_count = 0;
      $best_ref_c = "";
      $tmp = "";
      for $ref_c (keys %{$input_ending2ref_all{$input_c}{$ending}}) {
	$tmp .= "$ref_c (-$ending) ($input_ending2ref_all{$input_c}{$ending}{$ref_c})  ";
	if ($best_count < $input_ending2ref_all{$input_c}{$ending}{$ref_c}) {
	  $best_ref_c = $ref_c;
	  $best_count = $input_ending2ref_all{$input_c}{$ending}{$ref_c};
	}
      }
      if ($best_ref_c ne "" && $best_count > $input_ending2ref_total{$input_c}{$ending}*0.9) {
	$input_ending2ref{$input_c}{$ending} = $best_ref_c;
	print LOG "$input_c (-$ending)\t$best_ref_c\t$tmp\n";
      }
    }
  }
}
close LOG;

for $l (sort keys %input) {
  if (defined($ref_lemma2class{$l}) && $trust_ref_file) {
    for $input_c (sort keys %{$input{$l}}) {
      $l_with_suff = $l;
      if ($suffix ne "") {$l_with_suff .= $suffix."__".(++$l2curid{$l})}
      $printed_something = 0;
      for $ref_c (sort {$input2ref_all{$input_c}{$b} <=> $input2ref_all{$input_c}{$a}} keys %{$input2ref_all{$input_c}}) {
	if (defined($ref_lemma2class{$l}{$ref_c})) {
	  for $data (sort keys %{$input{$l}{$input_c}}) {
	    for $input_suff (sort keys %{$input{$l}{$input_c}{$data}}) {
	      print "${l_with_suff}___$input_suff\t$ref_c\t$data";
	      print "\t($input_c --[1]--> $ref_c)" if ($log == 1);
	      print "\n";
	    }
	  }
	  $printed_something = 1;
	  last;
	}
      }
      if ($printed_something == 0) {
	$ref_c = $input2ref{$input_c};
	for $data (sort keys %{$input{$l}{$input_c}}) {
	  for $input_suff (sort keys %{$input{$l}{$input_c}{$data}}) {
	    print "${l_with_suff}___$input_suff\t$ref_c\t$data\t($input_c --[2]--> $ref_c)\n";
	  }
	}
      }
    }
  } else {
    for $input_c (sort keys %{$input{$l}}) {
      $l_with_suff = $l;
      if ($suffix ne "") {$l_with_suff .= $suffix."__".(++$l2curid{$l})}
      if ($use_endings) {
	$mod_l = "###$l";
	$mod_l =~ /(.)(.)(.)$/;
	if (defined($input_ending2ref{$input_c}{$1.$2.$3})) {
	  $ref_c = $input_ending2ref{$input_c}{$1.$2.$3};
	} elsif (defined($input_ending2ref{$input_c}{$2.$3})) {
	  $ref_c = $input_ending2ref{$input_c}{$2.$3};
	} elsif (defined($input_ending2ref{$input_c}{$3})) {
	  $ref_c = $input_ending2ref{$input_c}{$3};
	} else {
	  $ref_c = $input2ref{$input_c} || "inv";
	}
      } else {
	$ref_c = $input2ref{$input_c} || "inv";
      }
      for $data (sort keys %{$input{$l}{$input_c}}) {
	for $input_suff (sort keys %{$input{$l}{$input_c}{$data}}) {
	  print "${l_with_suff}___$input_suff\t$ref_c\t$data\t($input_c --[3]--> $ref_c)\n";
	}
      }
    }
  }
}

sub max {
  my $a = shift;
  my $b = shift;
  return $a if $a > $b;
  return $b;
}
