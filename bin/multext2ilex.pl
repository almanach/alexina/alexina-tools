#!/usr/bin/env perl

use strict;
use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

my $many_files = 1;
my $correction_file = "";
my $file_prefix = "auto";
my $input_file = "";
while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-sf$/ || /^--?single-file$/i) {$many_files=0;}
    elsif (/^-fp=(.*)$/ || /^--?file-prefix=(.*)$/i) {$file_prefix=$1;}
    elsif (/^([^-].*)$/) {$input_file = $1;}
    else {die "Unknown option: $_"}
}

die "Please provide an input file as an argument" if ($input_file eq "");

my $line;
my ($form, $lemma, $cat, $tag, $trad, $semfeat, $semflag);
my %lemmaXtradXformXtag;
my %lemmaXtrad2semfeat;
my %cat2lemma;
my %cats;

print STDERR "  Initial pass ($input_file)\n";
open FILE, "<$input_file" || die "Could not open file '$input_file': $!";
binmode FILE, ":utf8";
while (<FILE>) {
  $line++;
  chomp;
  next if (/^\s+$/);
  if (/^([^\t]*)\t([^\t]*)\t(([^\t#]+)#[^\t\r]*)/) {
    $cat = $4;
    $cat =~ s/\//_/g;
  } elsif (/^([^\t]*)\t([^\t]*)\t([^\t:\+]+)(?:\+[\t:]*)?:[^\t\r]*/) {
    $cat = $3;
    $cat =~ s/\//_/g;
  } elsif (/^([^\t]*)\t([^\t]*)\t([^\t:\+]+)\+[^\t\r:]*([\r\t]|$)/) {
    $cat = $3;
    $cat =~ s/\//_/g;
  } elsif (/^([^\t]*)\t([^\t]*)\t([^\t])([^\t\r]*)/) {
    $cat = $3;
  } else {
    print STDERR "Format error line $line: $_\n";
    next;
  }
  $cats{$cat}++;
}
close FILE;
my $max_line = $line;

print STDERR "  Categories: ".join(", ", keys %cats)."\n";

unless ($many_files) {
  open(AF,">$file_prefix.af") || die "Could not open $file_prefix.af: $!";
  open(ALS,">$file_prefix.ilex") || die "Could not open $file_prefix.als: $!";
  close AF;
  close ALS;
}

for $cat (keys %cats) {
  my %lemma = ();
  my %lemmaXtradXformXtag = ();
  my $localcat;
  print STDERR "  Processing input file for cat '$cat'\n";
  open FILE, "<$input_file" || die "Could not open file '$input_file': $!";
  binmode FILE, ":utf8";
  while (<FILE>) {
    $line++;
    chomp;
    next if (/^\s+$/);
    if (/^([^\t]*)\t([^\t]*)\t(([^\t#]+)#[^\t\r]*)/) {
      $form = $1; $lemma = $2; $tag = $3; $localcat = $4;
      $tag =~ s/#/:/;
      $localcat =~ s/\//_/g;
      next if $localcat !~ /^$cat(\+|$)/;
    } elsif (/^([^\t]*)\t([^\t]*)\t([^\t:]+):([^\t\r]*)/) {
      $form = $1; $lemma = $2; $tag = $4; $localcat = $3;
      $localcat =~ s/\//_/g;
      next if $localcat !~ /^$cat(\+|$)/;
    } elsif (/^([^\t]*)\t([^\t]*)\t([^\t:\+]+\+[^\t\r:]+)([\r\t]|$)/) {
      $form = $1; $lemma = $2; $tag = ""; $localcat = $3;
      $localcat =~ s/\//_/g;
      next if $localcat !~ /^$cat(\+|$)/;
    } elsif (/^([^\t]*)\t([^\t]*)\t([^\t])([^\t\r]*)/) {
      next if $cat ne $3;
      $form = $1; $lemma = $2; $tag = $4;
    } else {
#      print STDERR "Format error line $line: $_\n";
      next;
    }
#    print STDERR "$form\t$lemma\t$cat\t$tag\n";
    if ($lemma =~ s/___(.*)$//) {
      $trad = $1;
    } else {
      $trad = "";
    }
    $form =~ s/_/ /g;
    $lemma =~ s/_/ /g;
    $form =~ s/"i/ï/;
    $form =~ s/\^o/ô/;
    $lemma =~ s/"i/ï/;
    $lemma =~ s/\^o/ô/;
    if ($localcat =~ s/\+(.*)$//) {
      $semfeat = "@".$1;
      $semfeat =~ s/\+/,\@/g;
    } else {
      $semfeat = "";
    }
    next if ($form =~ /^[0-9\+\-\=\.]+$/);
    if ($lemma eq "=") {$lemma = $form}
    $lemma{$lemma}++;
    $lemmaXtradXformXtag{$lemma}{$trad}{$form}{$tag} ++;
    if (defined($lemmaXtrad2semfeat{$lemma}{$trad})) {
      if ($lemmaXtrad2semfeat{$lemma}{$trad} ne $semfeat) {
	print STDERR "### WARNING: at least two semantic feature sets for lemma $lemma ($lemmaXtrad2semfeat{$lemma}{$trad} and $semfeat)\n";
      }
    } else {
      $lemmaXtrad2semfeat{$lemma}{$trad} = $semfeat;
    }
#    print STDERR "$form\t$lemma\___$cat\__1\t$tag\t$cat\n";
  }
  close FILE;
  
  if ($many_files) {
    my $catmod = $cat;
    $catmod =~ s/\|/_PIPE_/g;
    $catmod =~ s/\//_SLASH_/g;
    $catmod =~ s/:/_COLON_/g;
    $catmod =~ s/,/_COMMA_/g;
    $catmod =~ s/!/_EM_/g;
    $catmod =~ s/\?/_QM_/g;
    $catmod =~ s/{/_LCB_/g;
    $catmod =~ s/}/_RCB_/g;
    $catmod =~ s/\(/_LRB_/g;
    $catmod =~ s/\)/_RRB_/g;
    $catmod =~ s/\[/_LSB_/g;
    $catmod =~ s/\]/_RSB_/g;
    $catmod =~ s/\\/_BS_/g;
    $catmod =~ s/\|/_PI_/g;
    $catmod =~ s/\*/_STAR_/g;
    open(AF,">$catmod.af") || die "Could not open $cat.af: $!";
    open(ALS,">$catmod.ilex") || die "Could not open $cat.als: $!";
    binmode AF, ":utf8";
    binmode ALS, ":utf8";  
    for $lemma (sort keys %lemma) {
      print ALS "$lemma\t0\t100;Lemma;$cat;;cat=$cat;\%default\n";
      for $trad (sort keys %{$lemmaXtradXformXtag{$lemma}}) {
	for $form (sort keys %{$lemmaXtradXformXtag{$lemma}{$trad}}) {
	  for $tag (sort keys %{$lemmaXtradXformXtag{$lemma}{$trad}{$form}}) {
	    $semflag = $trad."+++".$lemmaXtrad2semfeat{$lemma}{$trad};
	    if ($semflag eq "+++") {
	      print AF "$form\t$lemma\t$tag\n";
	    } else {
	      print AF "$form\t${lemma}___$semflag\t$tag\n";
	    }
	  }
	}
      }
    }
    close (AF);
    close (ALS);
  } else {
    open(AF,">>$file_prefix.af") || die "Could not open $file_prefix.af: $!";
    open(ALS,">>$file_prefix.ilex") || die "Could not open $file_prefix.als: $!";
    binmode AF, ":utf8";
    binmode ALS, ":utf8";  
    for $lemma (sort keys %lemmaXtradXformXtag) {
      for $trad (sort keys %{$lemmaXtradXformXtag{$lemma}}) {
	$semflag = $trad."+++".$lemmaXtrad2semfeat{$lemma}{$trad};
	if ($semflag eq "+++") {
	  print ALS "$lemma\t0\t100;Lemma;$cat;;cat=$cat;\%default\n";
	} else {
	  print ALS "$lemma\___$trad\t0\t100;Lemma;$cat;;cat=$cat;\%default\n";
	}
	for $form (sort keys %{$lemmaXtradXformXtag{$lemma}{$trad}}) {
	  for $tag (sort keys %{$lemmaXtradXformXtag{$lemma}{$trad}{$form}}) {
	    print AF "$form\t$lemma\___$cat\__1\t$tag\t$cat\n";
	  }
	}
      }
    }
    close (AF);
    close (ALS);
  }
}
  
