#!/usr/bin/env perl

$Usage = "alexina_importer.pl input_file lang_name lexicon_name";

binmode STDOUT, ":utf8";
binmode STDIN, ":utf8";
binmode STDERR, ":utf8";

$input_file = shift;
$lang_name = shift;
$lexicon_name = shift;
$lefff_dir = shift || "lefff/trunk";

$orig_input_file = $input_file;

$encoding = ":utf8";
$recode = "";
$convert = "";

$tmp_dir = "alexina_importer_tmp_dir_$orig_input_file";
$tmp_dir =~ s/[^a-z_]+//g;


if (-e "$orig_input_file.aff" && -e "$orig_input_file.dic") {
  $format = "flt";
  $encoding = ":utf8";
  $recode = "";
  $convert = "";
  $input_file = $orig_input_file;
  $input_file =~ s/^.*\///;
  `rm -rf $tmp_dir`;
  `mkdir $tmp_dir`;
  $input_file = "$tmp_dir/$input_file";
  `cp $orig_input_file.aff $input_file.aff`;
  `cp $orig_input_file.dic $input_file.dic`;
  `perl aspell2flt.pl $input_file > $input_file.hs`;
  $input_file .= ".hs";
} elsif ($orig_input_file =~ /\.oxt$/) {
  `rm -rf $tmp_dir`;
  `mkdir $tmp_dir`;
  `cp $orig_input_file $tmp_dir/$orig_input_file`;
  `unzip $tmp_dir/$orig_input_file -d $tmp_dir`;
  $format = "flt";
  `mkdir $tmp_dir/zzzzzz`;
  `touch $tmp_dir/zzzzzz/zzzzzz.aff`;
  `touch $tmp_dir/zzzzzz.aff`;
  $input_file = `ls $tmp_dir/*.aff $tmp_dir/*/*.aff`;
  chomp($input_file);
  $input_file =~ s/(^|\n)[^\n]*zzzzzz[^\n]*//g;
  chomp($input_file);
  $input_file =~ s/\.aff$// || die "No .aff file found";
  print STDERR "perl aspell2flt.pl $input_file > $input_file.oxt.hs\n";
  `perl aspell2flt.pl $input_file > $input_file.oxt.hs`;
  `cp $input_file.oxt.hs $lexicon_name.oxt.hs`;
  $input_file .= ".oxt.hs";
  $encoding = ":utf8";
  $recode = "";
  $convert = "";
} elsif ($format ne "oxt") {
  $file_result = ` cat $input_file | head -20000 | tail -10000 | file -`;
  chomp($file_result);
  if ($file_result =~ /ISO-8859/ || $file_result =~ /(^| )ASCII/) {
    $recode = "| recode l1..u8";
    $encoding = ":encoding(iso-8859-1)";
  } elsif ($file_result =~ /Big-endian UTF-16/) {
    $recode = "| recode unicode..u8";
    $encoding = ":encoding(UTF-16BE)";
  } elsif ($file_result =~ /Little-endian UTF-16/) {
    $recode = "| recode unicode..u8";
    $encoding = ":encoding(UTF-16LE)";
  }
  print STDERR "Input encoding: $encoding\n";
  open (INPUT, "<$input_file") || die "$input_file unknown: $!";
  binmode INPUT, $encoding;
  while (<INPUT>) {
    chomp;
    $l++;
    $evidences{flt} += tr/\t/\t/;
    $evidences{ftl} += tr/\t/\t/;
    $evidences{flt} += 5 if (/\t=\t/);
    $evidences{ftl} += 5 if (/\t=\t/);
    if (/^(.*?)\t.*?\t\1$/) {
      $evidences{ftl}++;
    } elsif (/^(.*?)\t\1\t/) {
      $evidences{flt}++;
    } elsif (/\/[A-Za-z] [^ ]/) {
      $evidences{aspell}++;
    }
    $evidences{dela} += 5 if (/,\./ || /^(.*?),\1\./);
    last if ($l == 1000);
  }
  $format = (sort {$evidences{$b} <=> $evidences{$a}} keys %evidences)[0];
  print STDERR "Input format: $format\n";
}
if ($format eq "dela") {
  $convert = "| /usr/local/share/alexina-tools/dela2multext.pl";
} elsif ($format eq "ftl") {
  $convert = '| perl -pe "s/^(.*?)\\t(.*?)\\t(.*?)\$/\\1\\t\\3\\t\\2/"';
} elsif ($format eq "aspell") {
#  $convert = '| perl -pe "s/^(.*?)(?:\/(.*))? (.*)\$/\\3\\t\\1\\tx\\2/"';
  $convert = '| perl -pe "s/^(.*?)(?:\/(.*))? (.*)\$/\\3\\t\\1\\tx\\2/"';
}


if ($lexicon_name eq "") {$lexicon_name = $format."_".$lang_name}


die "$lexicon_name exists" if (-e $lexicon_name);

`mkdir $lexicon_name`;
`mkdir $lexicon_name/branches`;
`mkdir $lexicon_name/trunk`;
`mkdir $lexicon_name/tags`;
print STDERR "Converting to ilex/af format\n";
`cd $lexicon_name/trunk && cat ../../$input_file | grep -v "^\$" $recode $convert > $input_file.multext2ilex_input && /usr/local/share/alexina-tools/multext2ilex.pl $input_file.multext2ilex_input 2>multext2ilex.log && cd ../..`;
print STDERR "Building morphology tables and converting ilex/af into ilex/mf format\n";
`cd $lexicon_name/trunk && /usr/local/share/alexina-tools/af2morpho.pl -l=$lang_name -mp=3 2>af2morpho.log && cd ../..`;
#`rm -f $lexicon_name/trunk/*.af`;
`touch $lexicon_name/trunk/constructions`;
`touch $lexicon_name/trunk/templates`;
`touch $lexicon_name/trunk/amlgm`;
`cp $lefff_dir/alexina-makefile $lexicon_name/trunk/`;
`cat $lefff_dir/configure.ac | perl -pe "s/AC_INIT\\(lefff,[^,]*/AC_INIT($lexicon_name, 0.0.1/" > $lexicon_name/trunk/configure.ac`;
open (MAKEFILEAM, ">$lexicon_name/trunk/Makefile.am") || die "Could not open $lexicon_name/trunk/Makefile.am";
print MAKEFILEAM <<END;
 # Process this file with automake to produce Makefile.in

LANGUAGE = $lang_name
END
print MAKEFILEAM "LEXFILES =\t".join(" \\\n\t\t",(map {s/ilex/lex/; $_} split(/\n/,`cd $lexicon_name/trunk && ls *.ilex && cd ../..`)))."\n";
$lex_encoding = "UTF-8";
for $e ("L1","L2") {
  if (system("cat $input_file $recode | iconv -f UTF-8 -t $e >/dev/null 2>&1") == 0
      && system("cat $lexicon_name/trunk/morpho.$lang_name | iconv -f UTF-8 -t $e >/dev/null 2>&1") == 0) {
    $lex_encoding = $e;
    last;
  }
}
print STDERR "Extensional lexicon encoding: $lex_encoding\n";
print MAKEFILEAM <<END;
LEX_ENCODING = $lex_encoding

BUILT_SOURCES = alexina-makefile

include \$(srcdir)/alexina-makefile

alexina-makefile: \$(alexinatoolsdir)/alexina-makefile
	cp \$< \$@ 
END

#if ($orig_input_file =~ /\.oxt/) {
#  `rm -rf $tmp_dir`;
#}
