#!/usr/bin/env perl

#	cat *.lex $(srcdir)/amlgm | cut -f1 | grep ".[ ']." | perl -pe "s/(.) (.)/\1\_\2/g; s/(.) (.)/\1\_\2/g" | grep -vE '(^"*_|_"*$$)' > $$$$4;\
#	perl -pe "s/(?<=[^_])_(?=[^_])/ /g; s/\'(?=[^_])/\' /g;" $$$$4 > $$$$5;\
#	paste $$$$4 $$$$5  | perl -pe "chomp; s/^/\t/; s/\"//g;; s/([ \t])([^ \t]*[\+\&\?][^ \t]*)([ \t])/\1\'\2\'\3/g; s/([ \t])([^ \t]*[\&\+\?][^ \t]*)$$/\1\'\2\'/g; s/^\t//; s/$$/\n/" | sort -u > $@; \
#	rm $$$$*

$lang = "fr";

while (1) {
    $_=shift;
    last if /^$/;
    if (/^-l$/ || /^--?lang$/i) {$lang=shift;}
    else {die "unknown option : $_"}
}


while (<>) {
  chomp;
  s/\t.*//;
  next if ($lang =~ /^(fa|ckb)$/ && $_ !~ / /);
  next if ($lang !~ /^(fa|ckb)$/ && $_ !~ /.[' ]./);
  $cmpnd = $_;
  $cmpnd =~ s/'(?=[^_])/' /g; # d'__prep
  s/ /_/g;
  $_ = "\t".$_."\t".$cmpnd;
  s/\"//g;
  s/([ \t])([^ \t]*[\+\&\?][^ \t]*)([ \t])/\1\'\2\'\3/g;
  s/([ \t])([^ \t]*[\&\+\?][^ \t]*)$$/\1\'\2\'/g;
  s/^\t//;
  $hash{$_} = 1;
}

for (sort keys %hash) {
  print $_."\n";
}
