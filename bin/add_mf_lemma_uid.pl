#!/usr/bin/env perl

use utf8;
binmode STDIN, ":utf8";
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";

print STDERR "  Adding special forms... (preparing)\r";

my $entries_with_morphoclass_0 = ();
my $entry = ();

my $entries_with_morphoclass_0_file = shift;
if ($entries_with_morphoclass_0_file ne "") {
  open (EWM, "<$entries_with_morphoclass_0_file") || die "Could not open $entries_with_morphoclass_0_file: $!";
  binmode EWM, ":utf8";
  while (<EWM>) {
    chomp;
    /^(.*?[^_])(___.+?)\t/ || next;
    $entries_with_morphoclass_0{$1}{$2} = 1;
  }
  close (EWM);
}

my $l=0;
while (<>) {
  if (++$l % 1000 == 0) {
    print STDERR "  Adding special forms...$l             \r";
  }
  next if (/^(\#|$)/);
  chomp;
  s/^([^\t]+\t[^\t]+)$/\1\t/; # we add an empty column 3 if it is missing in the current line
  s/^([^\t]*\t[^\t]*\t[^\t]*).*/\1/;
  /^[^\t]+\t([^\t]+)\t/;
  $lemma=$1;
  if ($entries_with_morphoclass_0_file eq "" || $lemma=~/__[0-9]+$/ || $lemma=~/___/ || !defined($entries_with_morphoclass_0{$lemma})) {
    s/^([^\t]*\t[^\t]*)\t/$1\__1\t/ unless ($lemma=~/___.*__[0-9]+$/);
    s/(__[0-9]+)\t/___$1\t/ unless $lemma=~/___/;
    s/^([^\t]*\t[^\t]*)\t([^\t]*)\t([^\t]*)/\1\t\3\t\2/ ||
      s/^([^\t]*\t[^\t]*)/$1\tDefault/;
    print $_."\n";
  } else {
    $orig = $_;
    for $id (keys %{$entries_with_morphoclass_0{$lemma}}) {
      $_ = $orig;
      s/^([^\t]*\t[^\t]*)\t/$1$id\t/;
      s/^([^\t]*\t[^\t]*)\t([^\t]*)\t([^\t]*)/\1\t\3\t\2/ ||
	s/^([^\t]*\t[^\t]*)/$1\tDefault/;	
      print $_."\n";
    }
  }
}
print STDERR "  Adding special forms...done        \n";
