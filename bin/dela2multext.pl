#!/usr/bin/env perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

while (<>) {
    chomp;
    s/\r//g;
    s/^(.*),\./\1,\1./;
    s/\\([-\.])/\1/g;
    s/^([^,]+),/\1\t/g;
    s/\.([^.]+)$/\t\1/g;
#    s/\+[^\+:]+//g;
    s/\tADV(\S*)$/\tR\1/;
    s/\tDET(\S*)$/\tD\1/;
    s/\\-/-/g;
    next unless /./;
    print $_."\n";
}
