#!/usr/bin/env perl

use utf8;
binmode STDIN, ":utf8";
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";
use locale;

while (<>) {
  chomp;
  /^(.+?)\t(.+?)\t(.+?)\t/ || next;
  next unless ($2 eq "v" || $2 eq "adj" || $2 eq "adv" || $2 eq "nc");
  $form = $1;
  $cat = $2;
  $lemma = $3;
  next if $lemma =~ / /;
  next if $form =~ / /;
  $lemma_cat_form{$lemma}{$cat}{$form} = 1;
}

for $lemma (keys %lemma_cat_form) {
  for $cat (keys %{$lemma_cat_form{$lemma}}) {
    $output{"$lemma ".join(",",sort {$a <=> $b} keys %{$lemma_cat_form{$lemma}{$cat}})} = 1;
  }
}

for (sort {$a cmp $b} keys %output) {
  print $_."\n";
}
