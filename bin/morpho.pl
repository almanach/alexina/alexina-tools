<?xml version="1.0" encoding="UTF-8"?>
<description lang="pl">
  <letterclass name="rsoft" letters="ć dź ń ś ź"/>
  <letterclass name="vsoft" letters="ṕ ẃ ḿ ḃ ḟ"/>
  <letterclass name="soft" letters="ć dź ń ś ź l ṕ ẃ ḿ ḃ ḟ"/>
  <letterclass name="soft ij" letters="ć dź ń ś ź l ṕ ẃ ḿ ḃ ḟ i j"/>
  <letterclass name="hard or harden" letters="b p f w v m n ł t d r s z c cz dz dż rz sz ż ch h"/>
  <letterclass name="hard" letters="b p f w v m n ł t d r s z ch h"/>
  <letterclass name="hard not ch" letters="b p f w v m n ł t d r s z"/>
  <letterclass name="harden" letters="c cz dz dż rz sz ż"/>
  <letterclass name="velar" letters="k g h ch"/>
  <letterclass name="kg" letters="k g"/>
  <letterclass name="consonant" letters="b c ć cz d dz dź dż f g ch h j k l ł m n ń p r rz s ś sz t w v z ź ż ṕ ẃ ḿ ḃ ḟ"/>
  <letterclass name="consonant not kgcdz" letters="b ć cz d dź dż f ch h j l ł m n ń p r rz s ś sz t w v z ź ż ṕ ẃ ḿ ḃ ḟ"/>
  <letterclass name="consonant not kghchszż" letters="b c ć cz d dz dź dż f j l ł m n ń p r rz s ś t w v z ź ṕ ẃ ḿ ḃ ḟ"/>
  <letterclass name="dgkłrt" letters="d g k ł r t"/>
  <letterclass name="dźdzclrzć" letters="dź dz c l rz ć"/>
  <letterclass name="vowel" letters="a ą e ę i y o ó u"/>
  <letterclass name="vowel not i" letters="a ą e ę y o ó u"/>
  <letterclass name="iě" letters="i ě"/>
  <letterclass name="ie" letters="i e"/>
  <letterclass name="no acute" letters="c dz n s z"/>
  <letterclass name="pwmbf" letters="p w m b f"/>
  <letterclass name="mdlłtrsn" letters="m d l ł t r s n"/>
  <letterclass name="ćdźń" letters="ć dź ń"/>
  <letterclass name="cdzn" letters="c dz n"/>
  <letterclass name="voiced" letters="b d dz dź dż g h j l ł r rz w v z ź ż ẃ ḿ ḃ r"/>
  <pairs l1="no acute" l2="rsoft">
    <pair l1="c" l2="ć"/>
    <pair l1="dz" l2="dź"/>
    <pair l1="n" l2="ń"/>
    <pair l1="s" l2="ś"/>
    <pair l1="z" l2="ź"/>
  </pairs>
  <pairs l1="pwmbf" l2="vsoft">
    <pair l1="p" l2="ṕ"/>
    <pair l1="w" l2="ẃ"/>
    <pair l1="m" l2="ḿ"/>
    <pair l1="b" l2="ḃ"/>
    <pair l1="f" l2="ḟ"/>
  </pairs>
  <pairs l1="cdzn" l2="ćdźń">
    <pair l1="c" l2="ć"/>
    <pair l1="dz" l2="dź"/>
    <pair l1="n" l2="ń"/>
  </pairs>
  <pairs l1="iě" l2="ie">
    <pair l1="i" l2="i"/>
    <pair l1="ě" l2="e"/>
  </pairs>
  <pairs l1="dgkłrt" l2="dźdzclrzć">
    <pair l1="d" l2="dź"/>
    <pair l1="g" l2="dz"/>
    <pair l1="k" l2="c"/>
    <pair l1="ł" l2="l"/>
    <pair l1="r" l2="rz"/>
    <pair l1="t" l2="ć"/>
  </pairs>

  <translitteration source="ch" target="χ"/>
  <translitteration source="cz" target="č"/>
  <translitteration source="dz" target="ǳ"/>
  <translitteration source="dź" target="ď"/>
  <translitteration source="dż" target="ǆ"/>
  <translitteration source="rz" target="ř"/>
  <translitteration source="sz" target="š"/>

  <!-- fusions -->

  <!-- pour les comparatifs -->
  <fusion source="ek_sz" target="_sz" final="+"/>
  <fusion source="ok_sz" target="_sz" final="+"/>
  <fusion source="g_sz" target="ż_sz" final="+"/>
  <fusion source="ł_sz" target="l_sz" final="+"/>
  <fusion source="n_sz" target="ń_sz" final="+"/>
  <fusion source="z_sz" target="ż_sz" final="+"/>
  <fusion source="c_sz" target="t_sz" final="+"/>

  <fusion source="a[:consonant:]+_~ˇej$" target="e[:consonant:]+_ˇej$" final="+"/>
  <fusion source="ą[:consonant:]+_~ˇej$" target="ę[:consonant:]+_ˇej$" final="+"/>
  <fusion source="o[:consonant:]+_~ˇej$" target="e[:consonant:]+_ˇej$" final="+"/>
  <fusion source="e[:consonant:]+_-ˇej$" target="a[:consonant:]+_ˇej$" final="+"/>
  <fusion source="ę[:consonant:]+_-ˇej$" target="ą[:consonant:]+_ˇej$" final="+"/>

  <fusion source="ek_ˇej$" target="_ˇej$" final="+"/>
  <fusion source="ok_ˇej$" target="_ˇej$" final="+"/>
  <fusion source="k_ˇej$" target="_ˇej$" final="+"/>
  <fusion source="g_ˇej$" target="ż_ěj$" final="+"/>
  <fusion source="s_ˇej$" target="ż_ěj$" final="+"/>
  <fusion source="t_ˇej$" target="c_ej$" final="+"/>
  <fusion source="d_ˇej$" target="dz_ej$" final="+"/>
  <fusion source="_ˇej$" target="_ěj$" final="+"/>

  <!-- alternances vocaliques a/e, o/e -->
  <fusion source="a[:consonant:]+_~ě" target="e[:consonant:]+_ě" final="+"/>
  <fusion source="o[:consonant:]+_~ě" target="e[:consonant:]+_ě" final="+"/>

  <fusion source="o[:consonant:]+_~'i$" target="e[:consonant:]+_'i$" final="+"/>
  <fusion source="o[:consonant:]+_~i$" target="e[:consonant:]+_i$" final="+"/>

  <fusion source="[:no acute:][:kg:]_'ě$" target="[:rsoft:][:kg:]_'ě$" final="+"/>
  <fusion source="g_'ě$" target="ż_ě$" final="+"/>
  <fusion source="k_'ě$" target="cz_ě$" final="+"/>
  <fusion source="dz_'ě$" target="ż_ě$" final="+"/>
  <fusion source="c_'ě$" target="cz_ě$" final="+"/>
  <fusion source="[:consonant not kgcdz:]_'ě$" target="[:consonant not kgcdz:]_ě$" final="+"/>

  <fusion source="k_'i$" target="c_y$" final="+"/>
  <fusion source="g_'i$" target="dz_y$" final="+"/>
  <fusion source="h_'i$" target="s_i$" final="+"/>
  <fusion source="ch_'i$" target="s_i$" final="+"/>
  <fusion source="sz_'i$" target="s_i$" final="+"/>
  <fusion source="ż_'i$" target="z_i$" final="+"/>
  <fusion source="_'i$" target="_i$" final="+"/>

  <!-- <fusion source="[:no acute:][:dgkłrt:]_[:iě:]" target="[:rsoft:][:dźdzclrzć:]_[:ie:]" final="+"/> -->
  <!-- <fusion source="[:no acute:][:no acute:]_[:iě:]" target="[:rsoft:][:rsoft:]_[:ie:]" final="+"/> -->
  <fusion source="st_[:iě:]" target="ść_[:ie:]" final="+"/>
  <fusion source="zd_[:iě:]" target="źdź_[:ie:]" final="+"/>
  <fusion source="zn_[:iě:]" target="źń_[:ie:]" final="+"/>
  <!-- <fusion source="zm_[:iě:]" target="źḿ_[:ie:]" final="+"/> -->
  <fusion source="sł_[:iě:]" target="śl_[:ie:]" final="+"/>
  <fusion source="sm_[:iě:]" target="śḿ_[:ie:]" final="+"/>
  <fusion source="sn_[:iě:]" target="śń_[:ie:]" final="+"/>
  <fusion source="[:dgkłrt:]_[:iě:]" target="[:dźdzclrzć:]_[:ie:]" final="+"/>
  <fusion source="[:dgkłrt:]_[:iě:]" target="[:dźdzclrzć:]_[:ie:]" final="+"/>
  <fusion source="[:no acute:]_[:iě:]" target="[:rsoft:]_[:ie:]" final="+"/>
  <fusion source="h_[:iě:]" target="sz_[:ie:]" final="+"/><!-- ż ? -->
  <fusion source="ch_[:iě:]" target="sz_[:ie:]" final="+"/>
  <fusion source="[:pwmbf:]_[:iě:]" target="[:vsoft:]_[:ie:]" final="+"/>
  <fusion source="[:soft:]_ě" target="[:soft:]_e" final="+"/>
  <fusion source="[:harden:]_ě" target="[:harden:]_e" final="+"/>
  <fusion source="j_ě" target="j_e" final="+"/>
  <!-- <fusion source="_ě" target="_ie"/> en principe, inutile vu les lignes précédentes -->

  <fusion source="[:kg:]_e" target="[:kg:]_ie" final="+"/>

  <fusion source="[:hard or harden:]_i" target="[:hard or harden:]_y"  final="+"/>
  <fusion source="[:soft:]_y" target="[:soft:]_i"  final="+"/>
  <fusion source="[:kg:]_y" target="[:kg:]_i"  final="+"/>
  <fusion source="[:vowel:]_y" target="[:vowel:]_i"  final="+"/>
  <fusion source="j_y" target="j_i"  final="+"/>

  <fusion source="[:vowel:]j_i$" target="[:vowel:]_i$"  final="+"/><!-- pl:gen:f type aleja -->

  <fusion source="[:rsoft:]_i" target="[:no acute:]_i" final="+"/>
  <fusion source="[:rsoft:]_[:vowel not i:]" target="[:no acute:]_i[:vowel not i:]" final="+"/>
  <fusion source="[:vsoft:]_Ъ$" target="[:pwmbf:]_$" final="+"/>
  <fusion source="[:vsoft:]_Ъ-$" target="[:pwmbf:]_-$" final="+"/>
  <fusion source="[:vsoft:]_Ъ=$" target="[:pwmbf:]_=$" final="+"/>
  <fusion source="[:vsoft:]_[:consonant:]" target="[:pwmbf:]_[:consonant:]" final="+"/>
  <fusion source="[:vsoft:]_i" target="[:pwmbf:]_i" final="+"/>
  <fusion source="[:vsoft:]_[:vowel not i:]" target="[:pwmbf:]_i[:vowel not i:]" final="+"/>

  <fusion source="o[:consonant:]+_-" target="ó[:consonant:]+_" final="+"/>
  <fusion source="e[:consonant:]+_-" target="a[:consonant:]+_" final="+"/>
  <fusion source="ę[:consonant:]+_-" target="ą[:consonant:]+_" final="+"/>

  <fusion source="[:harden:]st_=$" target="[:harden:]est_$" final="+"/>
  <fusion source="[:harden:]ść_=$" target="[:harden:]eść_$" final="+"/>
  <fusion source="[:kg:][:consonant:]_=$" target="[:kg:]ie[:consonant:]_$" final="+"/>
  <fusion source="h[:consonant:]_=$" target="he[:consonant:]_$" final="+"/>
  <fusion source="ch[:consonant:]_=$" target="che[:consonant:]_$" final="+"/>
  <fusion source="l[:consonant:]_=$" target="le[:consonant:]_$" final="+"/>
  <fusion source="j[:consonant:]_=$" target="je[:consonant:]_$" final="+"/>
  <fusion source="[:rsoft:][:consonant:]_=$" target="[:no acute:]ie[:consonant:]_$" final="+"/>
  <fusion source="[:vsoft:][:consonant:]_=$" target="[:pwmbf:]ie[:consonant:]_$" final="+"/>
  <fusion source="[:hard or harden:][:consonant:]_=$" target="[:hard or harden:]e[:consonant:]_$" final="+"/>

  <fusion source="[:vsoft:]_$" target="[:pwmbf:]_$" final="+"/>
  <fusion source="[:vsoft:][:consonant:]+_" target="[:pwmbf:][:consonant:]+_" final="+"/>
  <fusion source="[:vsoft:][:consonant:]+e[:consonant:]_" target="[:pwmbf:][:consonant:]+e[:consonant:]_" final="+" rev="-"/>
  <fusion source="[:vsoft:][:consonant:]+ie[:consonant:]_" target="[:pwmbf:][:consonant:]+ie[:consonant:]_" final="+" rev="-"/>


  <fusion source="dźn_\*" target="dn_\*" final="+"/>
  <fusion source="ćn_\*" target="tn_\*" final="+"/>
  <fusion source="ńn_\*" target="nn_\*" final="+"/>
  <fusion source="dch_\*" target="tch_\*" final="+"/>
  <fusion source="rzc_\*" target="rc_\*" final="+"/>
  <fusion source="rzł_\*" target="rł_\*" final="+"/><!-- orzeł -->
  <fusion source="czśc_\*" target="czc_\*" final="+"/>
  <fusion source="chrzsc_\*" target="chrzc_\*" final="+"/>
  <fusion source="dszcz_\*" target="dźdź_\*" final="+"/>
  <fusion source="rzl_\*" target="rl_\*" final="+"/>
  <fusion source="źł_\*" target="zł_\*" final="+"/>
  <fusion source="ćł_\*" target="tł_\*" final="+"/>

  <!-- <table name="tag_unknown" source="" rads=".*"> -->
  <!--     <form suffix="" tag="-"/> -->
  <!-- </table> -->
  <!-- INVARIABLE -->
  <table name="inv" rads="..*" except=".*[:vsoft:]" fast="-">
    <form suffix="" tag=""/>
  </table>
  <table name="pref" rads="..*-" fast="-">
    <form suffix="_" tag=""/>
  </table>
  <!-- NOUNS -->
  <table name="subst-m1" tag_suffix=":m1" rads="..*[^a]">
    <alt>
      <form suffix="" tag="sg:nom" except=".*[:vsoft:]" var="NS0" show="ten #"/>
      <form suffix="-" tag="sg:nom" rads="..*[oę][:voiced:][:consonant:]*" except=".*[:vsoft:]" var="NS0alt1" show="ten #"/>
      <form suffix="-" tag="sg:nom" rads="..*[oę]t[:consonant:]*" except=".*[:vsoft:]" var="NS0alt1" show="ten #"/><!-- obrót... -->
      <form suffix="=" tag="sg:nom" rads=".*[:consonant:][:consonant:]" except=".*[:vsoft:]" var="NS0alt2" show="ten #"/>
      <form suffix="Ъ" tag="sg:nom" rads="..*[:vsoft:]" var="NSd" show="ten #"/>
      <form suffix="Ъ-" tag="sg:nom" rads="..*[oę][ẃḃ]" var="NSdalt1" show="ten #"/>
      <form suffix="Ъ-" tag="sg:nom" rads="..*[oę][:voiced:][:consonant:]*[:vsoft:]" var="NSdalt1" show="ten #"/>
      <form suffix="Ъ=" tag="sg:nom" rads=".*[:consonant:][:vsoft:]" var="NSdalt2" show="ten #"/>
    </alt>
    <form like="sg:gen" tag="sg:acc"/>
    <form suffix="a" tag="sg:gen" except="(woł|bawoł)"/>
    <form suffix="u" tag="sg:gen" rads="(woł|bawoł)" show="(dla #)"/>
    <alt>
      <form suffix="owi" tag="sg:dat" var="Dowi" show="Dat: #"/>
      <form suffix="u" tag="sg:dat" var="Du" show="Dat: #"/>
    </alt>
    <form suffix="em" tag="sg:inst"/>
    <form suffix="ě" tag="sg:loc" rads="..*[:hard not ch:]" except="(syn|dom|pan|komitat|kwiat|las|.*jazd|obiad|ornat|popioł|powiat|przod|spat|sąsiad|wat|wiatr|.*świat)"/>
    <form suffix="~ě" tag="sg:loc" rads="(komitat|kwiat|las|.*jazd|obiad|ornat|popioł|powiat|przod|spat|sąsiad|wat|wiatr|.*świat)"/>
    <form suffix="u" tag="sg:loc" except="..*[:hard not ch:]"/>
    <form suffix="u" tag="sg:loc" rads="(syn|dom|pan|bor)"/>
    <alt>
      <form suffix="'ě" tag="sg:voc" rads="..*([:hard:]|[cdzkg])"  except="(komitat|kwiat|las|mat|.*jazd|obiad|ornat|popioł|powiat|przod|spat|sąsiad|wat|wiatr|.*świat|..*[hch])" var="V'ě" show="#!"/>
      <form suffix="~ě" tag="sg:voc" rads="(komitat|kwiat|las|mat|.*jazd|obiad|ornat|popioł|powiat|przod|spat|sąsiad|wat|wiatr|.*świat)" var="V'ě"  show="#!"/><!-- should be ~'ě, but it has no effect excpet requiring a level-5 rev-fusion -->
      <form suffix="u" tag="sg:voc" rads="..*([:soft:]|[:harden:]|[kghchjl])" var="Vu"  show="#!"/>
    </alt>
    <alt>
      <form suffix="owie" tag="pl:nom" var="NPowie" show="ci #"/>
      <form suffix="ě" tag="pl:nom" rads="..*([:harden:]|[:soft:]|[jl])" except="..*c" var="NPstd" show="ci #"/>
      <form suffix="y" tag="pl:nom" rads="..*c" var="NPstd" show="ci #"/>
      <form suffix="i" tag="pl:nom" rads="..*([:hard:]|[:kg:])" except="..*[hch]" var="NPstd" show="ci #"/>
      <form suffix="'i" tag="pl:nom" rads="..*[hch]" var="NPstd" show="ci #"/>
    </alt>
    <form like="pl:gen" tag="pl:acc"/>
    <alt>
      <form suffix="ów" tag="pl:gen" var="GPów" show="5 #"/>
      <form suffix="y" tag="pl:gen" rads="..*([:harden:]|[:soft:]|[jl])" var="GPi" show="5 #"/>
      <form suffix="" tag="pl:gen" rads="certains pays" var="GP0" show="5 #"/>
    </alt>
    <form suffix="om" tag="pl:dat"/>
    <form suffix="ami" tag="pl:inst"/>
    <form suffix="mi" tag="pl:inst" rads=".*[:vowel:]([:rsoft:]|l)"/>
    <form suffix="ach" tag="pl:loc" except="quelques pays 2"/>
    <form suffix="ěch" tag="pl:loc" rads="quelques pays 2"/>
    <form like="pl:nom" tag="pl:voc"/>
  </table>
  <table name="subst-m1a" tag_suffix=":m1" rads="..*([:consonant:]|i)">
    <like name="subst-m1" except="sg"/>
    <form suffix="a" tag="sg:nom" show="ten #"/>
    <form suffix="o" tag="sg:voc"/>
    <form suffix="ę" tag="sg:acc"/><!-- ą ? -->
    <form suffix="y" tag="sg:gen"/>
    <form like="sg:loc" tag="sg:dat"/>
    <form suffix="ą" tag="sg:inst"/>
    <form suffix="ě" tag="sg:loc" rads="..*([:hard:]|[:kg:])"/>
    <form suffix="y" tag="sg:loc" rads="..*([:soft ij:]|[:harden:])"/>
  </table>
  <table name="subst-m1anin" tag_suffix=":m1" rads="..*an">
    <form suffix="in" tag="sg:nom" show="ten #"/>
    <form suffix="ina" tag="sg:acc"/>
    <form suffix="ina" tag="sg:gen"/>
    <form suffix="inowi" tag="sg:dat"/>
    <form suffix="inem" tag="sg:inst"/>
    <form suffix="inie" tag="sg:loc"/>
    <form suffix="inie" tag="sg:voc"/>
    <form suffix="ie" tag="pl:nom"/>
    <form suffix="" tag="pl:acc"/>
    <form suffix="" tag="pl:gen"/>
    <form suffix="om" tag="pl:dat"/>
    <form suffix="ami" tag="pl:inst"/>
    <form suffix="ach" tag="pl:loc"/>
    <form suffix="ie" tag="pl:voc"/>
  </table>
  <table name="subst-m1mix" canonical_tag="sg:nom" tag_suffix=":m1" rads="...*" fast="-">
    <form suffix="y" tag="sg:nom" show="ten #"/>
    <form suffix="ego" tag="sg:acc"/>
    <form suffix="ego" tag="sg:gen" show="dla #"/>
    <form suffix="emu" tag="sg:dat"/>
    <form suffix="ym" tag="sg:loc"/>
    <form suffix="ym" tag="sg:inst"/>
    <form suffix="owie" tag="pl:nom" rads="..*([:harden:]|[:soft:]|j)" show="ci #"/>
    <form suffix="i" tag="pl:nom" rads="..*([:kg:]|[:hard:])" show="ci #"/>
    <form suffix="ych" tag="pl:acc"/>
    <form suffix="ym" tag="pl:dat"/>
    <form suffix="ych" tag="pl:gen"/>
    <form suffix="ymi" tag="pl:inst"/>
    <form suffix="ych" tag="pl:loc"/>
  </table>
  <table name="subst-m1amix" canonical_tag="sg:nom" tag_suffix=":m1" rads="(sędź|hraḃ|..*graḃ|..*[csdz]k)" fast="-">
    <like name="subst-m1" except="sg"/>
    <form suffix="a" tag="sg:nom" show="ten #"/>
    <form suffix="ego" tag="sg:acc"/>
    <form suffix="ego" tag="sg:gen" show="dla #"/>
    <form suffix="emu" tag="sg:dat"/>
    <form suffix="ym" tag="sg:loc"/>
    <form suffix="y" tag="sg:loc"/>
    <form suffix="ą" tag="sg:inst"/>
    <form suffix="owie" tag="pl:nom" show="ci #"/>
  </table>
  <table name="subst-m1plur" canonical_tag="pl:nom" tag_suffix=":m1" rads="...*" fast="-">
    <like name="subst-m1" except="sg"/>
  </table>
  <table name="subst-m2" tag_suffix=":m2" rads="...*">
    <like name="subst-m1"/>
    <alt>
      <form suffix="y" tag="pl:nom" rads="..*([:hard or harden:]|[:kg:])" var="NPy" show="te #"/>
      <form suffix="i" tag="pl:nom" rads="dźń" var="NPi" show="te #"/>
      <form suffix="i" tag="pl:nom" rads="..*([:hard:])" var="NPi" show="te #"/>
      <form suffix="ě" tag="pl:nom" rads="..*([:soft:]|l|j|[:hard or harden:])" var="NPstd" show="te #"/>
      <form suffix="e" tag="pl:nom" rads="..*(c|ans)" var="NPe" show="te #"/>
    </alt>
    <form like="pl:nom" tag="pl:acc"/>
    <form like="pl:nom" tag="pl:voc"/>
  </table>
  <table name="subst-m3" tag_suffix=":m3" rads="...*">
    <like name="subst-m2"/>
    <alt>
      <form suffix="a" tag="sg:gen" var="GaDowi" show="dla #"/>
      <form suffix="a" tag="sg:gen" var="GaDu" show="dla #"/>
      <form suffix="u" tag="sg:gen" var="Gu" show="dla #"/>
    </alt>
    <form like="sg:nom" tag="sg:acc"/>
    <alt>
      <form suffix="owi" tag="sg:dat" var="Gu" show="Dat: #"/>
      <form suffix="owi" tag="sg:dat" var="GaDowi" show="Dat: #"/>
      <form suffix="u" tag="sg:dat" var="GaDu" show="Dat: #"/>
    </alt>
  </table>
  <table name="subst-m1inv" tag_suffix=":m1" rads="..*" fast="-">
    <form suffix="" tag="sg:nom" show="ten # (inv)"/>
    <form suffix="" tag="sg:acc"/>
    <form suffix="" tag="sg:gen"/>
    <form suffix="" tag="sg:dat"/>
    <form suffix="" tag="sg:inst"/>
    <form suffix="" tag="sg:loc"/>
    <form suffix="" tag="sg:voc"/>
    <form suffix="" tag="pl:nom"/>
    <form suffix="" tag="pl:acc"/>
    <form suffix="" tag="pl:gen"/>
    <form suffix="" tag="pl:dat"/>
    <form suffix="" tag="pl:inst"/>
    <form suffix="" tag="pl:loc"/>
    <form suffix="" tag="pl:voc"/>
  </table>
  <table name="subst-m2inv" tag_suffix=":m2" rads="...*" fast="-">
    <like name="subst-m1inv"/>
  </table>
  <table name="subst-m3inv" tag_suffix=":m3" rads="...*" fast="-">
    <like name="subst-m2inv"/>
  </table>
  <table name="subst-n" tag_suffix=":n" rads="...*">
    <alt>
      <form suffix="e" tag="sg:nom" rads="..*([:soft:]|[:harden:]|[lj])" except="(gorąc|mol|płuc|radi|jaj)" var="e" show="to #"/>
      <form suffix="o" tag="sg:nom" rads="..*([:hard:]|[kg])" var="o" show="to #"/>
      <form suffix="o" tag="sg:nom" rads="(gorąc|mol|płuc|lic|radi|jaj)" var="spec" show="to #"/>
    </alt>
    <form like="sg:nom" tag="sg:acc"/>
    <form suffix="a" tag="sg:gen"/>
    <form suffix="u" tag="sg:dat"/>
    <form suffix="em" tag="sg:inst"/>
    <alt>
      <form suffix="ě" tag="sg:loc" rads="..*[:hard:]" except="(dobr|zł)" var="std" show="w/na/przy #"/>
      <form suffix="~ě" tag="sg:loc" rads="..*a[:consonant:]*[:hard:]" var="alt" show="w/na/przy #" req="+"/>
      <form suffix="u" tag="sg:loc" except="..*[:hard:]" var="std" show="w/na/przy #"/>
      <form suffix="u" tag="sg:loc" rads="(dobr|zł|państw)" var="std" show="w/na/przy #"/>
      <form suffix="u" tag="sg:loc" var="spec" show="w/na/przy #" req="+"/>
    </alt>
    <form like="sg:nom" tag="sg:voc"/>
    <form suffix="a" tag="pl:nom"/>
    <form like="pl:nom" tag="pl:voc"/>
    <form suffix="a" tag="pl:acc"/>
    <alt>
      <form suffix="" tag="pl:gen" var="GP0" show="5 #"/>
      <form suffix="-" tag="pl:gen" rads="..*[oę][:voiced:][:consonant:]*" var="GP0alt1" show="5 #"/>
      <form suffix="-" tag="pl:gen" rads="..*[oę]t[:consonant:]*" var="GP0alt1" show="5 #"/>
      <form suffix="=" tag="pl:gen" rads=".*[:consonant:][:consonant:]" var="GP0alt2" show="5 #"/>
      <form suffix="y" tag="pl:gen" rads="..*([:harden:]|[:soft:])" except=".*ń" var="GPi" show="5 #"/>
      <form suffix="y" tag="pl:gen" rads="(błoń|spodń)" var="GPi" show="5 #"/>
    </alt>
    <form suffix="om" tag="pl:dat"/>
    <form suffix="ami" tag="pl:inst"/>
    <form suffix="mi" tag="pl:inst" rads=".*[:vowel:]([:rsoft:]|l)"/>
    <form suffix="ach" tag="pl:loc"/>
  </table>
  <table name="subst-nplur" canonical_tag="pl:nom" tag_suffix=":n" rads="...*" fast="-">
    <alt>
      <form suffix="a" tag="pl:nom" var="n" show="te # (pl.tant.)"/>
      <form suffix="y" tag="pl:nom" rads="..*([:hard:]|[:kg:])" var="f" show="te # (pl.tant.)"/>
      <form suffix="e" tag="pl:nom" rads="..*([:soft ij:]|[:harden:]|[:vowel:])" var="f" show="te # (pl.tant.)"/>
    </alt>
    <form like="pl:nom" tag="pl:voc"/>
    <form like="pl:nom" tag="pl:acc"/>
    <alt>
      <form suffix="" tag="pl:gen" var="GP0" show="dużo #"/>
      <form suffix="-" tag="pl:gen" rads="..*[oę][:voiced:][:consonant:]*" var="GP0alt1" show="dużo #"/>
      <form suffix="-" tag="pl:gen" rads="..*[oę]t[:consonant:]*" var="GP0alt1" show="dużo #"/>
      <form suffix="=" tag="pl:gen" rads=".*[:consonant:][:consonant:]" var="GP0alt2" show="dużo #"/>
      <form suffix="y" tag="pl:gen" rads="..*([:harden:]|[:soft ij:]|[:vowel:])" var="GPi" show="dużo #"/>
      <form suffix="ów" tag="pl:gen" rads="...*" var="GPów" show="dużo #"/>
    </alt>
    <form suffix="om" tag="pl:dat"/>
    <form suffix="ami" tag="pl:inst"/>
    <form suffix="mi" tag="pl:inst" rads=".*[:vowel:]([:rsoft:]|l)"/>
    <form suffix="ach" tag="pl:loc"/>
  </table>
  <table name="subst-num" tag_suffix=":n" rads="...*">
    <like name="subst-n"/>
    <form suffix="um" tag="sg:nom" show="to #" req="+"/>
    <form like="sg:nom" tag="sg:acc"/>
    <form like="sg:nom" tag="sg:gen"/>
    <form like="sg:nom" tag="sg:dat"/>
    <form like="sg:nom" tag="sg:inst"/>
    <form like="sg:nom" tag="sg:loc"/>
    <form like="sg:nom" tag="sg:voc"/>
    <form suffix="ów" tag="pl:gen"/>
  </table>
  <table name="subst-nen" tag_suffix=":n" rads="..*([:soft:]|[:harden:])">
    <form suffix="ę" tag="sg:nom" show="to #"/>
    <form suffix="ę" tag="sg:acc"/>
    <form suffix="enia" tag="sg:gen" show="dla #"/>
    <form suffix="eniu" tag="sg:dat"/>
    <form suffix="eniem" tag="sg:inst"/>
    <form suffix="eniu" tag="sg:loc"/>
    <form suffix="ę" tag="sg:voc"/>
    <form suffix="ona" tag="pl:nom"/>
    <form suffix="ona" tag="pl:acc"/>
    <form suffix="on" tag="pl:gen"/>
    <form suffix="onom" tag="pl:dat"/>
    <form suffix="onami" tag="pl:inst"/>
    <form suffix="onach" tag="pl:loc"/>
    <form suffix="ona" tag="pl:voc"/>
  </table>
  <table name="subst-net" tag_suffix=":n" rads="..*([:soft:]|[:harden:])">
    <form suffix="ę" tag="sg:nom" show="to #"/>
    <form suffix="ę" tag="sg:acc"/>
    <form suffix="ęcia" tag="sg:gen" show="dla #"/>
    <form suffix="ęciu" tag="sg:dat"/>
    <form suffix="ęciem" tag="sg:inst"/>
    <form suffix="ęciu" tag="sg:loc"/>
    <form suffix="ę" tag="sg:voc"/>
    <form suffix="ęta" tag="pl:nom"/>
    <form suffix="ęta" tag="pl:acc"/>
    <form suffix="ąt" tag="pl:gen"/>
    <form suffix="ętom" tag="pl:dat"/>
    <form suffix="ętami" tag="pl:inst"/>
    <form suffix="ętach" tag="pl:loc"/>
    <form suffix="ęta" tag="pl:voc"/>
  </table>
  <table name="subst-nmix" canonical_tag="sg:nom" tag_suffix=":n" rads="..*[:consonant:](n|ow)" fast="-">
    <like name="subst-m1mix"/>
    <form suffix="e" tag="sg:nom" show="to #"/>
    <form suffix="e" tag="sg:acc"/>
    <form suffix="e" tag="pl:nom"/>
    <form suffix="e" tag="pl:acc"/>
  </table>
  <table name="subst-ninv" tag_suffix=":n" rads="..*" fast="-">
    <like name="subst-m1inv"/>
  </table>
  <table name="subst-fv" tag_suffix=":f" rads="..*[^a]">
    <alt>
      <form suffix="a" tag="sg:nom" var="a" show="ta #"/>
      <form suffix="i" tag="sg:nom" rads="..*([:rsoft:]|[:kg:]|l)" var="i" show="ta #" req="+"/>
    </alt>
    <alt>
      <form suffix="o" tag="sg:voc" var="a"/>
      <form suffix="i" tag="sg:voc" var="i"/>
    </alt>
    <form suffix="ę" tag="sg:acc"/><!-- ą ? -->
    <form suffix="y" tag="sg:gen"/>
    <form like="sg:loc" tag="sg:dat"/>
    <form suffix="ą" tag="sg:inst"/>
    <form suffix="ě" tag="sg:loc" rads="..*([:hard:]|[:kg:])"/>
    <form suffix="y" tag="sg:loc" rads="..*([:soft ij:]|[:harden:]|[:vowel:])"/>
    <form suffix="y" tag="pl:nom" rads="..*([:hard:]|[:kg:])"/>
    <form suffix="e" tag="pl:nom" rads="..*([:soft ij:]|[:harden:]|[:vowel:])"/>
    <form like="pl:nom" tag="pl:voc"/>
    <form like="pl:nom" tag="pl:acc"/>
    <alt>
      <form suffix="" tag="pl:gen" var="GP0" show="5 #"/>
      <form suffix="-" tag="pl:gen" rads="..*[oę][:voiced:][:consonant:]*" var="GP0alt1" show="5 #"/>
      <form suffix="-" tag="pl:gen" rads="..*[oę]t[:consonant:]*" var="GP0alt1" show="5 #"/>
      <form suffix="=" tag="pl:gen" rads=".*[:consonant:][:consonant:]" var="GP0alt2" show="5 #"/>
      <form suffix="y" tag="pl:gen" rads="..*([:harden:]|[:soft ij:]|[:vowel:])" var="GPi" show="5 #"/>
    </alt>
    <form suffix="om" tag="pl:dat"/>
    <form suffix="ami" tag="pl:inst"/>
    <form suffix="mi" tag="pl:inst" rads=".*[:vowel:]([:rsoft:]|l)"/>
    <form suffix="ach" tag="pl:loc"/>
  </table>
  <table name="subst-fc" tag_suffix=":f" rads="..*([:harden:]|[:soft:]|[jl])">
    <like name="subst-fv"/>
    <alt>
      <form suffix="" tag="sg:nom" except=".*[:vsoft:]" var="NS0" show="ta #"/>
      <form suffix="-" tag="sg:nom" rads="..*[oę][:voiced:][:consonant:]*" except=".*[:vsoft:]" var="NS0alt1" show="ta #"/>
      <form suffix="-" tag="sg:nom" rads="..*[oę]t[:consonant:]*" except=".*[:vsoft:]" var="NS0alt1" show="ta #"/>
      <form suffix="=" tag="sg:nom" rads=".*[:consonant:][:consonant:]" except=".*[:vsoft:]" var="NS0alt2" show="ta #"/>
      <form suffix="Ъ" tag="sg:nom" rads="..*[:vsoft:]" var="NSd" show="ta #"/>
      <form suffix="Ъ-" tag="sg:nom" rads="..*[oę][ẃḃ]" var="NSdalt1" show="ta #"/>
      <form suffix="Ъ-" tag="sg:nom" rads="..*[oę][:voiced:][:consonant:]*[:vsoft:]" var="NSdalt1" show="ta #"/>
      <form suffix="Ъ=" tag="sg:nom" rads=".*[:consonant:][:vsoft:]" var="NSdalt2" show="ta #"/>
    </alt>
    <form suffix="y" tag="sg:voc"/>
    <form like="sg:nom" tag="sg:acc"/>
    <alt>
      <form suffix="e" tag="pl:nom" except="..*ść" var="NPe" show="te #"/>
      <form suffix="y" tag="pl:nom" var="NPi" show="te #"/><!-- except=".*[:consonant:]ẃ" -->
      <!-- <form suffix="i" tag="pl:nom" rads="brẃ" var="NPi"/> -->
    </alt>
    <form like="pl:nom" tag="pl:voc"/>
    <form like="pl:nom" tag="pl:acc"/>
    <form suffix="y" tag="pl:gen"/>
  </table>
  <table name="subst-fmix" canonical_tag="sg:nom" tag_suffix=":f" rads="(..*ow|..*[csdz]k|.*[żczw]n|.*it|księżn)" fast="-">
    <form suffix="a" tag="sg:nom" show="ta #"/>
    <form suffix="ą" tag="sg:acc"/>
    <form suffix="ej" tag="sg:gen" show="dla #"/>
    <form suffix="ej" tag="sg:dat"/>
    <form suffix="ej" tag="sg:loc"/>
    <form suffix="ą" tag="sg:inst"/>
    <form suffix="e" tag="pl:nom"/>
    <form suffix="e" tag="pl:acc"/>
    <form suffix="ym" tag="pl:dat"/>
    <form suffix="ych" tag="pl:gen"/>
    <form suffix="ymi" tag="pl:inst"/>
    <form suffix="ych" tag="pl:loc"/>
  </table>
  <table name="subst-finv" tag_suffix=":f" rads="..*" fast="-">
    <like name="subst-m1inv"/>
  </table>
  <!-- ADJECTIVES -->
  <table name="adj" canonical_tag="sg:nom:m1" tag_suffix=":pos" rads="..*[:consonant:]" except=".*(dch|rz[cłl]|[źć]ł|[ńdźć]n|[:vsoft:][:consonant:]+)">
    <form suffix="y" tag="sg:nom:m1" show="#,a,e"/>
    <form suffix="y" tag="sg:nom:m2"/>
    <form suffix="y" tag="sg:nom:m3"/>
    <form suffix="a" tag="sg:nom:f"/>
    <form suffix="e" tag="sg:nom:n"/>
    <form suffix="ego" tag="sg:acc:m1"/>
    <form suffix="ego" tag="sg:acc:m2"/>
    <form suffix="y" tag="sg:acc:m3"/>
    <form suffix="ą" tag="sg:acc:f"/>
    <form suffix="e" tag="sg:acc:n"/>
    <form suffix="ego" tag="sg:gen:m1"/>
    <form suffix="ego" tag="sg:gen:m2"/>
    <form suffix="ego" tag="sg:gen:m3"/>
    <form suffix="ej" tag="sg:gen:f"/>
    <form suffix="ego" tag="sg:gen:n"/>
    <form suffix="emu" tag="sg:dat:m1"/>
    <form suffix="emu" tag="sg:dat:m2"/>
    <form suffix="emu" tag="sg:dat:m3"/>
    <form suffix="ej" tag="sg:dat:f"/>
    <form suffix="emu" tag="sg:dat:n"/>
    <form suffix="ym" tag="sg:loc:m1"/>
    <form suffix="ym" tag="sg:loc:m2"/>
    <form suffix="ym" tag="sg:loc:m3"/>
    <form suffix="ej" tag="sg:loc:f"/>
    <form suffix="ym" tag="sg:loc:n"/>
    <form suffix="ym" tag="sg:inst:m1"/>
    <form suffix="ym" tag="sg:inst:m2"/>
    <form suffix="ym" tag="sg:inst:m3"/>
    <form suffix="ą" tag="sg:inst:f"/>
    <form suffix="ym" tag="sg:inst:n"/>
    <alt>
      <form suffix="'i" tag="pl:nom:m1" except="..*[cczdzdżrz]" var="std" show="NomPl m1: #"/>
      <form suffix="i" tag="pl:nom:m1" rads="..*[cczdzdżrz]" var="std" show="NomPl m1: #"/>
      <form suffix="i" tag="pl:nom:m1" rads="(nie|)(duż|boż)" var="std" show="NomPl m1: #"/>
      <form suffix="~'i" tag="pl:nom:m1" rads=".*o[:consonant:]+" except="...*[cczdzdżrz]" var="alt" show="NomPl m1: #" req="+"/>
      <form suffix="~i" tag="pl:nom:m1" rads=".*o[:consonant:]*[cczdzdżrz]" var="alt" show="NomPl m1: #" req="+"/>
    </alt>
    <form suffix="e" tag="pl:nom:m2"/>
    <form suffix="e" tag="pl:nom:m3"/>
    <form suffix="e" tag="pl:nom:n"/>
    <form suffix="e" tag="pl:nom:f"/>
    <form suffix="ych" tag="pl:acc:m1"/>
    <form suffix="e" tag="pl:acc:m2"/>
    <form suffix="e" tag="pl:acc:m3"/>
    <form suffix="e" tag="pl:acc:f"/>
    <form suffix="e" tag="pl:acc:n"/>
    <form suffix="ym" tag="pl:dat:m1"/>
    <form suffix="ym" tag="pl:dat:m2"/>
    <form suffix="ym" tag="pl:dat:m3"/>
    <form suffix="ym" tag="pl:dat:n"/>
    <form suffix="ym" tag="pl:dat:f"/>
    <form suffix="ych" tag="pl:gen:m1"/>
    <form suffix="ych" tag="pl:gen:m2"/>
    <form suffix="ych" tag="pl:gen:m3"/>
    <form suffix="ych" tag="pl:gen:n"/>
    <form suffix="ych" tag="pl:gen:f"/>
    <form suffix="ymi" tag="pl:inst:m1"/>
    <form suffix="ymi" tag="pl:inst:m2"/>
    <form suffix="ymi" tag="pl:inst:m3"/>
    <form suffix="ymi" tag="pl:inst:n"/>
    <form suffix="ymi" tag="pl:inst:f"/>
    <form suffix="ych" tag="pl:loc:m1"/>
    <form suffix="ych" tag="pl:loc:m2"/>
    <form suffix="ych" tag="pl:loc:m3"/>
    <form suffix="ych" tag="pl:loc:n"/>
    <form suffix="ych" tag="pl:loc:f"/>
    <!--   adj-preadj form -->
    <derivation suffix="o" table="adj-preadj"/>
    <!--   derivated adverb -->
    <derivation suffix="o" table="adv:o" rads=".*([kghch]|[:soft ij:]|[:harden:])"/>
    <derivation suffix="ě" table="adv:e" rads=".*([:hard:])"/>
    <!--   comparative -->
    <derivation suffix="ějszy" table="adj-comp" rads=".*(str|[ldkcz]n|pł|tw|dr|łt|st)"/>
    <derivation suffix="szy" table="adj-comp" except=".*(str|[ldkcz]n|pł|tw)"/>
    <!--   superlative -->
    <!--   <derivation prefix="naj" table="adj"/> -->
    <!--   superlatifs alternatifs -->
    <!--   <derivation prefix="prze" table="adj"/> -->
    <!--   <derivation prefix="przenaj" table="adj"/> -->
  </table>
  <table name="adj-comp" canonical_tag="sg:nom:m1" tag_suffix=":comp" rads="...*sz" except="naj.*">
    <like name="adj"/>
  </table>
  <table name="adj-sup" canonical_tag="sg:nom:m1" tag_suffix=":sup" rads="(naj...*)sz">
    <like name="adj"/>
  </table>
  <table name="adj-spec" canonical_tag="sg:nom:m1" tag_suffix=":pos" rads="(nie|)(ciekaw|gotow|łaskaw|syt|wesoł|żyw|świadom|zdrow|godźn|pełn|mocn|wińn|współwińn|dostojn|peẃn|kontent|wart|rad|siostrzyn|ojcow|bratow|daẃn|praw)">
    <like name="adj"/>
    <form suffix="y" tag="sg:nom:m1" except="(nie|)(kontent|wart|rad)" show="#"/>
    <form suffix="" tag="sg:nom:m1" except="(..*o[wł]|.*[:consonant:][:consonant:]|rad)" show="(predicative form: #)"/>
    <form suffix="" tag="sg:nom:m1" rads="(nie|)(kontent|wart|rad)" show=" # (predicative form; no -y/-i NomSg m1 form)"/>
    <form suffix="-" tag="sg:nom:m1" rads="..*o[wł]" show="(predicative form: #)"/>
    <form suffix="=" tag="sg:nom:m1" rads=".*[:consonant:][:consonant:]" except="(nie|)(kontent|wart)" show="(predicative form: #)"/>
    <form like="sg:nom:m1" tag="sg:nom:m2"/>
    <form like="sg:nom:m1" tag="sg:nom:m3"/>
    <form like="sg:nom:m3" tag="sg:acc:m3"/>
  </table>
  <table name="adj-inv" canonical_tag="sg:nom:m1" tag_suffix=":pos" fast="-">
    <form suffix="" tag="sg:nom:m1" show="# (inv)"/>
    <form suffix="" tag="sg:nom:m2"/>
    <form suffix="" tag="sg:nom:m3"/>
    <form suffix="" tag="sg:nom:f"/>
    <form suffix="" tag="sg:nom:n"/>
    <form suffix="" tag="sg:acc:m1"/>
    <form suffix="" tag="sg:acc:m2"/>
    <form suffix="" tag="sg:acc:m3"/>
    <form suffix="" tag="sg:acc:f"/>
    <form suffix="" tag="sg:acc:n"/>
    <form suffix="" tag="sg:gen:m1"/>
    <form suffix="" tag="sg:gen:m2"/>
    <form suffix="" tag="sg:gen:m3"/>
    <form suffix="" tag="sg:gen:f"/>
    <form suffix="" tag="sg:gen:n"/>
    <form suffix="" tag="sg:dat:m1"/>
    <form suffix="" tag="sg:dat:m2"/>
    <form suffix="" tag="sg:dat:m3"/>
    <form suffix="" tag="sg:dat:f"/>
    <form suffix="" tag="sg:dat:n"/>
    <form suffix="" tag="sg:loc:m1"/>
    <form suffix="" tag="sg:loc:m2"/>
    <form suffix="" tag="sg:loc:m3"/>
    <form suffix="" tag="sg:loc:f"/>
    <form suffix="" tag="sg:loc:n"/>
    <form suffix="" tag="sg:inst:m1"/>
    <form suffix="" tag="sg:inst:m2"/>
    <form suffix="" tag="sg:inst:m3"/>
    <form suffix="" tag="sg:inst:f"/>
    <form suffix="" tag="sg:inst:n"/>
    <form suffix="" tag="pl:nom:m1"/>
    <form suffix="" tag="pl:nom:m2"/>
    <form suffix="" tag="pl:nom:m3"/>
    <form suffix="" tag="pl:nom:n"/>
    <form suffix="" tag="pl:nom:f"/>
    <form suffix="" tag="pl:acc:m1"/>
    <form suffix="" tag="pl:acc:m2"/>
    <form suffix="" tag="pl:acc:m3"/>
    <form suffix="" tag="pl:acc:f"/>
    <form suffix="" tag="pl:acc:n"/>
    <form suffix="" tag="pl:dat:m1"/>
    <form suffix="" tag="pl:dat:m2"/>
    <form suffix="" tag="pl:dat:m3"/>
    <form suffix="" tag="pl:dat:n"/>
    <form suffix="" tag="pl:dat:f"/>
    <form suffix="" tag="pl:gen:m1"/>
    <form suffix="" tag="pl:gen:m2"/>
    <form suffix="" tag="pl:gen:m3"/>
    <form suffix="" tag="pl:gen:n"/>
    <form suffix="" tag="pl:gen:f"/>
    <form suffix="" tag="pl:inst:m1"/>
    <form suffix="" tag="pl:inst:m2"/>
    <form suffix="" tag="pl:inst:m3"/>
    <form suffix="" tag="pl:inst:n"/>
    <form suffix="" tag="pl:inst:f"/>
    <form suffix="" tag="pl:loc:m1"/>
    <form suffix="" tag="pl:loc:m2"/>
    <form suffix="" tag="pl:loc:m3"/>
    <form suffix="" tag="pl:loc:n"/>
    <form suffix="" tag="pl:loc:f"/>
  </table>
  <table name="adv" canonical_tag="pos" rads="..*[:consonant:]">
    <alt>
      <form suffix="e" tag="pos" var="e" show="# (adv)" req="+"/>
      <form suffix="o" tag="pos" var="o" show="# (adv)" req="+"/> <!-- pb with najczyściej and najgęściej, generated as najczęscej and najgęscej -->
    </alt>
    <form suffix="ˇej" tag="comp" except="(źl|gorąc|dobrz|lekk|mał|wąsk|śmiał|wesoł)" show="(naj)#"/>
    <form suffix="~ˇej" tag="comp" rads="(gorąc|wesoł|wąsk|śmiał)"/>
    <form prefix="naj" suffix="ˇej" tag="sup" except="(źl|gorąc|dobrz|lekk|mał|wąsk|śmiał|wesoł)"/>
    <form prefix="naj" suffix="~ˇej" tag="sup" rads="(gorąc|wesoł|wąsk|śmiał)"/>
  </table>
  <table name="adv-inv" canonical_tag="pos" rads="...*" fast="-">
    <form suffix="" tag="pos" var="" show="# (adv, no comp/sup)"/>
  </table>
  <table name="adj-preadj" rads="...*o">
    <form suffix="" tag="pos" var="" show="#-&lt;adj&gt;"/>
  </table>
</description>