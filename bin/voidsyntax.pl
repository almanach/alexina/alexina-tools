#!/usr/bin/env perl

while (<>) {
    chomp;
    next if (/<error/ || /\t\?\t/ || /^\t*$/);
    /^([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*)/ || die ("error:$_\n");
    $form=$1;
    $lemma=$2;
    $tag=$4;
#    print STDERR "$tag\n";
    $cat=$5;
    $cat=~s/^\@[^_]+_//;
    $catext="";
    if ($tag ne "") {$catext=":".$tag;}
    $tag=~s/:/, \@/g;
    $line="$form\t\t$cat$catext\t[pred=\'$lemma\', cat = $cat";
    if ($tag ne "") {
	$line.=", \@$tag]\t$lemma\t\t\t\n";
    } else {
	$line.="]\t$lemma\t\t$tag\t\n";
    }
    print $line;
}
