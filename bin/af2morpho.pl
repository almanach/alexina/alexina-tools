#!/usr/bin/env perl

use strict;
use utf8;

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

my $correction_file = "";
my $lang = "";
my $table_min_pop = 3;

while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-l=(.*)$/ || /^--?lang(?:uage)?=(.*)$/i) {$lang=$1;}
    elsif (/^-cf=(.*)$/ || /^--?correction-file=(.*)$/i) {$correction_file=$1;}
    elsif (/^-mp=(.*)$/) {$table_min_pop=$1;}
    else {die "Unknown option: $_"}
}
die unless ($lang ne "");

`mv morpho.$lang morpho.$lang.old`;
open (MORPHO, ">morpho.$lang") || die "Could not open morpho.tmp";
binmode MORPHO, ":utf8";

print MORPHO "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
print MORPHO "<description lang=\"$lang\">\n";
print MORPHO <<END;
  <!-- INVARIABLE -->
  <table name="inv" rads="..*" fast="-">
    <form suffix="" tag=""/>
  </table>
  <table name="pref" rads="..*" fast="-">
    <form suffix="_" tag=""/>
  </table>
END

for my $af_file (<*.af>) {

  my %lemmaXformXtag;

  open (AF,"<$af_file") || die "Could not open $af_file";
  binmode AF, ":utf8";

  my $cat = $af_file;
  $cat =~ s/\.af$//;
  $cat =~ s/\|/_PIPE_/g;
  $cat =~ s/\//_SLASH_/g;
  $cat =~ s/:/_COLON_/g;
  $cat =~ s/,/_COMMA_/g;
  $cat =~ s/!/_EM_/g;
  $cat =~ s/\?/_QM_/g;
  $cat =~ s/{/_LCB_/g;
  $cat =~ s/}/_RCB_/g;
  $cat =~ s/\(/_LRB_/g;
  $cat =~ s/\)/_RRB_/g;
  $cat =~ s/\[/_LSB_/g;
  $cat =~ s/\]/_RSB_/g;
  $cat =~ s/\\/_BS_/g;
  $cat =~ s/\|/_PI_/g;
  $cat =~ s/\*/_STAR_/g;

  open (ILEXTMP,">$cat.ilex") || die "Could not open $cat.ilex";
  open (MFTMP,">$cat.mf") || die "Could not open $cat.ilex";
  binmode ILEXTMP, ":utf8";
  binmode MFTMP, ":utf8";

  print STDERR "Reading $af_file...\r";

  my $l = 0;

  while (<AF>) {
    chomp;
#    if (++$l % 1000 == 0) {print STDERR "Reading $af_file\r";}
    if (/^([^\t]*?) *\t *([^\t]*?) *\t *([^\t]*?) *$/) {
      $lemmaXformXtag{$2}{$1}{$3}++;
    } elsif (/^([^\t]*?) *\t *([^\t_]*)[^\t]*? *\t *([^\t]*?) *\t *([^\t]*?) *$/) {
      $lemmaXformXtag{$2}{$1}{$4.$3}++;
    } else {
      die "Wrong input format";
    }
  }
  print STDERR "Reading $af_file... done        \n";

  for (keys %lemmaXformXtag) {
    $lemmaXformXtag{$_}{$_}{_HIDDEN_LEMMA_} = 1 unless defined $lemmaXformXtag{$_}{$_};
  }

  if ($correction_file ne "") {
    open (CORR,"<$correction_file") || die "Could not open $correction_file";
    binmode CORR, ":utf8";
    while (<CORR>) {
      chomp;
      /^([^\t]*)\t([^\t]*)\t$cat([^\t]*)$/ || next;
      $lemmaXformXtag{$2}{$1}{$3}++;
    }
  }

  print STDERR "Building morphology for $af_file.";

  my ($signature, $canonical_thing, $citform);
  my $stem;
  my %signatureXlemma;
  for my $lemma (keys %lemmaXformXtag) {
    $signature = "";
    $citform = $lemma;
    $citform =~ s/___.*$//;
    if (!defined($lemmaXformXtag{$lemma}{$citform})) {
      print STDERR "WARNING: Lemma '$lemma' doesn't have '$citform' as one of its inflected forms: lemma abandoned\n";
      $lemmaXformXtag{$lemma}{$citform}{__HIDDEN_LEMMA__}++;
#      next;
    }
    for my $tag (sort keys %{$lemmaXformXtag{$lemma}{$citform}}) {
      $signature .= "$citform $tag\t";
    }
    for my $form (sort keys %{$lemmaXformXtag{$lemma}}) {
      next if ($form eq $citform);
      for my $tag (sort keys %{$lemmaXformXtag{$lemma}{$form}}) {
	$signature .= "$form $tag\t";
      }
    }
    my $lemma_length = length ($citform);
    for my $stem_length (0..$lemma_length-1) {
      $stem_length = $lemma_length - $stem_length;
      my $possible_stem = quotemeta (substr ($citform, 0, $stem_length));
      if ($signature =~ /^${possible_stem}[^ \t]* [^ \t]*\t([^ \t]*?${possible_stem}[^ \t]* [^ \t]*\t)*$/) { #avec une étoile à la place de stem_length et sans le for, perl boucle sur certains exemples
	$stem = $possible_stem;
	$stem =~ s/\\-/-/g;
	$signature =~ s/(^|\t)([^ \t]*?)$stem/\1\2__/g;
	last;
      }
    }
    $signature =~ s/^([^\t]+)(\t|$)// || die "Error somewhere (signature = $signature)";
    $canonical_thing = $1;
    $signature = join("\t",sort split(/\t/,$signature));
    if ($signature ne "") {$signature = "\t$signature"}
    $signature = $canonical_thing.$signature;
    $signatureXlemma{$signature}{$lemma} = $stem;
  }

  print STDERR ".";

  my $tableid = 0;
  my $name;
  my $c;
  my $stem;
  my $std_syntactic_info = "100;Lemma;$cat;;cat=$cat;%default";
  my $my_syntactic_info;
  my $macros;
  for my $table (sort {scalar keys %{$signatureXlemma{$b}} <=> scalar keys %{$signatureXlemma{$a}} 
		     || $a cmp $b} keys %signatureXlemma) {
    if (scalar keys %{$signatureXlemma{$table}} >= $table_min_pop) {
      $tableid++;
      my $stemms_ending="--NULL--";
      if (scalar keys %{$signatureXlemma{$table}} > 6) {
	my $tmp;
	for my $lemma (keys %{$signatureXlemma{$table}}) {
	  $stem = $signatureXlemma{$signature}{$lemma};
	  print STDERR "$lemma\n";
	  if ($stemms_ending eq "--NULL--") {$stemms_ending = $stem}
	  else {
	    $tmp="";
	    while ($stem=~s/\\?([^ ])$//) {
	      $c = quotemeta($1);
	      $c =~ s/\\-/-/g;
	      if ($stemms_ending=~s/($c)$//) {
		$tmp = $1.$tmp;
	      } elsif ($stemms_ending=~s/([^\]])$//) {
		$tmp = "[".$1.$c."]".$tmp;
	      } elsif ($stemms_ending=~s/(\[[^\[\]]*$c[^\[\]]*\])$//) {
		$tmp = $1.$tmp;
	      } elsif ($stemms_ending=~s/(\[[^\[\]]{1,4})\]$//) {
		$tmp = $1.$c."]".$tmp;
	      } else {
		last;
	      }
	    }
	    $stemms_ending = $tmp;
	  }
	}
	$table =~ /__([^ ]*)/;
	$stemms_ending =~ s/$1$//;
	$stemms_ending =~ s/\[((?:[^\]]|\\\[)*)\-((?:[^\]]|\\\[)+)\]/\[\1\2-\]/g;
      } else {
	$stemms_ending = join("|",values %{$signatureXlemma{$table}});
	$table =~ /__([^ ]*)/;
	my $tmp = $1;
	$tmp =~ s/"/\\"/g;
	$stemms_ending =~ s/(^|\|)[^|]* /$1/g;
	while ($stemms_ending =~ s/(^|\|)([^|)]+)\|\2(\||$)/\1\2\3/) {}
	$stemms_ending = "($stemms_ending)" if ($stemms_ending =~ /\|/);
      }
      $name = $cat.$tableid;
      #    print MORPHO "Table $name (".(scalar keys %{$signatureXlemma{$table}})." members)\n";
      print MORPHO "  <table name=\"$name\" rads=\".*$stemms_ending\"";
      if (scalar keys %{$signatureXlemma{$table}} < 30) {
	print MORPHO " fast=\"-\"";
      }
      print MORPHO ">\n";
      print MORPHO "    <!-- ".(scalar keys %{$signatureXlemma{$table}})." members -->\n";
      for (split(/\t/,$table)) {
	next if /^$/;
	s/ /" tag="/;
	s/^/    <form prefix="/;
	s/__/" suffix="/;
	s/prefix=""//;
	s/$/"\/>/;
	print MORPHO "$_\n";
      }
      print MORPHO "  </table>\n";
      for my $fulllemma (sort keys %{$signatureXlemma{$table}}) {
	my $lemma = $fulllemma;
	$macros = "";
	$my_syntactic_info = $std_syntactic_info;
	if ($lemma =~ s/\+\+\+(.*)$//) {
	  $macros = $1;
	  $my_syntactic_info =~ s/(;\%default)/,$macros\1/ unless $macros eq "";
	}
	$lemma =~ s/___$//;
	print ILEXTMP "$lemma\t$name\t$my_syntactic_info\n";
      }
    } else {
      for my $fulllemma (sort keys %{$signatureXlemma{$table}}) {
	my $lemma = $fulllemma;
	$macros = "";
	$my_syntactic_info = $std_syntactic_info;
	if ($lemma =~ s/\+\+\+(.*)$//) {
	  $macros = $1;
	  $my_syntactic_info =~ s/(;\%default)/,$macros\1/ unless $macros eq "";
	}
	$lemma =~ s/___$//;
	print ILEXTMP "$lemma\t0\t$my_syntactic_info\n";
	for my $form (sort keys %{$lemmaXformXtag{$fulllemma}}) {
	  for my $tag (sort keys %{$lemmaXformXtag{$fulllemma}{$form}}) {
	    print MFTMP "$form\t$lemma\t$tag\n";
	  }
	}
      }
    }
  } 

  close MFTMP;
  close ILEXTMP;

  print STDERR "done\n";

}

print MORPHO <<END;
</description>
END
