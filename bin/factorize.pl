#!/usr/bin/env perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

$func=qr/(?:Suj|Objde|Obj[aà]|Obj|Obl2?|Att|Dloc|Loc)/o;
$prep=qr/(?:[a-zàéèá_]+-)/o;
$onereal=qr/(?:$prep?(?:sn|scompl|sinf|qcompl|sa|sadv)|cl[nagdl]|en|y|ser[ée]fl|ser[ée]c|epsilon)/o;
$real=qr/(?:$onereal(?:\|$onereal)*)/o;

$l = 0;
while (<>) {
  $l++;
  print STDERR "Reading input: $l\t(".(scalar keys %entries)." lemmas)\r";
  chomp;
  s/#.*//;
  s/\'[^<>\]]+</\'</;
  s/v\[/\[/;
  s/;/#/g;
  /^(.+?)\t+(.*)/;
  $lemma=$1;
  $data=$2;
#  $data =~ s/$f:\((.*?)\)/$f:\1|epsilon/g;
  %real=();
  for $f (qw/Suj Obj Objà Obja Objde Loc Dloc Att Obl Obl2 Obl3/) {
    if ($data=~s/(<.*)$f:([^,>]+)(.*>)/\1\3/) {
      $real{$f}=$2;
    }
  }
  $data=~s/<,+>/<>/;
  for $f (qw/Suj Obj Objà Obja Objde Loc Dloc Att Obl Obl2 Obl3/) {
    if ($real{$f} ne "") {
      $data=~s/>/,$f:$real{$f}>/;
    }
  }
  $data=~s/<,/</;
  if (!defined($seen{$lemma}{$data})) {
    $seen{$lemma}{$data}=1;
    if (!defined($entries{$lemma})) {
      $entries{$lemma}=$data;
    } else {
      $entries{$lemma}.=";".$data;
    }
  }
}
print STDERR "\n";


$l = 0;
for $lemma (sort keys %entries) {
  $l++;
#  print STDERR "Factorizing entries: $l / ".(scalar keys %entries)."\r";
  $_=$entries{$lemma};
  s/:a-/:à-/g;
  s/:aupres[ _]de/:auprès_de/g;
  #    while (s/(\[[^#]*$func):($real)([^#]*#)(.*)\1:($real)\3(.*)\1:($real)\3(.*)\1:($real)\3/\1:\2|\5|\7|\9\3\4\6\8/g) {print STDERR "utile\n"}
  #    while (s/(\[[^#]*$func):($real)([^#]*#)(.*)\1:($real)\3(.*)\1:($real)\3/\1:\2|\5|\7\3\4\6/g) {}
  #              1            2        3      4       5       6       7

  s/^100#Lemma#v##cat=v#\%default;(.)/\1/;
  while (s/(^|;)([^;]*#[^#]*$func):($real)([^#;]*#[^;#]*#)([^;]*)(;(?:.*?;)?)\2:($real)\4([^;]*)(;|$)/$1.$2.':'.sort_reals($3.'|'.$7).$4.$5.$8.$6/e) {}
  s/;;+/;/g;
  s/([^;])$/$1;/;
  %prestructures = ();
  for (split(/;/,$_)) {
#    next unless (/#</);
    $prestructures{normalize_restr($_)}++;
  }
  $_ = join(";",sort keys %prestructures);
  for $f (qw/Suj Obj Objà Obja Objde Loc Dloc Att Obl Obl2 Obl3/) {
    while (s/(^|;)([^;]*#[^#]*),($f):([^,>]+)([,>][^#]*#[^;]*)(;(?:.*?;)?)\2\5(;|$)/\1\2,\3:\(\4\)\5\6/g) {}	
    while (s/(^|;)([^;]*#[^#]*)([,>][^#]*#[^;]*)(;(?:.*?;)?)\2,($f):([^,>]+)\3(;|$)/\1\2,\5:\(\6\)\3\4/g) {}
  }
  s/;;+/;/g;
  s/([^;])$/$1;/;
  for (split(/;/,$_)) {
#    next unless (/#</);
    s/#/;/g;
    $structures{$lemma}{$_}++;
  }
}


for $lemma (sort keys %structures) {
  for (sort keys %{$structures{$lemma}}) {
    if (s/;\%(.*)$/;/) {
      %restr = ();
      for $s (split(/,?\%/,$1)) {
	$restr{$s} = 1 unless $s eq "";
      }
      print $lemma."\t".$_."\%".join(",\%",sort keys %restr)."\n";
    } else {
      print $lemma."\t".$_."\n";
    }
  }
}
print STDERR "\n";

sub sort_reals {
  $s = shift;
  %reals = ();
  for $s (split(/\|/,$s)) {
    $reals{$s} = 1 unless $s eq "";
  }
  return join("|",sort keys %reals);
}

sub normalize_restr {
  $s = shift;
  %restr = ();
  if ($s =~ s/#\%(.*)$/#/) {
    for $r (split(/,?\%/,$1)) {
      $restr{$r} = 1 unless $r eq "";
    }
    return $s."\%".join(",\%",sort keys %restr);
  } else {
    return $s;
  }
}
