//<head>//////////////////////////////////////////////////////////////
//
//	
//	Lexed version 4.0
//	Copyright (C) 1999 2000 2001 2002 2003.
//	Universit� Paris 7
//	INRIA Rocquencourt
//
//	Conception & programmation :
//	Lionel Cl�ment
//	e-mail : lionel.clement@linguist.jussieu.fr
//
//	vendredi 18 janvier 2002, 13h41
//
//
////////////////////////////////////////////////////////////////</head>////

#ifndef LEXEDTREE_H
#define LEXEDTREE_H

#include "lexedtypes.h"
#include "lexedinfo.h"

class Tree
{
private:
  Tree *son;
  Tree *brother;
  Info *info;
  char car;
  TYPEPTR adress;
  
public:
  Tree(Tree *son, Tree *brother, Info *info, char car);

  TYPEPTR get_adress();
  void set_adress(TYPEPTR adress);
  Tree *get_son();
  void set_son(Tree *son);
  Tree *get_brother();
  void set_brother(Tree *brother);
  Info *get_info();
  void set_info(Info *info);
  char get_car();
  void get_car(char car);

  void add(char *string, TYPEPTR offset);
  void set_index_fsa(TYPEPTR &index);
  void print_fsa(Tree *lexique_init, TYPEPTR *initial, FILE *out);
  void set_index_info(TYPEPTR &index);
  void print_info(FILE *out);
};

#endif
