#!/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";
use strict;
use XML::Simple;
use Data::Dumper;

my $xmlfile = shift || die;
die "'$xmlfile' not found" unless -e $xmlfile;


my $xml = XMLin($xmlfile, ForceContent => 1, ForceArray => ['syntacticargument', 'redistribution', 'lexeme', 'link', 'usage', 'feat', 'syntactic_property', 'lvf_entry', 'hasvariant', 'variantof']);
my $lexeme;
my ($id, $entry, $lemma, $example, $examples, $usage, $links, $link, $variants, $variant, $arg, $argid, $argreal, $argfunc, $redistr, $sprop, $sprops, $out, $realinitletter, $initletter, $anchor);

my $xmlbase = $xmlfile;
$xmlbase =~ s/\.aml$//i;
my %fh;
for ('a'..'z','other', 'index') {
  open $fh{$_}, ">$xmlbase-$_.html" || die $!;
  binmode $fh{$_}, ":utf8";
  print {$fh{$_}} <<END;
<html>
  <head>
    <title>Lefff</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <style type="text/css">
    <!--
    .tab { margin-left: 2em; }

    body { margin-top:30px; }
    .menuBar{
        height:30px;
        display:block;
        position:fixed;
        background-color:grey;
        width:100%;
        top:0;
        left:0;
        border-width:0px 0px 2px 0px;
        border-color:black;
        }
    .nav {
        float:left;
        margin-left:0px;
        margin-right:0px;
     }
    .nav ul {
        margin-top:2px;
        margin-left:0px;
    }
    .navitem {
        list-style:none;
        margin-right:1px;
        float:left;
        background-color:grey;
        color:white;
        }
    .navitem a {
        text-decoration:none;
        color:white;
        }
    .navitem a:hover {
        text-decoration:none;
        color:grey;
        }
    .navitem:hover {
        background-color:white;
        color:grey;
        cursor: pointer; cursor: hand;
        }
    .stab {
        margin-left:2em;
        font-size:90%;
        }
     p {
        margin-top:0.5em;
        margin-bottom:0.5em;
     }
     -->
     </style>
  </head>
  <body>

   <div class="menuBar">
        <div class="nav"> 
            <ul>
END
  print {$fh{$_}} "<li style='list-style:none;margin-left:0px;margin-right:10px;float:left;background-color:grey;color:white'><font size='4'><b>Verbes du Le<i>fff</i></b></li>";
  for my $o ('a'..'z','other','index') {
    if ($o eq $_) {
      print {$fh{$_}} "<li class='navitem'><font size='4'>&nbsp;$o&nbsp;</font></li>";
    } else {
      print {$fh{$_}} "<li class='navitem'><font size='4'><a href='$xmlbase-$o.html'>&nbsp;$o&nbsp;</a></font></li>";
    }
  }
  print {$fh{$_}} <<END;
            </ul> 
        </div>
   </div>

END
}


my %hash;
for my $i (0..$#{$xml->{lexicon}->{lexeme}}) {
  $lexeme = $xml->{lexicon}->{lexeme}->[$i];
#  print STDERR Dumper($lexeme)."\n";
  die unless defined ($lexeme->{id} && $lexeme->{id}->{content});
  die if (defined($hash{$lexeme->{id}->{content}}));
  $hash{$lexeme->{id}->{content}} = $lexeme;
}

for $id (sort {remove_diacritics($a) cmp remove_diacritics($b)} keys %hash) {
  $out = "";
  $lexeme = $hash{$id};
  $entry = $lexeme->{entry}->{content};
  chomp($entry);
  $lemma = $lexeme->{lemma}->{feat}->[0]->{val};
  chomp($lemma);
  $anchor = $id;
  $anchor =~ s/'/_APOS_/g;
  $id =~ s/__(\d+)$/<sub>$1<\/sub>/;
  chomp($id);
  $lemma =~ /^(.)/ || die;
  $realinitletter = $1;
  $initletter = remove_diacritics($realinitletter);
  next if $initletter eq "_";
  $initletter = "other" unless $initletter =~ /^[a-z]$/;
  $out .= "<div style='height:30px' id='$anchor'>&nbsp;</div>\n";
  $out .= "<p style='background-color:lightgrey;margin-top:0.08em;margin-bottom:0.08em'><font size='5'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$id: <b>$entry</b>";
  print {$fh{index}} "<div onclick=\"location.href='$xmlbase-$initletter.html#$anchor'\" onmouseover=\"this.style.cursor='hand';this.style.backgroundColor='lightgrey'\" onmouseout=\"this.style.cursor='none';this.style.backgroundColor='white'\">$id: <b>$entry</b>";
  if (defined($lexeme->{semid}) && defined($lexeme->{semid}->{content})) {
    next if $lexeme->{semid}->{content} eq "AUX";
    $out .= " ‘".$lexeme->{semid}->{content}."’";
    print {$fh{index}} " ‘".$lexeme->{semid}->{content}."’"
  }
  print {$fh{index}} "</div>\n";
  $out .= "</font></p>\n";
#  $out .= "Lemme: <b>$entry</b>\n";
  $examples = "";
  for my $j (0..$#{$lexeme->{usage}}) {
    $usage = $lexeme->{usage}->[$j];
    die unless defined ($usage->{content});
    $example = $usage->{content};
    chomp($example);
    $example =~ s/^(.)/uc($1)/e;
    $examples .= "<div class='stab'><i>$example</i>";
    if (defined ($usage->{source})) {
      $examples .= " <source style='font-size:xx-small;'>(".normalize_source($usage->{source}).")</source>";
    }
    $examples .= "</div>\n";
  }
#  <lvf_entry category="LITf" class="T1a" def="r/d académicien-n" domain="académiser" synt="T1108 P1000">On a~cet écrivain sans talent.Cet auteur ne peut s'a~.</lvf_entry>
  if (defined($lexeme->{lvf_entry})) {
    for my $j (0..$#{$lexeme->{lvf_entry}}) {
      die unless defined($lexeme->{lvf_entry}->[$j]->{content});
      for (split /\s*\.\s*/, $lexeme->{lvf_entry}->[$j]->{content}) {
	s/,([^ ])/, \1/g;
	if (/~/) {
	  if ($lemma eq "unifier" && /s~/) {$_ =~ s/s~/s'u~/g}
	  /$realinitletter\~/ || die "$lemma: $_";
	  my $ppart = $lemma;
	  $ppart =~ s/er$/é/ || die "$lemma";
	  my $P13s = $lemma;
	  $P13s =~ s/er$/e/ || die "$lemma";
	  my $P3p = $lemma;
	  $P3p =~ s/er$/ent/ || die "$lemma";
	  s/((?:ce|le|la|cette|cet|sa) [^ ]+(?: (?:de|en) [^ ]+)? (?:se |s')?)$realinitletter\~\s*/\1$P13s /gi;
	  s/(son [^aeioué][^ ]*(?: (?:de|en) [^ ]+)? (?:se |s')?)$realinitletter\~\s*/\1$P13s /gi;
	  s/((?:ces|les|ses) [^ ]+(?: (?:de|en) [^ ]+)? (?:se |s')?)$realinitletter\~\s*/\1$P3p /gi;
	  s/((?:ce|le|la|cet|sa) [^ ]+(?: (?:de|en) [^ ]+)? (?:se |s'))$realinitletter\~\s*/\1$P13s /gi;
	  s/(son [^aeioué][^ ]*(?: (?:de|en) [^ ]+)? (?:se |s'))$realinitletter\~\s*/\1$P13s /gi;
	  s/((?:ce|le) [^ ]+(?: (?:de|en) [^ ]+)? (?:s')?est )$realinitletter\~\s*/\1$ppart /gi;
	  s/(son [^aeioué][^ ]*(?: (?:de|en) [^ ]+)? (?:s')?est )$realinitletter\~\s*/\1$ppart /gi;
	  s/((?:cette|la|sa) [^ ]+(?: (?:de|en) [^ ]+)? (?:s')?est )$realinitletter\~\s*/\1${ppart}e /gi;
	  s/(on (?:se |s')?)$realinitletter\~\s*/\1$P13s /gi;
	  s/a $realinitletter\~\s*/\1 $ppart /gi;
	  s/((?:ce|le|la|cet|son) [^ ]+(?: (?:de|en) [^ ]+)? (?:se |s')[^ ]+e , est )$realinitletter\~\s*/\1$ppart /gi;
	  s/((?:cette|la|sa) [^ ]+(?: (?:de|en) [^ ]+)? (?:se |s')[^ ]+e , est )$realinitletter\~\s*/\1${ppart}e /gi;
	  s/(on est )$realinitletter\~\s*/\1$ppart /gi;
	  s/ ,/,/g;
	  if (s/$realinitletter\~\s*/$realinitletter\~ /) {
#	    print STDERR $_."\n";
	  }
	  s/^(.)/uc($1)/e;
	  $examples .= "<div class='stab'><i>$_</i>";
	  $examples .= " <source style='font-size:xx-small;'>(LVF)</source>";
	  $examples .= "</div>\n";
	}
      }
    }
  }
  if ($examples ne "") {
    $out .= "<p><b>Exemples&nbsp;:</b></p>\n";
    $out .= $examples;
  }
  $links = "";
  for my $j (0..$#{$lexeme->{link}}) {
    $link = $lexeme->{link}->[$j];
    die Dumper($link) unless defined ($link->{resource});
    $links .= "<ul>" if $links eq "";
    $links .= "<li>";
    if (defined ($link->{entry})) {
      if (defined($link->{loc})) {
	if ($link->{loc} =~ /:/) {
	  $links .= normalize_loc($link->{loc})."&nbsp;: ".normalize_entry($link->{entry});
	} else {
	  $links .= normalize_source($link->{resource})."<sub>".$link->{loc}."</sub>: ".normalize_entry($link->{entry})."<br>";
	}
      } else {
	$links .= normalize_source($link->{resource}).": ".normalize_entry($link->{entry})."<br>";
      }
      if (defined($link->{def})) {
	$links .= "<ul>";
	for (split /\s*\|\|\s*/, $link->{def}) {
	  $links .= "<li>$_</li>";
	}
	$links .= "</ul>";
      }
      $links .= "<br>";
    } elsif (defined ($link->{table})) {
      $links .= normalize_source($link->{resource})." (<i>Table ".normalize_entry($link->{table})."</i>)<br>";
    } else {
      die Dumper($link);
    }
    $links .= "</li>"
  }
  if ($links ne "") {
    $links .= "</ul>";
    $out .= "<p><b>Équivalents&nbsp;:</b> $links</p>\n";
  }
  if (defined $lexeme->{hasvariant}) {
    $variants = "";
    for my $j (0..$#{$lexeme->{hasvariant}}) {
      $variant = $lexeme->{hasvariant}->[$j];
      $variants .= ", " unless $variants eq "";
      $variants .= $variant->{lemma}."<sub>".$variant->{subid}."</sub>";
    }
    if ($variants ne "") {
      $variants .= "</ul>";
      $out .= "<p><b>Variante(s)&nbsp;:</b> $variants</p>\n";
    }
  }
  if (defined $lexeme->{variantof}) {
    $variants = "";
    for my $j (0..$#{$lexeme->{variantof}}) {
      $variant = $lexeme->{variantof}->[$j];
      $variants .= ", " unless $variants eq "";
      $variants .= $variant->{lemma}."<sub>".$variant->{subid}."</sub>";
    }
    if ($variants ne "") {
      $variants .= "</ul>";
      $out .= "<p><b>Variante de</b> $variants</p>\n";
    }
  }
  if (defined($lexeme->{subcategorizationframe})) {
    $out .= "<p><b>Arguments syntaxiques profonds&nbsp;:</b></p>\n";
    $out .= "<p class='tab'>\n";
    $out .= "<table>\n";
    $out .= "<tbody>\n";
    for $argid (sort {$a cmp $b} keys %{$lexeme->{subcategorizationframe}->{syntacticargument}}) {
      $arg = $lexeme->{subcategorizationframe}->{syntacticargument}->{$argid};
      die Dumper($arg) unless defined ($arg->{feat});
      for my $k (0..$#{$arg->{feat}}) {
	die unless defined($arg->{feat}->[$k]->{att});
	if ($arg->{feat}->[$k]->{att} =~ /syntacticfunction/i) {
	  $argfunc = $arg->{feat}->[$k]->{val};
	} elsif ($arg->{feat}->[$k]->{att} =~ /syntacticconstituent/i) {
	  $argreal = $arg->{feat}->[$k]->{val};
	  $argreal =~ s/\|/, /g;
	  $argreal =~ s/seréc/se<sub>réc<\/sub>/g;
	  $argreal =~ s/seréfl/se<sub>réfl<\/sub>/g;
	}
      }
      $out .= "<tr><td>$argid</td><td><b>$argfunc</b></td><td>";
      $out .= "ε, " if ($arg->{mandatory} == 0);
      $out .= "$argreal</td></tr>\n";
    }
    $out .= "</tbody>\n";
    $out .= "</table>\n";
    $out .= "</p>\n";
  }
  if (defined($lexeme->{redistributions})) {
    my $bool = 1;
    my $props = "";
    die unless defined($lexeme->{redistributions}->{redistribution});
    for my $j (0..$#{$lexeme->{redistributions}->{redistribution}}) {
      $redistr = $lexeme->{redistributions}->{redistribution}->[$j];
      die Dumper($redistr) unless defined ($redistr->{content});
      $props .= "<div class='stab'>".normalize_redistr($redistr->{content})."</div>\n";
    }
    if ($props ne "") {
      $out .= "<p><b>(Re)distributions&nbsp;:</b></p>\n";
      $out .= $props;
    }
  }
  if (defined($lexeme->{syntactic_properties})) {
    $sprops = "";
    die unless defined($lexeme->{syntactic_properties}->{syntactic_property});
    for my $j (0..$#{$lexeme->{syntactic_properties}->{syntactic_property}}) {
      $sprop = $lexeme->{syntactic_properties}->{syntactic_property}->[$j];
      die Dumper($sprop) unless defined ($sprop->{content});
      if ($sprop->{content} =~ /^cat\s*=/) {
      } elsif ($sprop->{content} =~ /^upos\s*=\s*(.+)$/) {
	$sprops .= "<div class='stab'>UPOS: $1</div>\n";
      } elsif ($sprop->{content} =~ /^[a-z]+type\s*=\s*(.+)$/) {
	$sprops .= "<div class='stab'>type: $1</div>\n";
      } elsif ($sprop->{content} =~ /^def(?:ined?)?\s*=\s*\+$/) {
	$sprops .= "<div class='stab'>Défini</div>\n";
      } elsif ($sprop->{content} =~ /^def(?:ined?)?\s*=\s*\-$/) {
	$sprops .= "<div class='stab'>Indéfini</div>\n";
      } elsif ($sprop->{content} =~ /^det\s*=\s*\+$/) {
	$sprops .= "<div class='stab'>Déterminant</div>\n";
      } elsif ($sprop->{content} =~ /^demonstrative\s*=\s*\+$/) {
	$sprops .= "<div class='stab'>Démonstratif</div>\n";
      } elsif ($sprop->{content} =~ /^refl\s*=\s*\+$/) {
	$sprops .= "<div class='stab'>Réfléchi</div>\n";
      } elsif ($sprop->{content} =~ /^qu\s*=\s*\+$/) {
	$sprops .= "<div class='stab'>Interrogatif</div>\n";
      } elsif ($sprop->{content} =~ /^case\s*=\s*(.+)$/) {
	$sprops .= "<div class='stab'>Cas: $1</div>\n";
      } else {
	$sprops .= "<div class='stab'>".normalize_sprop($sprop->{content})."</div>\n";
      }
    }
    if ($sprops ne "") {
      $out .= "<p><b>Propriétés syntaxiques&nbsp;:</b></p>\n";
      $out .= $sprops;
    }
  }
  print {$fh{$initletter}} $out;
}

for ('a'..'z','index') {
  print {$fh{$_}} "  </body>\n</html>\n";
  close $fh{$_};
}

sub normalize_source {
  my $s = shift;
  return "DicoValence" if ($s =~ /^dv$/i || $s =~ /^dicovalence$/);
  return "Lexique-Grammaire" if ($s =~ /^lg$/i);
  return "Lefff(v3)" if ($s =~ /^lefff$/);
  return "Tobler-Lommatzsch" if ($s =~ /^tl$/i);
  return "AND" if ($s =~ /^and$/i);
  return "DÉCT" if ($s =~ /^dect$/i);
  return "FEW" if ($s =~ /^few$/i);
  return "DMF" if ($s =~ /^dmf$/i);
  return $s;
}
sub normalize_loc {
  my $s = shift;
  $s =~ s/:/<sub>/;
  $s .= "</sub>";
  return $s;
}

sub normalize_redistr {
  my $s = shift;
  return "Participe passé passif employé comme adjectif" if $s eq "ppp_employé_comme_adj";
  $s =~ s/_/ /g;
  $s =~ s/^(.)(.*)$/uc($1).$2/e;
  $s =~ s/\b(se)\b/<i>\1<\/i>/i;
  return $s;
}
sub normalize_entry {
  my $s = shift;
  $s =~ s/__(\d+)$/($1)/;
  return $s;
}
sub normalize_sprop {
  my $s = shift;
  $s =~ s/^\@//;
  $s =~ s/__DV$//;
  if ($s =~ /^Ctrl([A-Z].+?)([A-Z].+?)$/) {
    return "Contrôle du $1 sur le $2";
  } elsif ($s =~ /^Att([A-Z].+?)$/) {
    return "Attribut du $1";
  } elsif ($s =~ /^lightverb\s*=\s*(.*)$/) {
    return "Verbe support $1";
  } elsif ($s =~ /^être_possible$/) {
    return "Auxiliaire: être ou avoir";
  } elsif ($s =~ /^être$/) {
    return "Auxiliaire: être";
  } elsif ($s =~ /^avoir$/) {
    return "Auxiliaire: avoir";
  } elsif ($s =~ /^pseudo-en_possible$/) {
    return "Pseudo <i>en</i> facultatif";
  } elsif ($s =~ /^pseudo-(.*)$/) {
    return "Pseudo <i>$1</i>";
  } elsif ($s =~ s/^(Att|Dloc|Loc|Obj|Objde|Objà|Obl2?|Suj)(Hum|Nonhum|Abs|Cnhum|Abs_OR_Cnhum|Abs_OR_Hum|Cnhum_OR_Hum|Abs_OR_Complex|Complex_OR_Hum|Cnhum_OR_Complex)$/\2/) {
    my $sfunc = $1;
    $sfunc = "Sujet" if ($sfunc eq "Suj");
    $sfunc = "Objet direct" if ($sfunc eq "Obj");
    $sfunc = "Objet indirect en <i>à</i>" if ($sfunc eq "Objà");
    $sfunc = "Objet indirect en <i>de</i>" if ($sfunc eq "Objde");
    $sfunc = "Argument locatif" if ($sfunc eq "Loc");
    $sfunc = "Argument délocatif" if ($sfunc eq "Dloc");
    $sfunc = "Attribut" if ($sfunc eq "Att");
    $sfunc = "Argument oblique" if ($sfunc eq "Obl");
    $sfunc = "Second argument oblique" if ($sfunc eq "Obl2");
    if ($s =~ /^Hum$/) {
      return "$sfunc humain";
    } elsif ($s =~ /^Nonhum$/) {
      return "$sfunc non humain (concret et/ou abstrait)";
    } elsif ($s =~ /^Abs$/) {
      return "$sfunc abstrait";
    } elsif ($s =~ /^Cnhum$/) {
      return "$sfunc concret non humain";
    } elsif ($s =~ /^Abs_OR_Cnhum$/) {
      return "$sfunc concret non humain ou abstrait";
    } elsif ($s =~ /^Abs_OR_Hum$/) {
      return "$sfunc humain ou abstrait";
    } elsif ($s =~ /^Cnhum_OR_Hum$/) {
      return "$sfunc concret (humain ou non)";
    } elsif ($s =~ /^Abs_OR_Complex$/) {
      return "$sfunc abstrait collectif";
    } elsif ($s =~ /^Cnhum_OR_Complex$/) {
      return "$sfunc concret non humain collectif";
    } elsif ($s =~ /^Complex_OR_Hum$/) {
      return "$sfunc humain collectif";
    } else {
      die $s;
    }
  } elsif ($s =~ /^SujHum$/) {
    return "Sujet humain";
  } elsif ($s =~ /^SujNonhum$/) {
    return "Sujet non humain";
  } elsif ($s =~ /^SujComplex$/) {
    return "Sujet complexe/pluriel";
  } elsif ($s =~ /^SujPlur$/) {
    return "Sujet pluriel";
  } elsif ($s =~ /^ObjHum$/) {
    return "Objet humain";
  } elsif ($s =~ /^ObjNonhum$/) {
    return "Objet non humain";
  } elsif ($s =~ /^ObjComplex$/) {
    return "Objet complexe/pluriel";
  } elsif ($s =~ /^ObjPlur$/) {
    return "Objet pluriel";
  } elsif ($s =~ /^SCompInd$/) {
    return "Complétive sujet à l'indicatif";
  } elsif ($s =~ /^SCompSubj$/) {
    return "Complétive sujet au subjonctif";
  } elsif ($s =~ /^SCompAlt$/) {
    return "Complétive sujet à mode alternant";
  } elsif ($s =~ /^DeCompInd$/) {
    return "Complétive indirecte en <i>de</i> à l'indicatif";
  } elsif ($s =~ /^DeCompSubj$/) {
    return "Complétive indirecte en <i>de</i> au subjonctif";
  } elsif ($s =~ /^DeCompAlt$/) {
    return "Complétive indirecte en <i>de</i> à mode alternant";
  } elsif ($s =~ /^ACompInd$/) {
    return "Complétive indirecte en <i>à</i> à l'indicatif";
  } elsif ($s =~ /^ACompSubj$/) {
    return "Complétive indirecte en <i>à</i> au subjonctif";
  } elsif ($s =~ /^ACompAlt$/) {
    return "Complétive indirecte en <i>à</i> à mode alternant";
  } elsif ($s =~ /^Compl?Ind$/) {
    return "Complétive directe à l'indicatif";
  } elsif ($s =~ /^CompSubj$/) {
    return "Complétive directe au subjonctif";
  } elsif ($s =~ /^CompAlt$/) {
    return "Complétive directe à mode alternant";
  } elsif ($s =~ /^pron$/) {
    return "Entrée pronominale";
  } elsif ($s =~ /^pron_possible$/) {
    return "Entrée facultativement pronominale";
  } elsif ($s =~ /^négatif$/) {
    return "Entrée négative";
  } elsif ($s =~ /^time$/) {
    return "Temporel";
  } elsif ($s =~ /^weekday$/) {
    return "Jour de la semaine";
  } else {
    die $s;
  }
  return $s;
}

sub remove_diacritics {
  my $s = shift;
  $s =~ s/[áàâäąãăå]/a/g;
  $s =~ s/[ćčç]/c/g;
  $s =~ s/[ď]/d/g;
  $s =~ s/[éèêëęě]/e/g;
  $s =~ s/[ğ]/g/g;
  $s =~ s/[ìíîĩĭıï]/i/g;
  $s =~ s/[ĺľł]/l/g;
  $s =~ s/[ńñň]/n/g;
  $s =~ s/[òóôõöø]/o/g;
  $s =~ s/[ŕř]/r/g;
  $s =~ s/[śšş]/s/g;
  $s =~ s/[ťţ]/t/g;
  $s =~ s/[ùúûũüǔ]/u/g;
  $s =~ s/[ỳýŷÿ]/y/g;
  $s =~ s/[źẑżž]/z/g;
  $s =~ s/[ÁÀÂÄĄÃĂÅ]/A/g;
  $s =~ s/[ĆČÇ]/C/g;
  $s =~ s/[Ď]/D/g;
  $s =~ s/[ÉÈÊËĘĚ]/E/g;
  $s =~ s/[Ğ]/G/g;
  $s =~ s/[ÌÍÎĨĬİÏ]/I/g;
  $s =~ s/[ĹĽŁ]/L/g;
  $s =~ s/[ŃÑŇ]/N/g;
  $s =~ s/[ÒÓÔÕÖØ]/O/g;
  $s =~ s/[ŔŘ]/R/g;
  $s =~ s/[ŚŠŞ]/S/g;
  $s =~ s/[ŤŢ]/T/g;
  $s =~ s/[ÙÚÛŨÜǓ]/U/g;
  $s =~ s/[ỲÝŶŸ]/Y/g;
  $s =~ s/[ŹẐŻŽ]/Z/g;
  return $s;
}
