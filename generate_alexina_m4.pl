#!/usr/bin/env perl

my $alexinadir = shift;

open LDF, "alexina_lexicons.list" || die $!;
while (<LDF>) {
  chomp;
  s/#.*//;
  /^(.*?):(.*)/ || next;
  $lex{$1} = $2;
}
close LDF;

open AM, ">alexina.m4" || die $!;

open ALEXINAM4IN, "alexina.m4.in" || die $!;
while (<ALEXINAM4IN>) {
  print AM $_;
}
close ALEXINAM4IN;

  print AM <<END;


# AC_PATH_ALEXINA
# Test for alexina installation (i.e., at least one alexina lexicon)
# defines alexinadir
AC_DEFUN([AC_PATH_ALEXINA], [
	AC_ARG_WITH(
		alexinadir,
		AC_HELP_STRING(
		       --with-alexinadir=DIR,
		       path where Alexina lexicon(s) is/are installed
		),
		alexinadir=\$withval,
		alexinadir=$alexinadir
	)

	AC_MSG_CHECKING(for Alexina lexicons hierarchy)

	# check validity
	if test -d "\$alexinadir"; then
		# expand to an absolute path
		alexinadir=`cd \$alexinadir && pwd`
		AC_MSG_RESULT(yes: \$alexinadir)
	else
		AC_MSG_RESULT(no)
	fi

	AC_SUBST(alexinadir)
])

END

for $lang (keys %lex) {
  $lex = $lex{$lang};
  $uclex = uc($lex);
  $lclex = lc($lex);
  print AM <<END;


# AC_PATH_$uclex
# Test for $lex installation
# defines ${lclex}dir
AC_DEFUN([AC_PATH_$uclex], [
	AC_ARG_WITH(
		${lclex}dir,
		AC_HELP_STRING(
		       --with-${lclex}dir=DIR,
		       path where ${lclex} is installed
		),
		${lclex}dir=\$withval,
		if test -d \$alexinadir/${lang}; then
			${lclex}dir=\$alexinadir/${lang}
		else
			if test -d \$datadir/${lclex}; then
				${lclex}dir=\$datadir/${lclex}
			else
				if test -d \$prefix/share/${lclex}; then
					${lclex}dir=\$prefix/share/${lclex}
				else
					if test -d /usr/local/share/${lclex}; then
						${lclex}dir=/usr/local/share/${lclex}
					else
						if test -d /usr/share/${lclex}; then
							${lclex}dir=/usr/share/${lclex}
						fi
					fi
				fi
			fi
		fi
	)

	AC_MSG_CHECKING(for $lex)

	# check validity
	if test -d "\$${lclex}dir"; then
		# expand to an absolute path
		${lclex}dir=`cd \$${lclex}dir && pwd`
		AC_MSG_RESULT(yes)
	else
		AC_MSG_RESULT(no)
	fi

	${lang}lexdir=\$${lclex}dir
	AC_SUBST(${lclex}dir)
	AC_SUBST(${lang}lexdir)
])
END
}
close AM;
