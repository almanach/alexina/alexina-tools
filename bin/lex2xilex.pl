#!/usr/bin/env perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

my $all=0;
my $noopt=0;
my $noobl=0;
my $noatt=0;

while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-all$/i) {$all=1;}
    elsif (/^-tags$/i) {$all=1; $tags = 1;}
    elsif (/^-frames$/i) {$frames=1;$all=1; $tags = 1;}
    elsif (/^-mframes$/i) {$alexinalike_frames=1;$marie=$1;$all=1; $tags = 1;}
    elsif (/^-aframes$/i) {$alexinalike_frames=1;$all=1; $tags = 1;}
    elsif (/^-noopt$/i) {$noopt=1;}
    elsif (/^-noobl$/i) {$noobl=1;}
    elsif (/^-noatt$/i) {$noatt=1;}
}

$linenumber=0;
while (<>) {
    if (++$linenumber % 1000 == 0) {print STDERR "Loading lexicon: line $linenumber\r";}
    chomp;
    next if (/^_error/);
    if (/SeMoyen/) {
      s/(pred=\')([aeiouéè])/$1s\'$2/ || s/(pred=\')/$1se /;
      s/^([aeiouéè])/s\'$1/ || s/^/se /;
    }
    $orig=$_;
    if ($all || (s/,\@Km?s?([ ,>\]])/$1/ && s/\]\t.*\%actif/\]/)) {
      s/:\(\([^\)]+\)/:\(\1/; # sécurité
      s/:\(([^\)]*)\)/:$1\|__NULL__/g;
      while (s/([A-Z][a-zà2]+:[^:,>\)]+)(?:__[A-Z])([\|,>\)])/\1\2/) {}
      $cat=$1;
      if (!/\t.*>/) {s/",/<>",/}
      s/>.*?;/>;/;
      s/\@pron,\@(impers|pers)/\@\1,\@pron/;
      s/;$/;\%actif/;
      if ($noobl) {
	s/,(Obl\d|Obl|Loc|Dloc):[^,>]*//g;
      }
      
      if ($tags) {
	/^[^\t]*?\t[^\t]*?\t[^\t]*?\t[^\t]*?\t[^\t]*?\t[^\t]*?\t([^\t]*?)(\t|$)/;
	$tag = $1;
	next unless ($tag =~ /^Km?s?$/ || $tag eq "W" || $tag eq "G" || $tag =~ /^P.*?3.*?s$/);
      }
      
      if ($noatt) {
	s/,(Att):[^,>]*//g;
      }
      
      if ($frames) {
	for $i (1,2) {
	  s/([\(:\|])cln([\)\|>,])/\1CLS\2/g;
	  s/([\(:\|])(?:cla|cld|en|y)([\)\|>,])/\1CLO\2/g;
#	  s/([\(:\|])(?:seréfl|seréc|serefl|serec)([\)\|>,])/\1CLR\2/g;
	  s/([\(:\|])(?:seréfl|seréc|serefl|serec)([\)\|>,])/\1N\2/g;
	  s/([\(:\|])sn([\)\|>,])/\1ADJWH\|NC\|NPP\|PRO\|PROREL\|PROWH\|DETWH\|ET\|ADJ\2/g;
	  s/([\(:\|])sa([\)\|>,])/\1ADJ\2/g;
	  s/([\(:\|])scompl([\)\|>,])/\1CS\|I\2/g;
	  s/([\(:\|])qcompl([\)\|>,])/\1CS\|ADVWH\|I\2/g;
	  s/([\(:\|])cla([\)\|>,])/\1CLO\2/g;
	  s/([\(:\|])sinf([\)\|>,])/\1VINF\|VPP\2/g;
	  if ($add_sub_pos) {
	    s/([\(:\|])(à)-sn([\)\|>,])/\1P\{\2\}\[NC\]\|P\{\2\}\[NPP\]\|PRO\|P\+PRO\|PROREL\|PROWH\3/g;
	    s/([\(:\|])(de)-sn([\)\|>,])/\1P\{\2\}\[NC\]\|P\{\2\}\[NPP\]\|PRO\|P\+PRO\|PROREL\|PROWH\3/g;
	    s/([\(:\|])(d[^e -,\|>][ -,\|>]*)-sn([\)\|>,])/\1P\{\2\}\[NC\]\|P\{\2\}\[NPP\]\|P\{\2\}\[PRO\]\3/g;
	    s/([\(:\|])([^àd -,\|>][^ -,\|>]*)-sn([\)\|>,])/\1P\{\2\}\[NC\]\|P\{\2\}\[NPP\]\|P\{\2\}\[PRO\]\3/g;
	    s/([\(:\|])([^ -,\|>]+)-sn([\)\|>,])/\1P\{\2\}\[NC\]\3/g;
	    s/([\(:\|])([^ -,\|>]+)-sa([\)\|>,])/\1P\{\2\}\[ADJ\]\3/g;
	    s/([\(:\|])([^ -,\|>]+)-sinf([\)\|>,])/\1P\{\2\}\[VINF\]\|P\{\2\}\[VPP\]\3/g;
	    s/([\(:\|])([^ -,\|>]+)-scompl([\)\|>,])/\1P\{\2\}\[CS\]\|P\{\2\}\[I\]\3/g;
	    s/([\(:\|])([^ -,\|>]+)-qcompl([\)\|>,])/\1P\{\2\}\[CS\]\|P\{\2\}\[ADVWH\]\|\|P\{\2\}\[I\]\3/g;
	  } else {
	    s/([\(:\|])(à)-sn([\)\|>,])/\1P\{\2\}\|PRO\{\2\}\|P\+PRO\{\2\}\|PROREL\{\2\}\|PROWH\{\2\}\3/g;
	    s/([\(:\|])(de)-sn([\)\|>,])/\1P\{\2\}\|PRO\{\2\}\|P\+PRO\{\2\}\|PROREL\{\2\}\|PROWH\{\2\}\3/g;
	    s/([\(:\|])(d[^e -,\|>][ -,\|>]*)-sn([\)\|>,])/\1P\{\2\}\3/g;
	    s/([\(:\|])([^àd -,\|>][^ -,\|>]*)-sn([\)\|>,])/\1P\{\2\}\3/g;
	    s/([\(:\|])([^ -,\|>]+)-sn([\)\|>,])/\1P\{\2\}\3/g;
	    s/([\(:\|])([^ -,\|>]+)-sa([\)\|>,])/\1P\{\2\}\3/g;
	    s/([\(:\|])([^ -,\|>]+)-sinf([\)\|>,])/\1P\{\2\}\3/g;
	    s/([\(:\|])([^ -,\|>]+)-scompl([\)\|>,])/\1P\{\2\}\3/g;
	    s/([\(:\|])([^ -,\|>]+)-qcompl([\)\|>,])/\1P\{\2\}\3/g;
	  }
	}
	if (/\%passif\t[^\t]*$/) {
	  s/<Suj:/<aux_pass:V|VS|VPP|VPR|VINF,Suj:/;
	}
	while (s/([\(:\|])([^,\|>]+)(\|[^,\|>]+)*?\|\2([\)\|>,])/\1\2\3\4/g) {}
      } elsif ($alexinalike_frames) {
	for $i (1,2) {
	  s/([\(:\|])cln([\)\|>,])/\1N\2/g;
	  s/([\(:\|])(?:cla|cld|en|y)([\)\|>,])/\1N\2/g;
#	  s/([\(:\|])(?:seréfl|seréc|serefl|serec)([\)\|>,])/\1CLR\2/g;
	  s/([\(:\|])(?:seréfl|seréc|serefl|serec)([\)\|>,])/\1N\2/g;
	  s/([\(:\|])sn([\)\|>,])/\1N\|ADJ\2/g;
	  s/([\(:\|])sa([\)\|>,])/\1ADJ\2/g;
	  s/([\(:\|])scompl([\)\|>,])/\1CS\2/g;
	  s/([\(:\|])qcompl([\)\|>,])/\1CS\2/g;
	  s/([\(:\|])cla([\)\|>,])/\1N\2/g;
	  s/([\(:\|])sinf([\)\|>,])/\1VINF\|VPP\2/g;
	  s/([\(:\|])(à)-sn([\)\|>,])/\1N\{\2\}\3/g;
	  s/([\(:\|])(de)-sn([\)\|>,])/\1N\{\2\}\3/g;
	  s/([\(:\|])(d[^e -,\|>][ -,\|>]*)-sn([\)\|>,])/\1N\{\2\}\3/g;
	  s/([\(:\|])([^àd -,\|>][^ -,\|>]*)-sn([\)\|>,])/\1N\{\2\}\3/g;
	  s/([\(:\|])([^ -,\|>]+)-sn([\)\|>,])/\1N\{\2\}\3/g;
	  s/([\(:\|])([^ -,\|>]+)-sa([\)\|>,])/\1ADJ\{\2\}\3/g;
	  s/([\(:\|])([^ -,\|>]+)-sinf([\)\|>,])/\1VINF\|VPP\{\2\}\3/g;
	  s/([\(:\|])([^ -,\|>]+)-scompl([\)\|>,])/\1CS\{\2\}\3/g;
	  s/([\(:\|])([^ -,\|>]+)-qcompl([\)\|>,])/\1CS\{\2\}\3/g;
	}
	if (/\%passif\t[^\t]*$/) {
	  s/<Suj:/<aux_pass:V|VPP|VPR|VINF,Suj:/;
	}
	while (s/([\(:\|])([^,\|>]+)((?:\|[^,\|>]+)*?)\|\2([\)\|>,])/\1\2\3\4/g) {}
      }
      #	# on supprime les macros qu'on ne sait pas traiter
      #	s/\@Ca/\@êa/;
      #	s/\@[^pêaNIi][^,>\] ]+//g;
      #	s/\@êa/\@Ca/;
      #	# on supprime aussi pour le moment les macros qui donnent l'auxiliaire
      #	s/\@(être|avoir),//;
      #	s/\s*,\s*\@W//g;
      s/\s*;\s*$//;
      push(@lexicon,$_);
    }
    $_=$orig;
  }
print STDERR "Loading lexicon: done                           \n";

$linenumber=0;
$i=0;
$orig_lexicon_size = $#lexicon + 1;
while ($i<=$#lexicon) {
  print STDERR "De-factorizing lexicon: ".int(100*(1-($#lexicon - $i)/$orig_lexicon_size))."\% done       \r";
  @newentries=();
  push(@newentries,$lexicon[$i]);
  $finished=0;
  while (!$finished) {
    $finished=1;
    $j=0;
    while ($j<=$#newentries) {
      if ($newentries[$j]=~/^(.+?<[^\|]*:)([^,>]*\|[^,>]*)([,>].*)/) {
	$finished=0;
	$debut=$1;
	$milieu=$2;
	$fin=$3;
	@tmpentries=();
	for (split(/\|/,$milieu)) {
	  push(@tmpentries,$debut.$_.$fin);
	}
	splice(@newentries,$j,1,@tmpentries);
	$j+=$#newentries;
      }
      $j++;
    }
  }
  splice(@lexicon,$i,1,@newentries);
  $i+=$#newentries+1;
}
print STDERR "De-factorizing lexicon: done                                      \n";

print STDERR "Generating output...";
for (@lexicon) {
  if (!$noopt || $_!~/__NULL__/) {
    if (/AttObj[^àd]/) {$att = "ato"}
    else {$att = "ats"}
    next if /\taux(Avoir|Etre)/;
    next if /___lv__/;
    $is_impers = 0;
    if (s/,\@impers//) {
      $is_impers = 1;
    }
    s/^[^\t]*?\t[^\t]*?\t[^\t]*?\t\[pred=\"([^<\"\t]*?)___[^<]*(<[^\t>]*?>)[^\t]*\t[^\t]*\t[^\t]+\t([^\t]*)\t\%[^\t]+\t[^\t]+$/\1\t\2\t\3/ || die map {s/\t/<TAB>/g, $_} $_;
    s/(?<=[<,])[^:]+:__NULL__//g;
    s/,,+/,/g;
    s/<,+/</;
    s/,+>/>/;
    s/,(\t|$)//;
    s/^(.*)\@pron_possible(.*)$/\1\2\n\1\@pron\2/;
    s/\",.*?\t//;
    $has_clr = 0;
    if (s/^(?:s'|se ) *//) {
      $has_clr = 1;
    } elsif (s/^\(s[e']\) *//) {
      $has_clr = 2;
    }
    if ($frames || $alexinalike_frames) {
      s/^(.*?)\t<(.*?)>\t(.*)$/\1/;
      $tag = $3;
      $scat = $2;
      if ($tag eq "G") {$tag = "VPR"}
      elsif ($tag eq "W") {$tag = "VINF"}
      elsif ($tag =~ /^K/) {$tag = "VPP"}
      elsif ($tag =~ /^P/) {$tag = "V"}
      $_ .= ":$tag\t";
      @scat = ();
      if ($is_impers) {
	push @scat, "suj:N";
      }
      for $s (split /,/, $scat) {
	$s =~ s/^objà/a_obj/i;
	$s =~ s/^objde/de_obj/i;
	$s =~ s/^obj/obj/i;
	$s =~ s/^att/$att/i;
	if ($is_impers) {
	  $s =~ s/^suj/obj/i;
	} else {
	  $s =~ s/^suj/suj/i;
	}
	$s =~ s/^(d?loc|obl\d*)/p_obj/i;
	unless ($marie) {
	  $s =~ s/^obj:(.*?)\{de\}$/obj:P/;
	  $s =~ s/^obj:(.*?)\{à\}$/obj:P/;
#	  $s =~ s/^obj:(.*?)\{de\}$/de_obj:\1/;
#	  $s =~ s/^obj:(.*?)\{à\}$/a_obj:\1/;
	}
	die if (/\}$/ && /^obj/);
	$s =~ s/\{.*\}$//;
#	if (/^.*?:CLR$/) {
#	  if ($has_clr == 0) {$has_clr = 1;}
#	} else {
	  push @scat, $s;
#	}
      }
      $_ .= join(" ", @scat);
    }
    $saved = $_;
    if ($has_clr == 0 || $has_clr == 2) {
      output ($_);
      if ($tag =~ /^(?:VPP|VINF|VPR)$/ && s/(\t| )suj:N( |$)/\1/) {
	s/^ //;
	output ($_);
      }
    }
    $_ = $saved;
    if ($has_clr == 1 || $has_clr == 2) {
      s/$/ aff:CLR/;
      output ($_);
      if ($tag =~ /^(?:VPP|VINF|VPR)$/ && s/(\t| )suj:N /\1/) {
	output ($_);
      }
    }
  }
}
print STDERR "done\n";

sub output {
  my $s = shift;
  $s =~ s/\t +/\t/;
  if ($s =~ /^[^\t]+$/) {$s .= "\t"}
  if ($s =~ /\t$/) {$s .= "NULL"}
  print "$s\n";
}
