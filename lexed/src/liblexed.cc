//<head>//////////////////////////////////////////////////////////////
//
//	
//	Lexed version 3.1
//	Copyright (C) 1999 2000 2001 2002 2003, 2006.
//	Universit� Paris 7
//	INRIA Rocquencourt
//
//	Conception & programmation :
//	Lionel Cl�ment
//	e-mail : lionel.clement@linguist.jussieu.fr
//
//	vendredi 18 janvier 2002, 13h41
//
//
////////////////////////////////////////////////////////////////</head>////

#include <sys/stat.h>

#include "liblexed.h"

//#define DIFF

char error[LEXEDMAXSTRING];
char sep_pref[LEXEDMAXSTRING] = "";
char sep_or[LEXEDMAXSTRING]   = "|";
char sep_suff[LEXEDMAXSTRING] = "\n";
char sep_uw[LEXEDMAXSTRING]   = "<unknown>";
char delimiter = '\t';
int weight_replace      = 1;
int weight_add_double   = 3;
int weight_erase_double = 3;
int weight_swapp        = 4;
int weight_substitute   = 5;
int weight_add          = 6;
int weight_erase        = 6;
int memoire = 1;
int indexation = 0;
FILE *table_file;
char *table;
char *words;
long table_size = 0;
FSA *fsa;         // table fsa
InfoBuff *info;   //table des info
TYPEPTR initial;
Tree *lexique_init;
Tree *lexique;

int _add_result(TYPEPTR **result, int *result_size, int *result_index, TYPEPTR info, int weight) {
  if (info == (TYPEPTR)(~(0UL))) {
    return 0;
  }

  if (*result == NULL) {
    *result_size = LEXEDMAXRESULT;
    if ((*result = (TYPEPTR *)calloc(LEXEDMAXRESULT, sizeof(TYPEPTR))) == NULL) {
      perror("too much allocation");
      return -1;
    }
    
    (*result_index) = 0;
  }

  if (*result_index == *result_size) {
    (*result_size) *= 2;
    if ((*result = (TYPEPTR *)realloc(*result, sizeof(TYPEPTR) * (*result_size))) == NULL) {
      perror("too much allocation");
      return -1;
    }
  }

  (*result)[*result_index]       = info;
  (*result)[(*result_index) + 1] = weight;
  (*result)[(*result_index) + 2] = (TYPEPTR)~0UL;
  (*result_index) += 2;
  return 0;
}

int _list(list_result *result, TYPEPTR index, char *word, int rank) {
  word[rank] = fsa[index].car;

  if (fsa[index].son != (TYPEPTR)(~(0UL))) {
    _list(result, fsa[index].son, word, rank + 1);
  }

  if (fsa[index].brother != (TYPEPTR)(~(0UL))) {
    _list(result, fsa[index].brother, word, rank);
  }

  if (fsa[index].info != (TYPEPTR)(~(0UL))) {
    word[rank]     = fsa[index].car;
    word[rank + 1] = 0;

    if (result->items_size + 1 > result->items_max_size) {
      result->items_max_size = result->items_max_size * 2;
      result->items = (list_item *) realloc(result->items, sizeof(list_item) * (result->items_max_size));
    }

    int wordlen = strlen(word) + 1;
    if (result->words_size + wordlen > result->words_max_size) {
      result->words_max_size = result->words_max_size * 2;
      result->words = (char *) realloc(result->words, sizeof(char) * (result->words_max_size));
    }

    memcpy(result->words + result->words_size, word, wordlen);

    result->items[result->items_size].word_offset = result->words_size;
    result->items[result->items_size].info = table + info[fsa[index].info].offset;

    result->words_size += wordlen;
    result->items_size += 1;
  }

  return 0;
}

static
int add_table_entry (TYPEPTR offset, char *info,long input_size,long *allocated_size) 
{

    int infolen = strlen(info);

//    fprintf(stderr,"tmp table info o=%d '%s'\n",offset,info);
      
        // reallocate if needed
    if (!input_size) {
	table_size += infolen + 1;
	if (table_size > *allocated_size) {
                //          fprintf(stderr,"table reallocate '%s'\n",info);
            *allocated_size = (*allocated_size) * 2;
            table = (char *) realloc(table, *allocated_size);
	}
    }

//    fprintf(stderr,"table info '%s'\n",info);
    memcpy(table + offset, info, infolen);
    table[offset + infolen] = 0;

    return infolen + 1;
}


int load_from_input(FILE *input, long input_size) {

  int i;
  char *p;
  int infolen = 0;
  int match;
  long allocated_size = 1024;
  char line[LEXEDMAXSTRING];
  char info[LEXEDMAXSTRING];
  TYPEPTR offset = 0;
  int init = 1;
  
  lexique = new Tree(NULL, NULL, NULL, '\0');
  lexique_init = new Tree(NULL, NULL, NULL, '\0');
  lexique_init->set_son(lexique);

  // initial allocation
  if (input_size) {
    table = (char *) malloc(sizeof(char) * input_size);
  } else {
    table = (char *) malloc(sizeof(char) * allocated_size);
  }

  while (fgets(line, LEXEDMAXSTRING, input)) {
    if (!*line) {
      continue;
    }

    // get rid of end of line char if needed
    p = line + strlen(line) - 1;
    if (*p == '\n')
      *p = 0;

    // delimit word
    if (p = strchr(line, delimiter)) {
      match = 1;
      *p = 0;
    } else {
      match = 0;
    }

    
//    fprintf(stderr,"info o=%d match=%d info='%s' p='%s'\n",offset,match,info,p+1);
    
    // insert in table if needed
    if (match && strncmp(info, p + 1, LEXEDMAXSTRING)) {

        offset += infolen;
        
//        fprintf(stderr,"***enter\n");
        
        strncpy(info, p + 1, LEXEDMAXSTRING); 
        infolen = add_table_entry(offset,info,input_size,&allocated_size);
    }

//    fprintf(stderr,"Add  offset=%d '%s'\n",offset,line);
    // add new word at current offset
    lexique->add(line, offset);

    
  }

  add_table_entry(offset,info,input_size,&allocated_size);
  
  return 1;
}

int load_from_files(char *directory, char *prefix) {
  char table_filename[LEXEDMAXSTRING];
  char fsa_filename[LEXEDMAXSTRING];

  snprintf(fsa_filename, LEXEDMAXSTRING, "%s/%s.fsa", directory, prefix);
  if (!load_fsa(fsa_filename))
    return 0;

  snprintf(table_filename, LEXEDMAXSTRING, "%s/%s.tbl", directory, prefix);
  if (!load_table(table_filename))
    return 0;

  return 1;
}

int save_to_files(char *directory, char *prefix) {
  char table_filename[LEXEDMAXSTRING];
  char fsa_filename[LEXEDMAXSTRING];
  
  fputs("*** Writing Finite State Automata\n", stderr);
  snprintf(fsa_filename, LEXEDMAXSTRING, "%s/%s.fsa", directory, prefix);
  if (save_fsa(fsa_filename))
    return 0;
  
  if (!indexation) {
    fputs("*** Writing information table\n", stderr);
    snprintf(table_filename, LEXEDMAXSTRING, "%s/%s.tbl", directory, prefix);
    save_table(table_filename);
  }

  return 1;
}

int finish() {
  if (!memoire) {
    fclose(table_file);
  }
}



////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
int print_results (TYPEPTR index,
		   int client_server, 
		   Server *server, 
		   int sep)
{
  char label[LEXEDMAXSTRING]; 

  if (sep) {
    if (client_server) {
      server->put_data(sep_pref);
    } else {
      fputs(sep_pref, stdout);
    }
  }

  if (index == (TYPEPTR)(~(0UL))) {
    if (client_server) {
      server->put_data(sep_uw);
      if (sep)
	server->put_data(sep_suff);
    } else {
      fputs(sep_uw, stdout);
      if (sep)
	fputs(sep_suff, stdout);
    }
    return 0;
  }

  while (index != (TYPEPTR)(~(0UL))) {
    // an other element will be describe
    if (indexation) {
      snprintf(label, LEXEDMAXSTRING, "%lX", info[index].offset);
    } else {
      if (memoire) {
	strncpy(label, table+(info[index].offset), LEXEDMAXSTRING);
      } else {
	fseek (table_file, info[index].offset, SEEK_SET);
	fgets (label, LEXEDMAXSTRING, table_file);
      }
    }

    if (client_server) {
      server->put_data(label);
      if (info[index].next!=(TYPEPTR)(~(0UL)))
	server->put_data(sep_or);
    } else {
      fputs(label, stdout);
      if (info[index].next!=(TYPEPTR)(~(0UL)))
	fputs(sep_or, stdout);
    }

    if ((info[index].next)!=(TYPEPTR)(~(0UL))) {
      index=info[index].next;
    } else {
      index=(TYPEPTR)(~(0UL));
    }
  }

  if (sep) {
    if (client_server) {
      server->put_data(sep_suff);
    } else {
      fputs(sep_suff, stdout);
    }
  }

  return 0;
}

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
int sprint_results (TYPEPTR index,
		    char ***result,
		    int *result_size,
		    int *result_index)
{
  char label[LEXEDMAXSTRING]; 

  if ((*result)==NULL) {
    *result_size=LEXEDMAXRESULT;
    if ((*result = (char **)calloc(LEXEDMAXRESULT, sizeof(char*)))==NULL) {
      perror("too much allocation");
      return -1;
    }
    *result_index=0;
  }

  if (index == (TYPEPTR)(~(0UL))) {
    strncpy((*result)[*result_index], sep_uw, LEXEDMAXSTRING);
    (*result)[(*result_index)+1]=NULL;
    (*result_index)++;
    return 0;
  }
  while (index != (TYPEPTR)(~(0UL))) {
    if ((*result_index) >= (*result_size)) {
      (*result_size) *= 2;
      if ((*result = (char **)realloc(*result, sizeof(char*) * (*result_size)))==NULL) {
	perror("too much allocation");
	return -1;
      }
    }
    // an other element will be describe
    if (indexation) {
      snprintf(label, LEXEDMAXSTRING, "%lX", info[index].offset);
    } else {
      if (memoire) {
	strncpy(label, table+(info[index].offset), LEXEDMAXSTRING);
      } else {
	fseek(table_file, info[index].offset, SEEK_SET);
	fgets(label, LEXEDMAXSTRING, table_file);
      }
    }
    (*result)[*result_index]=strdup(label);
    (*result)[(*result_index)+1]=NULL;
    (*result_index)++;
        
    if ((info[index].next)!=(TYPEPTR)(~(0UL)))
      index=info[index].next;
    else
      index=(TYPEPTR)(~(0UL));
  }
  return 0;
}

////////////////////////////////////////////////////////////
// input: string
// output: info
////////////////////////////////////////////////////////////
TYPEPTR search_static(TYPEPTR index,
		     char *string)
{
  char *car=string;
  index=fsa[index].son;
  // for each letter of the suffix
  while (*car) {
    // parse the brothers while founding the actual char
    while (*car != fsa[index].car) {
      if (fsa[index].brother != (TYPEPTR)(~(0UL)))
	index=fsa[index].brother;
      else
	return (TYPEPTR)(~(0UL));
    }
    if (*car != fsa[index].car)
      return ((TYPEPTR)(~(0UL)));
    if (*(car+1)) {
      if (fsa[index].son != (TYPEPTR)(~(0UL)))
	index=fsa[index].son;
      else
	return (TYPEPTR)(~(0UL));
    } else
      if (fsa[index].info != (TYPEPTR)(~(0UL)))
	return fsa[index].info;
    car++;
  } 
  return (TYPEPTR)(~(0UL));
}


////////////////////////////////////////////////////////////
// input: string
// output: list of info
////////////////////////////////////////////////////////////
int search(TYPEPTR index,
	    char *suffix,
	    int nb_erase, 
	    int nb_substitute, 
	    int nb_add, 
	    int nb_swap, 
	    int nb_erase_double, 
	    int nb_add_double,
	    int nb_replace, 
	    struct FindReplaceStructure *replace,
	    int already_modified,
	    TYPEPTR **result, 
	    int *result_size,
	    int *result_index,
	    int weight)
{
  //TYPEPTR sy;
  char *car=suffix;
  int r;
  //fputs (suffix, stderr);
  //fputc ('\n', stderr);
  //for (int i=0;i<already_modified;i++)
  //  fputc (' ', stderr);
  //fputc ('^', stderr);
  //fputc ('\n', stderr);
  
  for (TYPEPTR sy = fsa[index].son; sy != (TYPEPTR)(~(0UL)); sy = fsa[sy].brother) {
    if (*car) {
      if (!*(car+1)) {
	int cont = 0;
	if (fsa[sy].car == *car)
	  cont = 1;
	if (nb_substitute && !already_modified) {
	  cont = 1;
	}
	if (nb_replace && !already_modified) {
	  for (struct FindReplaceStructure *rep=replace;rep!=NULL && (*rep).find!=NULL;rep++) {
	    if ((strchr((*rep).find, *car)!=NULL) && (strchr((*rep).replace, fsa[sy].car)!=NULL)) {
	      cont=1;
	      break;
	    }
	  }
	}
	if (cont) {
	  r = _add_result(result, result_size, result_index, fsa[sy].info, weight
	    + ((nb_substitute && fsa[sy].car != *car) ? weight_substitute : 0)
	    + ((nb_replace && fsa[sy].car != *car) ? weight_replace : 0)
	  );
	  if (r == -1) {
	    return -1;
	  }
	}
      }
      if (fsa[sy].car == *car) {
	search(sy, car+1, nb_erase, nb_substitute, nb_add, nb_swap, nb_erase_double, nb_add_double, nb_replace, replace, already_modified?already_modified-1:0, result, result_size, result_index, weight);
      } else {
	if (nb_substitute && !already_modified) {
	  search(sy, car+1, nb_erase, nb_substitute-1, nb_add, nb_swap, nb_erase_double, nb_add_double, nb_replace, replace, already_modified?already_modified-1:0, result, result_size, result_index, weight + weight_substitute);
	}
	if (nb_replace && !already_modified) {
	  int cont = 0;
	  if (replace != NULL) {
	    for (struct FindReplaceStructure *rep=replace;rep!=NULL && (*rep).find!=NULL;rep++) {
	      if ((strchr((*rep).find, *car)!=NULL) && (strchr((*rep).replace, fsa[sy].car)!=NULL)) {
		cont = 1;
		break;
	      }
	    }
	  } else {
	    cont = 1;
	  }
	  if (cont) {
	    search(sy, car+1, nb_erase, nb_substitute, nb_add, nb_swap, nb_erase_double, nb_add_double, nb_replace-1, replace, already_modified?already_modified-1:0, result, result_size, result_index, weight + weight_replace);
	  }
	}
      }
    }
    if (nb_add && !already_modified && fsa[sy].car!=*(car)) {
      char *aux=(char*)calloc(strlen(car)+2, sizeof(char));
      strncpy(aux, car, LEXEDMAXSTRING);
      for (int i=strlen(aux);i;i--) {
	*(aux+i) = *(aux+i-1);
      }
      *aux = fsa[sy].car;
      search(index, aux, nb_erase, nb_substitute, nb_add-1, nb_swap, nb_erase_double, nb_add_double, nb_replace, replace, already_modified+1, result, result_size, result_index, weight + weight_add);
      free(aux);
    }
    if (nb_add_double && !already_modified && fsa[sy].car==*(car)) {
      char *aux=(char*)calloc(strlen(car)+2, sizeof(char));
      strncpy(aux, car, LEXEDMAXSTRING);
      for (int i=strlen(aux)+1;i;i--) {
	*(aux+i) = *(aux+i-1);
      }
      *aux = fsa[sy].car;
      search(index, aux, nb_erase, nb_substitute, nb_add, nb_swap, nb_erase_double, nb_add_double-1, nb_replace, replace, already_modified+1, result, result_size, result_index, weight + weight_add_double);
      free(aux);
    }
  }

  if (*car) {
    if (nb_erase && !already_modified) {
      if (!*(car+nb_erase)) {
	r = _add_result(result, result_size, result_index, fsa[index].info, weight + weight_erase);
	if (r == -1) {
	  return -1;
	}
      }
      search(index, car+1, nb_erase-1, nb_substitute, nb_add, nb_swap, nb_erase_double, nb_add_double, nb_replace, replace, already_modified?already_modified-1:0, result, result_size, result_index, weight + weight_erase);
    }
    if (nb_erase_double && !already_modified && *car==*(car+1)) {
      if (!*(car+2)) {
	r = _add_result(result, result_size, result_index, fsa[index].info, weight + weight_erase_double);
	if (r == -1) {
	  return -1;
	}
      }
      search(index, car+1, nb_erase, nb_substitute, nb_add, nb_swap, nb_erase_double-1, nb_add_double, nb_replace, replace, already_modified?already_modified-1:0, result, result_size, result_index, weight + weight_erase_double);
    }
    if (nb_swap && *(car+1) && !already_modified) {
      char *aux;
      if ((aux=(char*)calloc(strlen(car)+2, sizeof(char))) == NULL) {
	perror("too much allocation");
	return -1;
      }
      strncpy(aux, car, LEXEDMAXSTRING);
      char c=*(car+1);
      *(car+1)=*(car);
      *(car)=c;
      search(index, car, nb_erase, nb_substitute, nb_add, nb_swap-1, nb_erase_double, nb_add_double, nb_replace, replace, already_modified+2, result, result_size, result_index, weight + weight_swapp);
      strncpy(car, aux, LEXEDMAXSTRING);
      free(aux);
    }
  }
}

////////////////////////////////////////////////////////////
// 
////////////////////////////////////////////////////////////
list_result list() {
  char word[LEXEDMAXSTRING];

  struct list_result result;
  result.items_size     = 0;
  result.items_max_size = 8;
  result.items = (list_item*) malloc(sizeof(list_item) * result.items_max_size);
  result.words_size     = 0;
  result.words_max_size = 64;
  result.words = (char*) malloc(sizeof(char) * result.words_max_size);

  _list(&result, fsa[initial].son, word, 0);

  return result;
}

int free_list_result(list_result *result) {
  free(result->items);
}

////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////
int
save_fsa(char* filename)
{
  TYPEPTR size;
  int nb_bytes;
  FILE *file;

  file = fopen(filename, "w");
  if (!file) {
    snprintf(error, LEXEDMAXSTRING, "Unable to open file %s for writing", filename);
    perror(error);
    return -1;
  } else {
    // encodage des offsets (16 ou 32 bits)
    nb_bytes=sizeof(TYPEPTR);
    fwrite(&nb_bytes, sizeof(nb_bytes), 1, file);
#ifdef DIFF
    fprintf(stdout, "%d bytes\n", nb_bytes);
#endif //DIFF
    size=(TYPEPTR)~0UL;
    fwrite(&size, sizeof(size), 1, file);
#ifdef DIFF
    fprintf(stdout, "Test %lX\n", size);
#endif //DIFF
    size=indexation?(TYPEPTR)1:(TYPEPTR)0;
    fwrite(&size, sizeof(size), 1, file);
#ifdef DIFF
    fprintf(stdout, "indexation %lX\n", size);
#endif //DIFF
    // nombre d'offsets du fsa
    size=0;
    lexique_init->set_index_fsa(size);
    fwrite(&size, sizeof(size), 1, file);
#ifdef DIFF
    fprintf(stdout, "Size FSA %lX\n", size);
#endif //DIFF
    if (size == (TYPEPTR)~0UL) {
      perror("Lexicon too large");
      fclose(file);
      return -1;
    }
    size=0;
    lexique_init->set_index_info(size);
    fwrite(&size, sizeof(size), 1, file);
#ifdef DIFF
    fprintf(stdout, "Size Info %lX\n", size);
#endif //DIFF
    if (size == (TYPEPTR)~0UL) {
      perror("Data too large");
      fclose(file);
      return -1;
    }
#ifdef DIFF
    fprintf(stdout, "---FSA---\n");
#endif //DIFF
    lexique_init->print_fsa(lexique_init, &initial, file);
#ifdef DIFF
    fprintf(stdout, "---Info---\n");
#endif //DIFF
    lexique_init->print_info(file);
    fputs("*** Writing Data\n", stderr);
    fflush(file);
    // table
    fflush(file);
    fwrite(&initial, sizeof(initial), 1, file);
#ifdef DIFF
    fprintf(stdout, "initial: %lX\n", Initial);
#endif //DIFF
    fclose(file);
    return 0;
  }
}

int
save_table(char* filename)
{
  FILE *file;

  file = fopen (filename, "w");
  if (!file) {
    snprintf(error, LEXEDMAXSTRING, "Unable to open file %s for writing", filename);
    perror(error);
    return -1;
  } else {
    fwrite(table, 1, table_size, file);
    fclose(file);
    return 0;
  }
}

////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////
int
load_fsa(char *filename) {
  FILE *file;
  TYPEPTR size_fsa;
  TYPEPTR size_info;
  int nb_bytes;

  file = fopen(filename, "r");
  if (!file) {
    snprintf(error, LEXEDMAXSTRING, "Unable to open file %s for reading", filename);
    perror(error);
    return -1;
  } else {
    fputs("*** Loading Finite State Automata\n", stderr);
    fread(&nb_bytes, sizeof(nb_bytes), 1, file);
#ifdef DIFF
    fprintf(stdout, "%d bytes\n", nb_bytes);
#endif //DIFF
    fread(&size_fsa, sizeof(size_fsa), 1, file);
#ifdef DIFF
    fprintf(stdout, "Test %lX\n", size_fsa);
#endif //DIFF
    if (nb_bytes!=(sizeof(TYPEPTR)) || (size_fsa!=(TYPEPTR)~0UL)) {
      perror("lexicon not compiled with the good version of Lexed or on an incompatible system");
      fclose (file);
      return -1;
    }
    fread(&size_fsa, sizeof(size_fsa), 1, file);
#ifdef DIFF
    fprintf(stdout, "indexation: %lX\n", size_fsa);
#endif //DIFF
    if (size_fsa)
      indexation=1;
    fread(&size_fsa, sizeof(size_fsa), 1, file);
#ifdef DIFF
    fprintf(stdout, "size FSA %lX\n", size_fsa);
#endif //DIFF
    fread(&size_info, sizeof(size_info), 1, file);
#ifdef DIFF
    fprintf(stdout, "size info %lX\n", size_info);
#endif //DIFF
#ifdef DIFF
    fprintf(stdout, "---FSA---\n");
#endif //DIFF
    fsa = new FSA[size_fsa+1];
    fread(fsa, sizeof(FSA), size_fsa, file);
#ifdef DIFF
    for (unsigned long int size_sy=0;size_sy<size_fsa;size_sy++) {
      fprintf(stdout, " so:%lX", fsa[size_sy].son);
      fprintf(stdout, " br:%lX", fsa[size_sy].brother);
      fprintf(stdout, " in:%lX", fsa[size_sy].info);
      fprintf(stdout, " <%c>\n", fsa[size_sy].car);
    }
#endif //DIFF
#ifdef DIFF
    fprintf(stdout, "---Info---\n");
#endif //DIFF
    info = new InfoBuff [size_info+1];
    fread(info, sizeof(InfoBuff), size_info, file);
#ifdef DIFF
    for (unsigned long int size_sy=0;size_sy<size_info;size_sy++) {
      fprintf(stdout, " su:%lX of:%lX\n", info[size_sy].next, info[size_sy].offset);
    }
#endif //DIFF
    fread(&initial, sizeof(initial), 1, file);
#ifdef DIFF
    fprintf(stdout, "initial: %lX\n", initial);
#endif //DIFF
    fclose(file);
    return 1;
  }
}

int
load_table(char *filename) {
  FILE *file;
  struct stat statbuf;

  file = fopen(filename, "r");
  if (!file) {
    snprintf(error, LEXEDMAXSTRING, "Unable to open file %s for reading", filename);
    perror(error);
    return -1;
  } else {
    if (memoire) {
      fputs("*** Load table in memory\n", stderr);
      stat(filename, &statbuf);
      table_size = statbuf.st_size;
      
      table = (char *) malloc(sizeof(char) * table_size);
      fread(table, 1, table_size, file);

      fclose(file);
    } else {
      table_file = file;
    }

    return 0;
  }
}
