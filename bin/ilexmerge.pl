#!/usr/bin/env perl

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

my %lexicon;
my $filter_entries_with_no_subcat = 1;
my $verbose = 0;
my $filenames = "";
my $keep_sources = 1;
my $default_table = "";

while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-f$/) {$filter_entries_with_no_subcat = 0;}
    elsif (/^-v$/) {$verbose=1;}
    elsif (/^-no_src$/) {$keep_sources=0;}
    elsif (/^-dt$/) {$default_table=shift;}
    elsif (/^([^-].*)$/) {$filenames.=" $1";}
    else {die "Unknown option: $_"}
}

for my $filename (split(/ /,$filenames)) {
  next if ($filename eq "");
  open (ILEX,"<$filename") || die "Could not open $filename: $!";
  binmode ILEX, ":utf8";
  while (<ILEX>) {
    chomp;
    if (s/(^|[^\\])\#(.*)//) {
      $comment = $2;
    } else {
      $comment = "";
    }
    next if (/^\s*$/ || /^>/);
    s/\@pseudo[-_]se/\@pron/g;
    s/\@se[-_]moyen/\@pron/g;
    s/^([^\t_]+)___[^\s_]+\t/\1\t/;
    /^(?:se |s' ?)?([^\t]+?)(?:___([^\t_]*)__([0-9A-Za-z]+))?\t(.*?)\t([0-9]+);(.*?);(.*?);(.*?);(.*?);([^\t]*)(\t.*)?/ || die "Error: $_\n";
    $lemma = $1;
    $lemma =~ s/\s+$//;
    $source = $2 || "L";
    $tmp{$lemma}{$source}++;
    $entry = $3 || $tmp{$lemma}{$source};
    if ($4 ne "" && $4 ne $default_table && ($source eq "L" || !defined($lexicon{$lemma}{L}{__DEFAULT__}{morpho}))) { # The Leff? entry stores the default morpho
      $lexicon{$lemma}{L}{__DEFAULT__}{morpho}=$4;
    }
    $lexicon{$lemma}{$source}{$entry}{morpho}=$4 unless $4 eq $default_table;
    $lexicon{$lemma}{$source}{$entry}{comment}=$comment.$11;
    $lexicon{$lemma}{$source}{$entry}{comment} =~ s/^\t//;
    #    print STDERR "$lemma $source $entry\n";
    $lexicon{$lemma}{$source}{$entry}{weight}=$5;
    $lexicon{$lemma}{$source}{$entry}{pred}=$6 || "Lemma";
    $lexicon{$lemma}{$source}{$entry}{cat}=$7;
    $lexicon{$lemma}{$source}{$entry}{scat}=$8;
    $lexicon{$lemma}{$source}{$entry}{macros}=$9;
    $lexicon{$lemma}{$source}{$entry}{restr}=$10 || "";
    $lexicon{$lemma}{$source}{$entry}{"ALL"}=$_;
    if ($lexicon{$lemma}{$source}{$entry}{restr} eq "\%pronominal_actif"
	&& $lexicon{$lemma}{$source}{$entry}{macros} !~ /\@pron/) {
      $lexicon{$lemma}{$source}{$entry}{macros} .= ",\@pron";
      $lexicon{$lemma}{$source}{$entry}{macros} =~ s/^,//;
    }
    if ($lexicon{$lemma}{$source}{$entry}{macros} =~ /\@pron/
	&& $lexicon{$lemma}{$source}{$entry}{pred} !~ /^s(e |')/) {
      if ($lemma =~ /^[aeiuoé]/) {
	$lexicon{$lemma}{$source}{$entry}{pred} = "s'".$lexicon{$lemma}{$source}{$entry}{pred};
      } else {
	$lexicon{$lemma}{$source}{$entry}{pred} = "se ".$lexicon{$lemma}{$source}{$entry}{pred};
      }
    }
  }
}

%tmp = ();

for $lemma (sort keys %lexicon) {
  %is_included_in=();
  %includes=();
  for $source1 (sort keys %{$lexicon{$lemma}}) {
    for $entry1 (keys %{$lexicon{$lemma}{$source1}}) {
      next if ($entry1 eq "__DEFAULT__");
      $is_included_in{$source1}{$entry1}{0}{0}=1;
      for $source2 (sort keys %{$lexicon{$lemma}}) {
	next if ($source2 eq $source1);
	for $entry2 (keys %{$lexicon{$lemma}{$source2}}) {
	  if ($source2 eq "M" && $source1 ne "M" 
	      && $lexicon{$lemma}{$source1}{$entry1}{pred} eq $lexicon{$lemma}{$source2}{$entry2}{pred}) {
	    $is_included_in{$source1}{$entry1}{$source2}{$entry2}=1;
	    $includes{$source2}{$entry2}{$source1}{$entry1}=1;
	  } elsif ($source2 eq "L" && $source1 eq "D"
		   || $source2 eq "T" && $source1 eq "L"
		   || $source2 eq "T" && $source1 eq "D"
		   || $source2 eq "L" && $source1 eq "LVF"
		   || $source2 eq "T" && $source1 eq "LVF"
		   || $source2 eq "D" && $source1 eq "LVF"
		  ) {
	    $fframe1 = $lexicon{$lemma}{$source1}{$entry1}{scat};
	    $fframe2 = $lexicon{$lemma}{$source2}{$entry2}{scat};
	    $fframe1 =~ s/:.*?([>,])/$1/g;
	    $fframe2 =~ s/:.*?([>,])/$1/g;
	    $fframe1 =~ s/^<(.*)>$/$1/g;
	    $fframe2 =~ s/^<(.*)>$/$1/g;
	    $offrame1=$fframe1;
	    if ($fframe2 ne "" && $fframe1 =~ s/$fframe2(,|$)/$1/ && $fframe1 !~ /(Suj|Obj)/) {
	      #			    print STDERR "$source1\__$entry1 is in $source2\__$entry2      $offrame1 ($fframe1) is included in $fframe2\n";
	      $is_included_in{$source1}{$entry1}{$source2}{$entry2}=1;
	      $includes{$source2}{$entry2}{$source1}{$entry1}=1;
	    }
	  }
	}
      }
    }
  }
  $didsomething = 1;
  while ($didsomething > 0) {
    $didsomething = 0;
    for $source1 (sort keys %is_included_in) {
      for $entry1 (keys %{$is_included_in{$source1}}) {
	#		print STDERR "$source1\t$entry1\n";
	for $source2 (sort keys %{$is_included_in{$source1}{$entry1}}) {
	  for $entry2 (keys %{$is_included_in{$source1}{$entry1}{$source2}}) {
	    for $source3 (sort keys %{$is_included_in{$source1}{$entry1}}) {
	      for $entry3 (keys %{$is_included_in{$source1}{$entry1}{$source3}}) {
		#				print STDERR "if (\$is_included_in{$source2}{$entry2}{$source3}{$entry3}) {\n";
		if ($is_included_in{$source2}{$entry2}{$source3}{$entry3}) {
		  #				    print STDERR "delete (\$is_included_in{$source1}{$entry1}{$source3}{$entry3});\n";
		  delete ($is_included_in{$source1}{$entry1}{$source3}{$entry3});
		  delete ($includes{$source3}{$entry3}{$source1}{$entry1});
		  $didsomething = 1;
		}
	      }
	    }
	  }
	}
      }
    }
  }
  #     for $source1 (sort keys %is_included_in) {
  # 	for $entry1 (keys %{$is_included_in{$source1}}) {
  # 	    print STDERR "$source1\t$entry1\t".(keys %{$is_included_in{$source1}{$entry1}})."\n";
  # 	    for $source2 (sort keys %{$is_included_in{$source1}{$entry1}}) {
  # 		for $entry2 (keys %{$is_included_in{$source1}{$entry1}{$source2}}) {
  # 		    print STDERR "! $source1\t$entry1\t$source2\t$entry2\n";
  # 		}
  # 	    }
  # 	}
  #     }
  $output = "";
  for $source1 (sort keys %is_included_in) {
    for $entry1 (keys %{$is_included_in{$source1}}) {
      next if (scalar keys %{$includes{$source1}{$entry1}} > 0 || $source eq "0");
      #	    print STDERR "<<<$source1\__$entry1>>>\n";
      $output .= join(" ; ",build_links ($source1, $entry1))." ; ";
    }
    $output .= " ; ";
  }
  $output =~ s/(?<=;)  ;//g;  
  $output =~ s/^ ;//g;  
  $output =~ s/; $//g;  
  if ($verbose) {
    print STDERR "=== $lemma: $output ===\n";
  }
  for $group (split(/ ; /,$output)) {
    $group =~ s/^ //;
    if ($verbose) {
      print STDERR "\t$lemma: $group\n";
    }
    @allscats=();
    @allrestrs=();
    @allmacros=();
    @allpreds=();
    $lefff_entry = "";
    $dv_entry = "";
    $m_entry = "";
    $lvf_entry = "";
    for (split(/ /,$group)) {
      /^(.+?)__(.+)$/;
      $s = $1; $e = $2;
      if ($s eq "L") {
	$lefff_entry = $e;
      } elsif ($s eq "D") {
	$dv_entry = $e;
      } elsif ($s eq "M") {
	$m_entry = $e;
      } elsif ($s eq "LVF") {
	$lvf_entry = $e;
      }
      if ($verbose) {
	print STDERR "\t\t";
	print STDERR $lexicon{$lemma}{$s}{$e}{pred}.";";
	print STDERR $lexicon{$lemma}{$s}{$e}{scat}.";";
	print STDERR $lexicon{$lemma}{$s}{$e}{macros}.";";
	print STDERR $lexicon{$lemma}{$s}{$e}{restr};
	print STDERR $lexicon{$lemma}{$s}{$e}{comment}."\n";
      }
      $scat = $lexicon{$lemma}{$s}{$e}{scat};
      $scat =~ s/(?<=[:,\|])(.*?)([,|\)>])/$1\__$s$2/g if $keep_sources;
      push(@allscats,$scat);
      push(@allrestrs,$lexicon{$lemma}{$s}{$e}{restr});
      push(@allmacros,$lexicon{$lemma}{$s}{$e}{macros});
      push(@allpreds,$lexicon{$lemma}{$s}{$e}{pred});
    }
    $additional_comment = "";
    $merged = "$lemma\t";
    if ($lefff_entry ne "" && defined($lexicon{$lemma}{L}{$lefff_entry}{morpho}) && $lexicon{$lemma}{L}{$lefff_entry}{morpho} ne "") {
      $merged .= $lexicon{$lemma}{L}{$lefff_entry}{morpho};
    } elsif (defined($lexicon{$lemma}{L}{__DEFAULT__}{morpho}) && $lexicon{$lemma}{L}{__DEFAULT__}{morpho} ne "") {
      $merged .= $lexicon{$lemma}{L}{__DEFAULT__}{morpho};
    } elsif ($default_table ne "") {
      $merged .= $default_table;
    } else {
      if ($verbose) {
	print STDERR ">>> $lemma\n";
      }
      if ($lemma =~ /er$/) {
	$merged .= "v-er:std";
	$additional_comment = " --- morphological class must be checked";
      } elsif ($lemma =~ /ir$/) {
	$merged .= "v-ir2";
	$additional_comment = " --- morphological class must be checked";
      } else {
	$additional_comment = " --- morphological class unknown";
      }
    }
    $merged .= "\t";
    if ($lefff_entry ne "" && defined($lexicon{$lemma}{L}{$lefff_entry}{weight})) {
      $merged .= $lexicon{$lemma}{L}{$lefff_entry}{weight}.";";
    } else {
      $merged .= "100;";
    }    
    $scat = scatsmerge(\@allscats);
    $merged .= longest(join(",,",@allpreds)).";v;$scat;".uniqsort(join(",",@allmacros)).";".restrsmerge(\@allrestrs)."\t# $group";
    if ($lefff_entry ne "" && $lexicon{$lemma}{L}{$lefff_entry}{comment} ne "") {
      $merged .= " --- ".$lexicon{$lemma}{L}{$lefff_entry}{comment};
    }
    if ($dv_entry ne "" && $lexicon{$lemma}{D}{$dv_entry}{comment} ne "") {
      $merged .= " --- DVex: ".$lexicon{$lemma}{D}{$dv_entry}{comment};
    }
    if ($m_entry ne "" && $lexicon{$lemma}{M}{$m_entry}{comment} ne "") {
      $merged .= " --- M: ".$lexicon{$lemma}{M}{$m_entry}{comment};
    }
    if ($lvf_entry ne "" && $lexicon{$lemma}{LVF}{$lvf_entry}{comment} ne "") {
      $merged .= " --- LVF: ".$lexicon{$lemma}{LVF}{$lvf_entry}{comment};
    }
    $merged .= $additional_comment;
    if ($scat eq "<>" && $filter_entries_with_no_subcat) {
      $merged .= "\t\tMERGED BUT UNUSABLE\t$merged\n";
    } else {
      print $merged."\n";
      $merged .= "\t\tMERGED\t$merged\n";
    }
    if ($verbose) {
      print STDERR "$merged\n";
    }
  }
}

sub build_links {
    my $s = shift;
    my $e = shift;
    my @r;
    my $didsomething = 0;
    if ($s eq "0") {
	return ();
    }
#    print STDERR "\n<$s\__$e: ".(scalar keys %{$is_included_in{$s}{$e}}).">\n";
    if (defined($is_included_in{$s}{$e})) {
	    for my $s2 (sort keys %{$is_included_in{$s}{$e}}) {
		next if $s2 eq "0";
		if (scalar keys %{$is_included_in{$s}{$e}{$s2}} > 0) {
#		    print STDERR ":$s2\n";
		    for my $e2 (sort keys %{$is_included_in{$s}{$e}{$s2}}) {
#			print STDERR "e2:$e2\n";
			for my $tail (build_links ($s2, $e2)) {
			    push(@r, "$s\__$e $tail");
			    $didsomething = 1;
			}
		    }
		}
	    }
    }
    if ($didsomething == 0) {
	push(@r, "$s\__$e");
#	print STDERR ":\n";
    }
    return @r;
}

sub scatsmerge {
    my $scats = shift;
    my @tmp=();
    for (@{$scats}) {
	/^<(.*)>$/;
	if ($1 eq "arg1,arg2") {return "<arg1,arg2>"}
	push(@tmp,$1);
    }
    $output="";
    for $func ("Suj","Obj","Objde","Objà","Obja","Loc","Dloc","Att","Obl","Obl2") {
      @tmp2=();
      $fac = 0;
      for (@tmp) {
	if (/$func:(\(?)([^\(\)]*?)\)?(,|$)/) {
	  $fac = $fac || ($1 eq "(");
	  push(@tmp2,$2);
	}
      }
      if (@tmp2) {
	$output .= ",$func:";
	if ($fac == 1) {
	  $output .= "(";
	}
	$output .= realuniqsort(join("|",@tmp2));
	if ($fac == 1) {
	  $output .= ")";
	}
      }
    }
    $output=~s/^,//;
    if ($output ne "") {
      return "<".$output.">";
    }
    return "";
}

sub restrsmerge {
    my $restrs = shift;
    my $retval = uniqsort(join(", ",@{$restrs}));
    if ($retval eq "") {$retval = "%default"}
    return $retval;
}

sub uniqsort {
    my $string = shift;
    my %words = ();
    $string =~ s/^, ?//;
    for (split(/, ?/,$string)) {
	$words{$_}=1 unless ($_ eq "*" || $_ eq "");
    }
    return join(",", sort keys %words);
}

sub realuniqsort {
    my $string = shift;
    my %words = ();
    $string =~ s/^\|//;
    for (split(/\|/,$string)) {
	s/__(.+)$//;
	$words{$_}.=$1 unless ($_ eq "*" || $words{$_} =~ /$1/);
    }
    my $output = "";
    for (keys %words) {
      if ($keep_sources) {
	$output .= "|$_\__$words{$_}";
      } else {
	$output .= "|$_";
      }
    }
    $output=~s/__DL//g;
    $output=~s/__LD//g;
    $output=~s/__L?DL?LVFL?//g;
    $output=~s/__L?LVFL?DL?//g;
    $output =~ s/^\|//;
    return $output;
}

sub longest {
    my $string = shift;
    my $output = "";
    for (split(/,,/,$string)) {
	if (length($output) < length($_)) {
	    $output = $_;
	}
    }
    return $output;
}
