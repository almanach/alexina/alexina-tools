#ifndef LEXEDTYPES_H
#define LEXEDTYPES_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

typedef unsigned long int TYPEPTR;

struct FSA {
  TYPEPTR son;
  TYPEPTR brother;
  TYPEPTR info;
  int car;
};

struct InfoBuff {
  TYPEPTR next;
  TYPEPTR offset;
};

struct FindReplaceStructure {
  char *find;
  char *replace;
};

// elementary result of a list operation
struct list_item {
  long int word_offset;
  char *info;
};

// global result of a list operation
struct list_result {
  list_item *items;
  char *words;
  long int items_size;
  long int items_max_size;
  long int words_size;
  long int words_max_size;
};


#endif
