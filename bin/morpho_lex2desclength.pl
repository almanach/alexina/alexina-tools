#!/usr/bin/env perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

use strict;

my $model = shift || die "Please provide an Alexina morphological description";
my $lexicon = shift || die "Please provide an Alexina morphological lexicon";

my ($codedlexicon, $inflinfo, $stemtableandstems, $canform, $stems, $stemid, $mfs, $suppletiveforms);
my (%canform, %stempattern, %stems, %variants, %pattern, %stemtable, %stemslist, %suppletiveforms);
my (%pattern_occ, %variants_occ, %canform_char_occ, %stem_char_occ, %regexp_char_occ, %letterclasses_occ, %model_occ, %stemid_occ, %translitt_char_occ, %mfs_occ, %operations_occ);

my $tmp;

open LEXICON, "<$lexicon" || die "Could not open $lexicon: $!";
binmode LEXICON, ":utf8";
my $line = 0;
my $entries = 0;
while (<LEXICON>) {
  $line++;
  chomp;
  s/(^|[^\\])#.*//;
  next if (/^\s*$/);
  /^(.+?)\t(.+?)(\t|$)/ || die "Error in lexicon on line $line: $_";
  $entries++;
  $canform = $1;
  $inflinfo = $2;
  $canform =~ s/___.*//;
  $codedlexicon .= "$canform#";
  for (split //, "$canform#") {
    $canform_char_occ{$_}++;
    $canform_char_occ{__ALL__}++;
  }
  if ($inflinfo =~ s/^(.*?)\/([^\/]*)/\1/) {
    $stems = $2;
  } else {
    $stems = "";
  }
  if ($inflinfo =~ s/^(.*?)\/([^\/]*)/\1/) {
    $suppletiveforms = $2;
  } else {
    $suppletiveforms = "";
  }
  if ($inflinfo =~ s/^(.*?)\/([^\/]*)/\1/) {
    $mfs = $1;
  } else {
    $mfs = "";
  }
  $inflinfo =~ s/^([^:]+):?// || die "$inflinfo";
  $codedlexicon .= "\[::PATTERN=".$1."::\]";
  $pattern_occ{"\[::PATTERN=".$1."::\]"}++;
  $pattern_occ{__ALL__}++;
  while ($inflinfo =~ s/^([^:\/]+)(:|$)//) {
    $codedlexicon .= "\[::VARIANT=".$1."::\]";
    $variants_occ{"\[::VARIANT=".$1."::\]"}++;
    $variants_occ{__ALL__}++;
  }
  die $inflinfo unless $inflinfo eq "";
  $codedlexicon .= "\[::VARIANTS_END::\]";
  $variants_occ{"\[::VARIANTS_END::\]"}++;
  $variants_occ{__ALL__}++;
  if ($mfs ne "") {
    $codedlexicon.= "[::MFSSET=".$mfs."::]";
    $mfs_occ{"[::MFSSET=".$mfs."::]"}++;
    $mfs_occ{__ALL__}++;    
  }
  %suppletiveforms = ();
  for (split /,/, $suppletiveforms) {
    /^(.+?)=(.+)$/ || die $_;
    $suppletiveforms{$1} = $2;
  }
  for (sort keys %suppletiveforms) {
    my $bool = 1;
    for my $f (split /\|/, $_) {
      if ($bool == 1) {$bool = 0} else {
	$codedlexicon .= "[::MFS_OR::]";
	$mfs_occ{"\[::MFS_OR::\]"}++;
	$mfs_occ{__ALL__}++;
      }
      for (split /\./, $f) {
	$codedlexicon .= "[::MFSFEAT=".$_."::]";
	$mfs_occ{"\[::MFSFEAT=".$_."::\]"}++;
	$mfs_occ{__ALL__}++;
      }
    }
    $mfs_occ{"\[::MFSBNDRY::\]"}++;
    $mfs_occ{__ALL__}++;
    $codedlexicon .= "[::MFSBNDRY::]";
    for (split //, $suppletiveforms{$_}."#") {
      $stem_char_occ{$_}++;
      $stem_char_occ{__ALL__}++;
    }
    $codedlexicon.= $suppletiveforms{$_}."#";
  }
  $codedlexicon.= "[::MFSBNDRY::]";
  $mfs_occ{"[::MFSBNDRY::]"}++;
  $mfs_occ{__ALL__}++;
  $stemid = 0;
  %stems = ();
  for (split /,/, $stems) {
    $stems{$stemid} = $_ unless ($_ eq "");
    $stemid++;
  }
  my $stemsequence = "";
  for (sort keys %stems) {
    $codedlexicon.= "[::STEMID=".$_."::]";
    $stemid_occ{"[::STEMID=".$_."::]"}++;
    $stemid_occ{__ALL__}++;
    my $stem = $stems{$_}."#"; # le dièse modélise le fin-de-stem, car on concatène tt ça
    $stemsequence .= "$stem";
    for (split //, $stem) {
      $stem_char_occ{$_}++;
      $stem_char_occ{__ALL__}++;
    }
  }
  $codedlexicon.= "[::STEMID_END::]$stemsequence";
  $stemid_occ{"[::STEMID_END::]"}++;
  $stemid_occ{__ALL__}++;
  $codedlexicon.="\n";
}
close LEXICON;

open MODEL, "<$model" || die "Could not open $model: $!";
binmode MODEL, ":utf8";
my (%model,%translitt);
$line = 0;
while (<MODEL>) {
  $line++;
  chomp;
  if (/<translitteration source="(.*?)" target="(.*?)"/) {
    $translitt{$1} = $2;
    $model{translitt} .= "$1#$2#";
    for (split //, "$1#$2#") {
      $translitt_char_occ{$_}++;
      $translitt_char_occ{__ALL__}++;
    }
  }
}
close MODEL;
open MODEL, "<$model" || die "Could not open $model: $!";
binmode MODEL, ":utf8";
$line = 0;
my $whereami = "";
while (<MODEL>) {
  $line++;
  chomp;
  s/\s*=\s*/=/g;
  if (/<(?:\?xml|\/?description|displayedform|canonicalform)/) { ### canonicalform?
  } elsif (/letterclass name="(.*?)" letters="(.*?)"/) {
    my $name = translitterate($1);
    my $letters = $2;
    $letters = translitterate($letters);
    $letters =~ s/ //g;
    $model{letterclasses} .= "[:".$name.":]".$letters;
    $regexp_char_occ{"[:".$name.":]"}++;
    $regexp_char_occ{__ALL__}++;
    for (split //, $letters) {
      $regexp_char_occ{$_}++;
      $regexp_char_occ{__ALL__}++;
    }
  } elsif (/<pairs l1="(.*?)" l2="(.*?)"/) {
    my $source = translitterate($1);
    my $target = translitterate($2);
    $model{letterclasses} .= "#[:".$source.":][:".$target.":]";
  } elsif (/<\/pairs/) {
  } elsif (/<pair l1="(.*?)" l2="(.*?)"/) {
    my $source = translitterate($1);
    my $target = translitterate($2);
    $model{letterclasses} .= $source.$target;
  } elsif (/(?:sandhi|fusion) source="(.*?)" target="(.*?)"/) {
    my $source = $1;
    my $target = $2;
    $source =~ s/^#/^/;
    $target =~ s/^#/^/;
    $source =~ s/#$/\$/;
    $target =~ s/#$/\$/;
    $model{sandhis} .= translitterate($source)."#".translitterate($target)."#";
  } elsif (/mfsbundle name="(.*?)" features="(.*)"/) {
    my $features = $2;
    $model{mfs} .= "[::MFSBUNDLE=".$1."::]";
    $mfs_occ{"\[::MFSBUNDLE=".$1."::\]"}++;
    $mfs_occ{__ALL__}++;
    if (/ morphomic="(?:true|1)"/) {
      $model{mfs} .= "[::IS_MORPHOMIC::]";
      $mfs_occ{"\[::IS_MORPHOMIC::\]"}++;
      $mfs_occ{__ALL__}++;
    }
    for (split /\|/, $features) {
      $model{mfs} .= "[::MFSFEAT=".$_."::]";
      $mfs_occ{"\[::MFSFEAT=".$_."::\]"}++;
      $mfs_occ{__ALL__}++;
    }
  } elsif (/mfsexclusion features="(.*?)" excludes="(.*)"/) {
    my $features = $1;
    my $excludes = $1;
    $model{mfs} .= "[::MFSEXCLUSION::]";
    $mfs_occ{"\[::MFSEXCLUSION::\]"}++;
    $mfs_occ{__ALL__}++;
    add_feats($features,"mfs",1);
    add_feats($excludes,"mfs");
  } elsif (/invalidfeatureset features="(.*)"/) {
    my $features = $1;
    $model{mfs} .= "[::INVALIDFEATSET::]";
    $mfs_occ{"\[::INVALIDFEATSET::\]"}++;
    $mfs_occ{__ALL__}++;
    add_feats($features,"mfs");
  } elsif (/<featureset name="(.*?)" (?:exist|features)="(.*?)"(?: (?:features?_?)?except="(.*?)")?/) {
    my $exist = $2;
    my $except = $3;
    $model{mfs} .= "[::MFSSET=".$1."::]";
    $mfs_occ{"\[::MFSSET=".$1."::\]"}++;
    $mfs_occ{__ALL__}++;
    add_feats($exist,"mfs",1);
    add_feats($except,"mfs");
  } elsif (/<partitionspace name="(.*?)"(?: (?:exist|features)="(.*?)")?(?: (?:features?_?)?except="(.*?)")?/) {
    my $exist = $2;
    my $except = $3;
    $model{mfs} .= "[::PSPACE=".$1."::]";
    $mfs_occ{"\[::PSPACE=".$1."::\]"}++;
    $mfs_occ{__ALL__}++;
    add_feats($exist,"mfs",1);
    add_feats($except,"mfs");
  } elsif (/<operation_definition name="(.+?)">/) {
    $model{operations} .= "[::OPERATION=".$1."::]";
    $operations_occ{"\[::OPERATION=".$1."::\]"}++;
    $operations_occ{__ALL__}++;
  } elsif (/<\/operation_definition/) {
  } elsif (/replace source="(.*?)" target="(.*?)"/) {
    my $source = $1;
    my $target = $2;
    $source =~ s/^#/^/;
    $target =~ s/^#/^/;
    $source =~ s/#$/\$/;
    $target =~ s/#$/\$/;
    if (/ stop="-"/) {
      $model{operations} .= "[::NO_STOP::]";
      $operations_occ{"\[::NO_STOP::\]"}++;
      $operations_occ{__ALL__}++;
    }
    $model{operations} .= "[::REPLACE::]";
    $operations_occ{"\[::REPLACE::\]"}++;
    $operations_occ{__ALL__}++;
    add_rads_and_vars($_,"operations");
    $tmp = translitterate($source)."#".translitterate($target)."#";
    $model{operations} .= $tmp;
    while ($tmp =~ s/^(\[\d*:[^:\]]*:\]|\\.|.)//) {
      $regexp_char_occ{$1}++;
      $regexp_char_occ{__ALL__}++;
    }
  } elsif (/insert pattern="(.*?)"/) {
    my $source = $1;
    if (/ stop="-"/) {
      $model{operations} .= "[::NO_STOP::]";
      $operations_occ{"\[::NO_STOP::\]"}++;
      $operations_occ{__ALL__}++;
    }
    $model{operations} .= "[::INSERT::]";
    $operations_occ{"\[::INSERT::\]"}++;
    $operations_occ{__ALL__}++;
    add_rads_and_vars($_,"operations");
    $source =~ s/^#/^/;
    $source =~ s/#$/\$/;
    $tmp = translitterate($source)."#";
    $model{operations} .= $tmp;
    while ($tmp =~ s/^(\[\d*:[^:\]]*:\]|\\.|.)//) {
      $regexp_char_occ{$1}++;
      $regexp_char_occ{__ALL__}++;
    }
  } elsif (/identity/) {
    if (/ stop="-"/) {
      $model{operations} .= "[::NO_STOP::]";
      $operations_occ{"\[::NO_STOP::\]"}++;
      $operations_occ{__ALL__}++;
    }
    $model{operations} .= "[::IDENTITY::]";
    $operations_occ{"\[::IDENTITY::\]"}++;
    $operations_occ{__ALL__}++;
    add_rads_and_vars($_,"operations");
  } elsif (/<pattern name="(.*?)"/) {
    $model{patterns} .= "[::PATTERN=".$1."::]";
    $pattern_occ{"[::PATTERN=".$1."::]"}++;
    $pattern_occ{__ALL__}++;
  } elsif (/<subpattern/) {
    $model{patterns} .= "[::SUBPATTERN::]";
    $model_occ{"[::SUBPATTERN::]"}++;
    $model_occ{__ALL__}++;
  } elsif (/<\/subpattern/) {
    $model{patterns} .= "[::SUBPATTERN_END::]";
    $model_occ{"[::SUBPATTERN_END::]"}++;
    $model_occ{__ALL__}++;
  } elsif (/<(?:toto|realzone) level="(.*?)"/) {
    $model{patterns} .= "[::LEVEL=".$1."::]";
    $model_occ{"[::LEVEL=".$1."::]"}++;
    $model_occ{__ALL__}++;
    if (/ on_failure="skip"/) {
      $model{patterns} .= "[::SKIP_ON_FAILURE::]";
      $model_occ{"[::SKIP_ON_FAILURE::]"}++;
      $model_occ{__ALL__}++;
    }
    if (/ transfer="(.*?)"/) {
      $model{patterns} .= "[::TRANSFER=$1::]";
      $model_occ{"[::TRANSFER=$1::]"}++;
      $model_occ{__ALL__}++;
    }
    if (/ table="(.*?)"/) {
      $model{patterns} .= "[::TABLE=$1::]";
      $model_occ{"[::TABLE=".$1."::]"}++;
      $model_occ{__ALL__}++;
    } elsif (/ zone="(.*?)"/) {
      $model{patterns} .= "[::ZONE=".$1."::]";
      $model_occ{"[::ZONE=".$1."::]"}++;
      $model_occ{__ALL__}++;
    } else {
      die;
    }
    add_ps($_,"patterns");
    add_rads_and_vars($_,"patterns");
  } elsif (/<\/pattern/) {
    $model{patterns} .= "[::PATTERN_END::]\n";
    $model_occ{"[::PATTERN_END::]"}++;
    $model_occ{__ALL__}++;
  } elsif (/<level type="(.*?)" level="(.*?)"/) {
    $model{tables} .= "\n[::LEVEL=".$2."::]";
    $model_occ{"[::LEVEL=".$2."::]"}++;
    $model_occ{__ALL__}++;
    $model{tables} .= "[::LEVELTYPE=".$1."::]\n";
    $model_occ{"[::LEVELTYPE=".$1."::]"}++;
    $model_occ{__ALL__}++;
  } elsif (/<\/level/) {
  } elsif (/<\/zone/) {
  } elsif (/<zoneref name="(.*?)"/) {
    $model{tables} .= "\n" unless $model{tables} =~ /\n$/;
    $model{tables} .= "[::ZONEREF=".$1."::]\n";
    $model_occ{"[::ZONEREF=".$1."::]"}++;
    $model_occ{__ALL__}++;
  } elsif (/<zone name="(.*?)"(?: features="(.*?)")?(?: (?:(?:features?_)?except|exclude)="(.*?)")?/) {
    my $features = $2;
    my $except = $3;
    my $name = $1;
    $model{tables} .= "\n" unless $model{tables} =~ /\n$/;
    $model{tables} .= "[::ZONE=".$name."::]";
    $model_occ{"[::ZONE=".$name."::]"}++;
    $model_occ{__ALL__}++;
    add_feats($features,"tables",1);
    add_feats($except,"tables");
    add_rads_and_vars($_,"tables");
  } elsif (/table name="(.*?)"/) {
    $model{tables} .= "[::TABLE=".$1."::]";
    $model_occ{"[::TABLE=".$1."::]"}++;
    $model_occ{__ALL__}++;
    add_rads_and_vars($_,"tables");
  } elsif (/<\/table/) {
    $model{tables} .= "\n[::TABLE_END::]\n";
    $model_occ{"[::TABLE_END::]"}++;
    $model_occ{__ALL__}++;
  } elsif (/<(?:item|form) /) {
    if (/ like=/) {
      next;
    } elsif (/name="(.*?)"/) {
      $model{tables} .= "[::ZONEITEM::]";
      $model_occ{"[::ZONEITEM::]"}++;
      $model_occ{__ALL__}++;
      $model{tables} .= "[::ZONE=".$1."::]";
      $model_occ{"[::ZONE=".$1."::]"}++;
      $model_occ{__ALL__}++;
      if (/source="(.*?)"/) {
	$model{tables} .= "[::ZONE=".$1."::]";
	$model_occ{"[::ZONE=".$1."::]"}++;
	$model_occ{__ALL__}++;
      } else {
	$model{tables} .= "[::ZONE=::]";
	$model_occ{"[::ZONE=::]"}++;
	$model_occ{__ALL__}++;
      }
    } elsif (/(?:features|tag)="(.*?)"/) {
      my $except = "";
      $model{tables} .= "[::STDITEM::]";
      $model_occ{"[::STDITEM::]"}++;
      $model_occ{__ALL__}++;
      my $features = $1;
      if (/(?:features?_)(?:except|exclude)="(.*?)"/) {
	$except = $1;
      }
      add_feats($features,"tables",1);
      add_feats($except,"tables");
    } else {
      $model{tables} .= "[::ZONEITEM::]";
      $model_occ{"[::ZONEITEM::]"}++;
      $model_occ{__ALL__}++;
      $model{tables} .= "[::ZONE=::]";
      $model_occ{"[::ZONE=::]"}++;
      $model_occ{__ALL__}++;
      if (/source="(.*?)"/) {
	$model{tables} .= "[::ZONE=::]";
	$model_occ{"[::ZONE=".$1."::]"}++;
	$model_occ{__ALL__}++;
      } else {
	$model{tables} .= "[::ZONE=::]";
	$model_occ{"[::ZONE=::]"}++;
	$model_occ{__ALL__}++;
      }
    }
    s/ operation="\+(.*?)"/ append="\1"/;
    if (/ operation=""/) {
      $model{tables} .= "[::IDENTITY::]";
      $operations_occ{"[::IDENTITY::]"}++;
      $operations_occ{__ALL__}++;
    } elsif (/ operation="(.+?)"/) {
      my $args = "";
      my $operations = $1;
      my $nops = ($operations =~ s/\+/\+/g) + 1;
      my $operation;
      if ($nops == 1) {
	$operation = $operations;
	if ($operation =~ s/\((.*)\)$//) {
	  $args = $1;
	}
	$model{tables} .= "[::OPERATION=".$operation."::]";
	$operations_occ{"[::OPERATION=".$operation."::]"}++;
	$operations_occ{__ALL__}++;
	$model{tables} .= translitterate($args);
	$tmp = translitterate($args);
	while ($tmp =~ s/^(\[\d*:[^:\]]*:\]|\\.|.)//) {
	  $regexp_char_occ{$1}++;
	  $regexp_char_occ{__ALL__}++;
	}
	$model{tables} .= "#";
	$regexp_char_occ{"#"}++;
	$regexp_char_occ{__ALL__}++;
      } else {
	$model{tables} .= "[::MULTIOPERATION=".$nops."::]";
	$operations_occ{"[::MULTIOPERATION=".$nops."::]"}++;
	$operations_occ{__ALL__}++;
	for $operation (split /\+/, $operations) {
	  if ($operation =~ s/\((.*)\)$//) {
	    $args = $1;
	  }
	  $model{tables} .= "[::OPERATION=".$operation."::]";
	  $operations_occ{"[::OPERATION=".$operation."::]"}++;
	  $operations_occ{__ALL__}++;
	  $model{tables} .= translitterate($args);
	  $tmp = translitterate($args);
	  while ($tmp =~ s/^(\[\d*:[^:\]]*:\]|\\.|.)//) {
	    $regexp_char_occ{$1}++;
	    $regexp_char_occ{__ALL__}++;
	  }
	  $model{tables} .= "#";
	  $regexp_char_occ{"#"}++;
	  $regexp_char_occ{__ALL__}++;
	}
      }
    } elsif (/(append|prepend|suffix|prefix)="(.*?)"/) {
      my $op = $1;
      $op = "append" if $op eq "suffix";
      $op = "prepend" if $op eq "prefix";
      $model{tables} .= "[::".uc($op)."::]";
      $operations_occ{"[::".uc($op)."::]"}++;
      $operations_occ{__ALL__}++;
      $model{tables} .= translitterate($2);
      $tmp = translitterate($2);
      while ($tmp =~ s/^(\[\d*:[^:\]]*:\]|\\.|.)//) {
	$regexp_char_occ{$1}++;
	$regexp_char_occ{__ALL__}++;
      }
      $model{tables} .= "#";
      $regexp_char_occ{"#"}++;
      $regexp_char_occ{__ALL__}++;
    } elsif (/<item name="[^"]*"\/>/) {
      $model{tables} .= "[::IDENTITY::]";
      $operations_occ{"[::IDENTITY::]"}++;
      $operations_occ{__ALL__}++;
    } else {
      die $_;
    }
    add_rads_and_vars($_,"tables");
  } elsif (/<transfer /) {
    / name="(.*?)"/ || die "Please provide a name for the transfer function: $_\n";
    $model{patterns} .= "[::TRANSFER=$1::]";
    $model_occ{"[::TRANSFER=$1::]"}++;
    $model_occ{__ALL__}++;
  } elsif (/<\/transfer/) {
    $model{patterns} .= "\n";
  } elsif (/<transfer_?rule/) {
    / source="(.*?)"/ || die "Please provide a source for the transfer rule: $_\n";
    my $source = $1;
    / target="(.*?)"/ || die "Please provide a target for the transfer rule: $_\n";
    my $target = $1;
    for my $f (split /\|/, $source) {
      for (split /\./, $f) {
	$model{patterns} .= "[::MFSFEAT=".$_."::]";
	$mfs_occ{"\[::MFSFEAT=".$_."::\]"}++;
	$mfs_occ{__ALL__}++;
      }
      $model{patterns} .= "[::MFS_OR::]";
      $mfs_occ{"\[::MFS_OR::\]"}++;
      $mfs_occ{__ALL__}++;
    }
    if ($model{patterns} =~ s/\[::MFS_OR::\]$//) {
      $mfs_occ{"\[::MFS_OR::\]"}--;
      $mfs_occ{__ALL__}--;
    }
    $model{patterns} .= "[::MFSBNDRY::]";
    $mfs_occ{"\[::MFSBNDRY::\]"}++;
    $mfs_occ{__ALL__}++;
    for (split /\./, $target) {
      $model{patterns} .= "[::MFSFEAT=".$_."::]";
      $mfs_occ{"\[::MFSFEAT=".$_."::\]"}++;
      $mfs_occ{__ALL__}++;
    }
    $model{patterns} .= "[::MFSBNDRY::]";
    $mfs_occ{"\[::MFSBNDRY::\]"}++;
    $mfs_occ{__ALL__}++;
  } else {
    print STDERR "Ignored line: $_\n" unless (/^\s*$/ || /<translitteration / || /\s*<!--/);
  }
}
$tmp = $model{letterclasses};
while ($tmp =~ s/^(\[:.*?:\]|\\.|.)//) {
  $letterclasses_occ{$_}++;
  $letterclasses_occ{__ALL__}++;
}
$tmp = $model{sandhis};
while ($tmp =~ s/^(\[\d*:[^:\]]*:\]|\\.|.)//) {
  $regexp_char_occ{$1}++;
  $regexp_char_occ{__ALL__}++;
}
#for (keys %variants_occ) {
#  print STDERR "$_\t$variants_occ{$_}\n";
#}

my ($lexicon_DL, $model_DL, %DL, %DL_add);
print STDERR "The lexicon contains $entries entries\n";
for (split /\n/, $codedlexicon) {
  next if /^$/;
  while (s/^([^#])//) {
#    $lexicon_DL -= log($canform_char_occ{$1}/$canform_char_occ{__ALL__});    
  }
  s/^#// || die;
#  $lexicon_DL -= log($canform_char_occ{"#"}/$canform_char_occ{__ALL__});    
  s/^(\[::PATTERN.*?::\])// || die;
  $lexicon_DL -= log($pattern_occ{$1}/$pattern_occ{__ALL__});
  $DL{pattern_lex} -= log($pattern_occ{$1}/$pattern_occ{__ALL__});
  while (s/^(\[::VARIANT=.*?::\])//) {
    $lexicon_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
    $DL{variant_lex} -= log($variants_occ{$1}/$variants_occ{__ALL__});
  }
  s/^(\[::VARIANTS_END::\])// || die $_;
  $lexicon_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
  $DL{variant_lex} -= log($variants_occ{$1}/$variants_occ{__ALL__});
  s/^(.*)(\[::STEMID_END::\])// || die;
  my $stems = $1;
  $lexicon_DL -= log($stemid_occ{$2}/$stemid_occ{__ALL__});
  $DL{suppletion_lex} -= log($stemid_occ{$2}/$stemid_occ{__ALL__});

  while ($stems !~ s/^(\[::MFSBNDRY::\])//) {
    if ($stems =~ s/^(\[::MFSSET=.*?::\])//) {
      $lexicon_DL -= log($mfs_occ{$1}/$mfs_occ{__ALL__});
      $DL{suppletion_lex} -= log($mfs_occ{$1}/$mfs_occ{__ALL__});
    } else {
      while ($stems =~ s/^(\[::MFSFEAT=.*?::\]|\[::MFS_(?:OR|[LR]B)::\])//) {
	$lexicon_DL -= log($mfs_occ{$1}/$mfs_occ{__ALL__});
	$DL{suppletion_lex} -= log($mfs_occ{$1}/$mfs_occ{__ALL__});
      }
      $stems =~ s/^\[::MFSBNDRY::\]// || die $stems;
      $lexicon_DL -= log($mfs_occ{"[::MFSBNDRY::]"}/$mfs_occ{__ALL__});
      $DL{suppletion_lex} -= log($mfs_occ{"[::MFSBNDRY::]"}/$mfs_occ{__ALL__});
      while ($stems =~ s/^([^#])//) {
	$lexicon_DL -= log($stem_char_occ{$1}/$stem_char_occ{__ALL__});    
	$DL{suppletion_lex} -= log($stem_char_occ{$1}/$stem_char_occ{__ALL__});    
      }
      $stems =~ s/^(#)// || die $stems;
      $lexicon_DL -= log($canform_char_occ{$1}/$canform_char_occ{__ALL__});    
    }
  }
  $lexicon_DL -= log($mfs_occ{"[::MFSBNDRY::]"}/$mfs_occ{__ALL__});
  $DL{suppletion_lex} -= log($mfs_occ{"[::MFSBNDRY::]"}/$mfs_occ{__ALL__});

  while ($stems =~ s/^(\[::STEMID=.*?::\])//) {
    $lexicon_DL -= log($stemid_occ{$1}/$stemid_occ{__ALL__});
    $DL{suppletion_lex} -= log($stemid_occ{$1}/$stemid_occ{__ALL__});
  }
  die unless $stems eq "";
  while (s/^([^\[])//) {
    $lexicon_DL -= log($stem_char_occ{$1}/$stem_char_occ{__ALL__});    
    $DL{suppletion_lex} -= log($stem_char_occ{$1}/$stem_char_occ{__ALL__});    
  }
  die $_ if $_ ne "";
}

print STDERR "Lexicon description length = $lexicon_DL\n";
for (keys %DL) {
  print STDERR "  $_ = $DL{$_}\n";
}

%DL = ();


while ($model{letterclasses} =~ s/^(\[:.*?:\]|\\.|.)//) {
  $model_DL -= log($regexp_char_occ{$1}/$regexp_char_occ{__ALL__});    
  $DL{"phono+morphono"} -= log($regexp_char_occ{$1}/$regexp_char_occ{__ALL__});    
}
$tmp = $model{sandhis};
while ($tmp =~ s/^(\[\d*:[^:\]]*:\]|\\.|.)//) {
  $model_DL -= log($regexp_char_occ{$1}/$regexp_char_occ{__ALL__});    
  $DL{"phono+morphono"} -= log($regexp_char_occ{$1}/$regexp_char_occ{__ALL__});    
}
$tmp = $model{operations};
my $in_rads = 0;
while ($tmp =~ s/^(\[\d*:[^\]]*:\]|\\.|.)//) {
  my $char = $1;
  die $char.$tmp if ($char =~ /^\[::/ && $in_rads != 0);
  if ($char =~ /^\[::NO_STOP/) {
    $model_DL -= log($operations_occ{$char}/$operations_occ{__ALL__});    
    $DL{operations} -= log($operations_occ{$char}/$operations_occ{__ALL__});    
  } elsif ($char =~ /^\[::(?:INSERT|IDENTITY|REPLACE)/) {
    $model_DL -= log($operations_occ{$char}/$operations_occ{__ALL__});    
    $DL{operations} -= log($operations_occ{$char}/$operations_occ{__ALL__});    
    $in_rads = 1;
  } elsif ($in_rads > 0 && $char eq "#") {
    $in_rads = 3;
    $model_DL -= log($regexp_char_occ{$char}/$regexp_char_occ{__ALL__});    
    $DL{operations} -= log($regexp_char_occ{$char}/$regexp_char_occ{__ALL__});    
    $DL_add{rads} -= log($regexp_char_occ{$char}/$regexp_char_occ{__ALL__});    
  } elsif ($in_rads == 1 && $char eq "§") {
    $in_rads = 2;
    $model_DL -= log($regexp_char_occ{$char}/$regexp_char_occ{__ALL__});    
    $DL{operations} -= log($regexp_char_occ{$char}/$regexp_char_occ{__ALL__});    
    $DL_add{rads} -= log($regexp_char_occ{$char}/$regexp_char_occ{__ALL__});    
  } elsif ($in_rads > 0) {
    $model_DL -= log($regexp_char_occ{$char}/$regexp_char_occ{__ALL__});    
    $DL{operations} -= log($regexp_char_occ{$char}/$regexp_char_occ{__ALL__});    
    $DL_add{rads} -= log($regexp_char_occ{$char}/$regexp_char_occ{__ALL__});    
  } elsif ($char =~ /^\[::/) {
    $model_DL -= log($operations_occ{$char}/$operations_occ{__ALL__});    
    $DL{operations} -= log($operations_occ{$char}/$operations_occ{__ALL__});    
  } else {
    $model_DL -= log($regexp_char_occ{$char}/$regexp_char_occ{__ALL__});    
    $DL{operations} -= log($regexp_char_occ{$char}/$regexp_char_occ{__ALL__});    
  }
  if ($in_rads == 3) {
    if ($tmp =~ s/^(\[::VARIANTS_END::\])//) {
      $model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
      $DL{operations} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
      $DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
    } else {
      while ($tmp =~ s/^(\[::VARIANT=.*?::\])//) {
	$model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
	$DL{operations} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	$DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
      }
      die unless ($tmp =~ s/^(\[::VARIANTS_BNDRY::\])//);
      $model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
      $DL{operations} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
      $DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
      while ($tmp =~ s/^(\[::VARIANT=.*?::\])//) {
	$model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
	$DL{operations} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	$DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
      }
      die unless ($tmp =~ s/^(\[::VARIANTS_END::\])//);
      $model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
      $DL{operations} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
      $DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
    }
    $in_rads = 0;
  }
}
die if ($in_rads != 0);
$tmp = $model{mfs};
while ($tmp =~ s/^(\[:.*?:\])//) {
  $model_DL -= log($mfs_occ{$1}/$mfs_occ{__ALL__});    
  $DL{mfs} -= log($mfs_occ{$1}/$mfs_occ{__ALL__});    
}
while ($model{translitt} =~ s/^(.)//) {
  $model_DL -= log($translitt_char_occ{$1}/$translitt_char_occ{__ALL__});    
  $DL{"phono+morphono"} -= log($translitt_char_occ{$1}/$translitt_char_occ{__ALL__});    
}
for (split /\n/, $model{patterns}) {
  next if /^$/;
  if (s/^(\[::TRANSFER=.*?::\])//) {
    $model_DL -= log($model_occ{$1}/$model_occ{__ALL__});
    $DL{mfs} -= log($model_occ{$1}/$model_occ{__ALL__});
    while (/\[::MFSFEAT=/) {
      while (s/^(\[::(?:MFSFEAT=.*?|MFS_(?:OR|[LR]B))::\])//) {
	$model_DL -= log($mfs_occ{$1}/$mfs_occ{__ALL__});
	$DL{mfs} -= log($mfs_occ{$1}/$mfs_occ{__ALL__});
      }
      s/^(\[::MFSBNDRY::\])// || die $_;    
      $model_DL -= log($mfs_occ{$1}/$mfs_occ{__ALL__});
      $DL{mfs} -= log($mfs_occ{$1}/$mfs_occ{__ALL__});
      while (s/^(\[::(?:MFSFEAT=.*?|MFS_(?:OR|[LR]B))::\])//) {
	$model_DL -= log($mfs_occ{$1}/$mfs_occ{__ALL__});
	$DL{mfs} -= log($mfs_occ{$1}/$mfs_occ{__ALL__});
      }
      s/^(\[::MFSBNDRY::\])// || die "$_";    
      $model_DL -= log($mfs_occ{$1}/$mfs_occ{__ALL__});
      $DL{mfs} -= log($mfs_occ{$1}/$mfs_occ{__ALL__});
    }
    die "$_" unless /^$/;
    next;
  }
  s/^(\[::PATTERN=.*?::\])// || die;
  $model_DL -= log($pattern_occ{$1}/$pattern_occ{__ALL__});
  $DL{pattern} -= log($pattern_occ{$1}/$pattern_occ{__ALL__});
  while (/^\[::(SUBPATTERN|PATTERN_END)/) {
    if (s/^(\[::SUBPATTERN::\])//) {
      $model_DL -= log($model_occ{$1}/$model_occ{__ALL__});    
      $DL{pattern} -= log($model_occ{$1}/$model_occ{__ALL__});
      while (!/^\[::SUBPATTERN_END/) {
	s/^(\[::LEVEL=.*?::\])// || die $_;
	$model_DL -= log($model_occ{$1}/$model_occ{__ALL__});    
	$DL{pattern} -= log($model_occ{$1}/$model_occ{__ALL__});
	if (s/^(\[::SKIP_ON_FAILURE::\])//) {
	  $model_DL -= log($model_occ{$1}/$model_occ{__ALL__});    
	  $DL{pattern} -= log($model_occ{$1}/$model_occ{__ALL__});
	}
	if (s/^(\[::TRANSFER=.*?::\])//) {
	  $model_DL -= log($model_occ{$1}/$model_occ{__ALL__});    
	  $DL{pattern} -= log($model_occ{$1}/$model_occ{__ALL__});
	}
	if (s/^(\[::ZONE=.*?::\])//) {
	  $model_DL -= log($model_occ{$1}/$model_occ{__ALL__});    
	  $DL{pattern} -= log($model_occ{$1}/$model_occ{__ALL__});
	} elsif (s/^(\[::TABLE=.*?::\])//) {
	  $model_DL -= log($model_occ{$1}/$model_occ{__ALL__});    
	  $DL{pattern} -= log($model_occ{$1}/$model_occ{__ALL__});
	} else {
	  die;
	}
	while (!/^(\[::MFSBNDRY::\])/) {
	  die unless (s/^(\[::PSPACE=.*?::\])//);
	  $model_DL -= log($mfs_occ{$1}/$mfs_occ{__ALL__});
	  $DL{pattern} -= log($mfs_occ{$1}/$mfs_occ{__ALL__});      
	}
	s/^(\[::MFSBNDRY::\])// || die;
	$model_DL -= log($mfs_occ{$1}/$mfs_occ{__ALL__});
	$DL{pattern} -= log($mfs_occ{$1}/$mfs_occ{__ALL__});      
	while (!/^\[::VARIANT/ && s/^(\[\d*:[^\]]*:\]|\\.|.)//) {
	  $model_DL -= log($regexp_char_occ{$1}/$regexp_char_occ{__ALL__});
	  $DL{pattern} -= log($regexp_char_occ{$1}/$regexp_char_occ{__ALL__});	
	  $DL_add{rads} -= log($regexp_char_occ{$1}/$regexp_char_occ{__ALL__});	
	}
	if (s/^(\[::VARIANTS_END::\])//) {
	  $model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
	  $DL{pattern} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	  $DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	} else {
	  while (s/^(\[::VARIANT=.*?::\])//) {
	    $model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
	    $DL{pattern} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	    $DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	  }
	  die $_ unless (s/^(\[::VARIANTS_BNDRY::\])//);
	  $model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
	  $DL{pattern} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	  $DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	  while (s/^(\[::VARIANT=.*?::\])//) {
	    $model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
	    $DL{pattern} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	    $DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	}
	  die unless (s/^(\[::VARIANTS_END::\])//);
	  $model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
	  $DL{pattern} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	  $DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	}
      }
      s/^(\[::SUBPATTERN_END::\])// || die;
      $model_DL -= log($model_occ{$1}/$model_occ{__ALL__});
      $DL{pattern} -= log($model_occ{$1}/$model_occ{__ALL__});      
    } elsif (s/^(\[::PATTERN_END::\])//) {
      $model_DL -= log($model_occ{$1}/$model_occ{__ALL__});
      $DL{pattern} -= log($model_occ{$1}/$model_occ{__ALL__});
      last;
    } else {
      die;
    }
  }
  die unless $_ eq "";
}
for (split /\n/, $model{tables}) {
  next if /^$/;
  if (s/^(\[::LEVEL=.*?::\])//) {
    $model_DL -= log($model_occ{$1}/$model_occ{__ALL__});
    $DL{structure} -= log($model_occ{$1}/$model_occ{__ALL__});
    s/^(\[::LEVELTYPE=.*?::\])// || die;
    $model_DL -= log($model_occ{$1}/$model_occ{__ALL__});
    $DL{structure} -= log($model_occ{$1}/$model_occ{__ALL__});
    die unless $_ eq "";
    next;
  } elsif (s/^(\[::TABLE_END::\])//) {
    $model_DL -= log($model_occ{$1}/$model_occ{__ALL__});
    $DL{structure} -= log($model_occ{$1}/$model_occ{__ALL__});
    die unless $_ eq "";
    next;
  }
  if (s/^(\[::ZONE=.*?::\])//) {
    $model_DL -= log($model_occ{$1}/$model_occ{__ALL__});
    $DL{structure} -= log($model_occ{$1}/$model_occ{__ALL__});
    while (s/^(\[::(?:MFSFEAT=.*?|MFS_(?:OR|[LR]B))::\])//) {
      $model_DL -= log($mfs_occ{$1}/$mfs_occ{__ALL__});
      $DL{structure} -= log($mfs_occ{$1}/$mfs_occ{__ALL__});
    }
    s/^(\[::MFSBNDRY::\])// || die $_;    
    $model_DL -= log($mfs_occ{$1}/$mfs_occ{__ALL__});
    $DL{structure} -= log($mfs_occ{$1}/$mfs_occ{__ALL__});
    while (s/^(\[::(?:MFSFEAT=.*?|MFS_(?:OR|[LR]B))::\])//) {
      $model_DL -= log($mfs_occ{$1}/$mfs_occ{__ALL__});
      $DL{structure} -= log($mfs_occ{$1}/$mfs_occ{__ALL__});
    }
    s/^(\[::MFSBNDRY::\])// || die;    
    $model_DL -= log($mfs_occ{$1}/$mfs_occ{__ALL__});
    $DL{structure} -= log($mfs_occ{$1}/$mfs_occ{__ALL__});
    while (s/^(\[[^:]|\[:[^:\]].*?:\]|\\.|[^\[])//) {
      $model_DL -= log($regexp_char_occ{$1}/$regexp_char_occ{__ALL__});
      $DL{structure} -= log($regexp_char_occ{$1}/$regexp_char_occ{__ALL__});	
      $DL_add{rads} -= log($regexp_char_occ{$1}/$regexp_char_occ{__ALL__});	
    }
    if (s/^(\[::VARIANTS_END::\])//) {
      $model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
      $DL{structure} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
      $DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
    } else {
      while (s/^(\[::VARIANT=.*?::\])//) {
	$model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
	$DL{structure} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	$DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
      }
      die "\"$_\"" unless (s/^(\[::VARIANTS_BNDRY::\])//);
      $model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
      $DL{structure} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
      $DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
      while (s/^(\[::VARIANT=.*?::\])//) {
	$model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
	$DL{structure} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	$DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
      }
      die unless (s/^(\[::VARIANTS_END::\])//);
      $model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
      $DL{structure} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
      $DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
    }
  } elsif (s/^(\[::TABLE=.*?::\])//) {
    $model_DL -= log($model_occ{$1}/$model_occ{__ALL__});
    $DL{structure} -= log($model_occ{$1}/$model_occ{__ALL__});
    while (!/^\[::VARIANT/ && s/^(\[\d*:[^\]]*:\]|\\.|.)//) {#s/^(\[[^:]|\[\d*:[^:\]].*?:\]|\\.|[^\[])//) {
      $model_DL -= log($regexp_char_occ{$1}/$regexp_char_occ{__ALL__});
      $DL{structure} -= log($regexp_char_occ{$1}/$regexp_char_occ{__ALL__});	
      $DL_add{rads} -= log($regexp_char_occ{$1}/$regexp_char_occ{__ALL__});	
    }
    if (s/^(\[::VARIANTS_END::\])//) {
      $model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
      $DL{structure} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
      $DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
    } else {
      while (s/^(\[::VARIANT=.*?::\])//) {
	$model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
	$DL{structure} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	$DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
      }
      die unless (s/^(\[::VARIANTS_BNDRY::\])//);
      $model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
      $DL{structure} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
      $DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
      while (s/^(\[::VARIANT=.*?::\])//) {
	$model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
	$DL{structure} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	$DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
      }
      die unless (s/^(\[::VARIANTS_END::\])//);
      $model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
      $DL{structure} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
      $DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
    }
  } elsif (s/^(\[::ZONEREF=.*?::\])//) {
    $model_DL -= log($model_occ{$1}/$model_occ{__ALL__});
    $DL{structure} -= log($model_occ{$1}/$model_occ{__ALL__});
  } else {
    die $_;
  }
  while (/^\[::(ZONEITEM|STDITEM)/) {
    if (s/^(\[::(?:ZONEITEM)::\])//) {
      $model_DL -= log($model_occ{$1}/$model_occ{__ALL__});
      $DL{structure} -= log($model_occ{$1}/$model_occ{__ALL__});
      s/^(\[::ZONE=.*?::\])// || die; # name
      $model_DL -= log($model_occ{$1}/$model_occ{__ALL__});
      $DL{structure} -= log($model_occ{$1}/$model_occ{__ALL__});
      s/^(\[::ZONE=.*?::\])// || die; # source
      $model_DL -= log($model_occ{$1}/$model_occ{__ALL__});
      $DL{structure} -= log($model_occ{$1}/$model_occ{__ALL__});
    } elsif (s/^(\[::(?:STDITEM)::\])//) {
      $model_DL -= log($model_occ{$1}/$model_occ{__ALL__});
      $DL{structure} -= log($model_occ{$1}/$model_occ{__ALL__});
      while (s/^(\[::(?:MFSFEAT=.*?|MFS_(?:OR|[LR]B))::\])//) {
	$model_DL -= log($mfs_occ{$1}/$mfs_occ{__ALL__});
	$DL{structure} -= log($mfs_occ{$1}/$mfs_occ{__ALL__});
      }
      s/^(\[::MFSBNDRY::\])// || die;    
      $model_DL -= log($mfs_occ{$1}/$mfs_occ{__ALL__});
      $DL{structure} -= log($mfs_occ{$1}/$mfs_occ{__ALL__});
      while (s/^(\[::(?:MFSFEAT=.*?|MFS_(?:OR|[LR]B))::\])//) {
	$model_DL -= log($mfs_occ{$1}/$mfs_occ{__ALL__});
	$DL{structure} -= log($mfs_occ{$1}/$mfs_occ{__ALL__});
      }
    }
    if (s/^(\[::(?:OPERATION=.*?|APPEND|PREPEND)::\])//) {
      $model_DL -= log($operations_occ{$1}/$operations_occ{__ALL__});
      $DL{structure} -= log($operations_occ{$1}/$operations_occ{__ALL__});
      s/^(.*?#)// || die;
      my $args = $1;
      while ($args =~ s/^(.)//) {
	$model_DL -= log($regexp_char_occ{$1}/$regexp_char_occ{__ALL__});    
	$DL{structure} -= log($regexp_char_occ{$1}/$regexp_char_occ{__ALL__});    
      }
      while (!/^\[::VARIANT/ && s/^(\[\d*:[^\]]*:\]|\\.|.)//) {
	$model_DL -= log($regexp_char_occ{$1}/$regexp_char_occ{__ALL__});
	$DL{structure} -= log($regexp_char_occ{$1}/$regexp_char_occ{__ALL__});	
	$DL_add{rads} -= log($regexp_char_occ{$1}/$regexp_char_occ{__ALL__});	
      }
      if (s/^(\[::VARIANTS_END::\])//) {
	$model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
	$DL{structure} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	$DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
      } else {
	while (s/^(\[::VARIANT=.*?::\])//) {
	  $model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
	  $DL{structure} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	  $DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	}
	die unless (s/^(\[::VARIANTS_BNDRY::\])//);
	$model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
	$DL{structure} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	$DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	while (s/^(\[::VARIANT=.*?::\])//) {
	  $model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
	  $DL{structure} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	  $DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	}
	die unless (s/^(\[::VARIANTS_END::\])//);
	$model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
	$DL{structure} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	$DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
      }
    } elsif (s/^(\[::MULTIOPERATION=(\d+)::\])//) {
      $model_DL -= log($operations_occ{$1}/$operations_occ{__ALL__});
      $DL{structure} -= log($operations_occ{$1}/$operations_occ{__ALL__});
      for my $nop (1..$2) {
	if (s/^(\[::OPERATION=.*?::\])//) {
	  $model_DL -= log($operations_occ{$1}/$operations_occ{__ALL__});
	  $DL{structure} -= log($operations_occ{$1}/$operations_occ{__ALL__});
	  s/^(.*?#)// || die;
	  my $args = $1;
	  while ($args =~ s/^(.)//) {
	    $model_DL -= log($regexp_char_occ{$1}/$regexp_char_occ{__ALL__});    
	    $DL{structure} -= log($regexp_char_occ{$1}/$regexp_char_occ{__ALL__});    
	  }
	  while (!/^\[::(?:OPERATION|VARIANTS)/ && s/^(\[\d*:[^\]]*:\]|\\.|.)//) {
	    $model_DL -= log($regexp_char_occ{$1}/$regexp_char_occ{__ALL__});
	    $DL{structure} -= log($regexp_char_occ{$1}/$regexp_char_occ{__ALL__});	
	    $DL_add{rads} -= log($regexp_char_occ{$1}/$regexp_char_occ{__ALL__});	
	  }
	} else {
	  die "<$_>";
	}
      }
      if (s/^(\[::VARIANTS_END::\])//) {
	$model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
	$DL{structure} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	$DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
      } else {
	while (s/^(\[::VARIANT=.*?::\])//) {
	  $model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
	  $DL{structure} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	  $DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	}
	die unless (s/^(\[::VARIANTS_BNDRY::\])//);
	$model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
	$DL{structure} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	$DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	while (s/^(\[::VARIANT=.*?::\])//) {
	  $model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
	  $DL{structure} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	  $DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	}
	die unless (s/^(\[::VARIANTS_END::\])//);
	$model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
	$DL{structure} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	$DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
      }
    } elsif (s/^(\[::(?:IDENTITY)::\])//) {
      $model_DL -= log($operations_occ{$1}/$operations_occ{__ALL__});
      $DL{structure} -= log($operations_occ{$1}/$operations_occ{__ALL__});
      while (!/^\[::VARIANT/ && s/^(\[\d*:[^\]]*:\]|\\.|.)//) {
	$model_DL -= log($regexp_char_occ{$1}/$regexp_char_occ{__ALL__});
	$DL{structure} -= log($regexp_char_occ{$1}/$regexp_char_occ{__ALL__});	
	$DL_add{rads} -= log($regexp_char_occ{$1}/$regexp_char_occ{__ALL__});	
      }
      if (s/^(\[::VARIANTS_END::\])//) {
	$model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
	$DL{structure} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	$DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
      } else {
	while (s/^(\[::VARIANT=.*?::\])//) {
	  $model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
	  $DL{structure} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	  $DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	}
	die unless (s/^(\[::VARIANTS_BNDRY::\])//);
	$model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
	$DL{structure} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	$DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	while (s/^(\[::VARIANT=.*?::\])//) {
	  $model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
	  $DL{structure} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	  $DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	}
	die unless (s/^(\[::VARIANTS_END::\])//);
	$model_DL -= log($variants_occ{$1}/$variants_occ{__ALL__});
	$DL{structure} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
	$DL_add{variants} -= log($variants_occ{$1}/$variants_occ{__ALL__});	
      }
    } else {
      die;
    }
  }
  die $_ unless $_ eq "";
}
print STDERR "Model description length = $model_DL\n";
for (keys %DL) {
  print STDERR "  $_ = $DL{$_}\n";
}
print STDERR "  Other breakdowns:\n";
for (keys %DL_add) {
  print STDERR "    $_ = $DL_add{$_}\n";
}


print STDERR "TOTAL DESCRIPTION LENGTH = ".($model_DL+$lexicon_DL)."\n";

sub add_rads_and_vars {
  my $s = shift;
  my $submodelname = shift || die;
  my $tmp;
  if ($s !~ / (rads|except)="(.*?)"/) {
    $model{$submodelname} .= "#";
    $regexp_char_occ{"#"}++;
    $regexp_char_occ{__ALL__}++;
  } else {
    if ($s =~ / rads="(.*?)"/) {
      $model{$submodelname} .= translitterate($1);
      $tmp = translitterate($1);
      while ($tmp =~ s/^(\[:.*?:\]|\\.|.)//) {
	$regexp_char_occ{$1}++;
	$regexp_char_occ{__ALL__}++;
      }
    }
    $model{$submodelname} .= "§";
    $regexp_char_occ{"§"}++;
    $regexp_char_occ{__ALL__}++;
    if ($s =~ / except="(.*?)"/) {
      $model{$submodelname} .= translitterate($1);
      $tmp = translitterate($1);
      while ($tmp =~ s/^(\[:.*?:\]|\\.|.)//) {
	$regexp_char_occ{$1}++;
	$regexp_char_occ{__ALL__}++;
      }
    }
    $model{$submodelname} .= "#";
    $regexp_char_occ{"#"}++;
    $regexp_char_occ{__ALL__}++;
  }
  if ($s !~ / vars?(?:_?except)?="(.*?)"/) {
    $model{$submodelname} .= "\[::VARIANTS_END::\]";
    $variants_occ{"\[::VARIANTS_END::\]"}++;
    $variants_occ{__ALL__}++;
  } else {
    if ($s =~ / vars?="(.*?)"/) {
      $tmp = $1;
      for (split /\s*\|\s*/, $tmp) {
	$model{$submodelname} .= "\[::VARIANT=".$_."::\]";
	$variants_occ{"\[::VARIANT=".$_."::\]"}++;
	$variants_occ{__ALL__}++;
      }
    }
    $model{$submodelname} .= "\[::VARIANTS_BNDRY::\]";
    $variants_occ{"\[::VARIANTS_BNDRY::\]"}++;
    $variants_occ{__ALL__}++;
    if ($s =~ / vars?_?except="(.*?)"/) {
      $tmp = $1;
      for (split /\s*\|\s*/, $tmp) {
	$model{$submodelname} .= "\[::VARIANT=".$_."::\]";
	$variants_occ{"\[::VARIANT=".$_."::\]"}++;
	$variants_occ{__ALL__}++;
      }
    }
    $model{$submodelname} .= "\[::VARIANTS_END::\]";
    $variants_occ{"\[::VARIANTS_END::\]"}++;
    $variants_occ{__ALL__}++;
  }
}


sub add_ps {
  my $s = shift;
  my $submodelname = shift || die;
  my $tmp;
  if ($s =~ / partitionspace="(.*?)"/) {
    $tmp = $1;
    for (split /\s*\+\s*/, $tmp) {
      $model{$submodelname} .= "[::PSPACE=".$_."::]";
      $mfs_occ{"\[::PSPACE=".$_."::\]"}++;
      $mfs_occ{__ALL__}++;      
    }
  }
  $model{$submodelname} .= "[::MFSBNDRY::]";
  $mfs_occ{"\[::MFSBNDRY::\]"}++;
  $mfs_occ{__ALL__}++;
}

sub add_feats_old {
  my $s = shift;
  my $submodelname = shift || die;
  for my $f (split /\|/, $s) {
    for (split /\./, $f) {
      $model{$submodelname} .= "[::MFSFEAT=".$_."::]";
      $mfs_occ{"\[::MFSFEAT=".$_."::\]"}++;
      $mfs_occ{__ALL__}++;
    }
    $model{$submodelname} .= "[::NEXTMFS::]";
    $mfs_occ{"\[::NEXTMFS::\]"}++;
    $mfs_occ{__ALL__}++;
  }
  if ($model{$submodelname} =~ s/\[::NEXTMFS::\]$//) {
    $mfs_occ{"\[::NEXTMFS::\]"}--;
    $mfs_occ{__ALL__}--;
  }
  $model{$submodelname} .= "[::MFSBNDRY::]";
  $mfs_occ{"\[::MFSBNDRY::\]"}++;
  $mfs_occ{__ALL__}++;
}

sub add_feats {
  my $s = shift;
  my $submodelname = shift || die;
  my $add_mfs_bndry = shift || 0;
  while ($s ne "") {
    if ($s =~ s/^\|//) {
      $model{$submodelname} .= "[::MFS_OR::]";
      $mfs_occ{"\[::MFS_OR::\]"}++;
      $mfs_occ{__ALL__}++;
    } elsif ($s =~ s/^\(//) {
      $model{$submodelname} .= "[::MFS_LB::]";
      $mfs_occ{"\[::MFS_LB::\]"}++;
      $mfs_occ{__ALL__}++;
    } elsif ($s =~ s/^\)//) {
      $model{$submodelname} .= "[::MFS_RB::]";
      $mfs_occ{"\[::MFS_RB::\]"}++;
      $mfs_occ{__ALL__}++;
    } elsif ($s =~ s/^\.//) {
    } elsif ($s =~ s/^([^\.()\|]+)//) {
      $model{$submodelname} .= "[::MFSFEAT=".$_."::]";
      $mfs_occ{"\[::MFSFEAT=".$_."::\]"}++;
      $mfs_occ{__ALL__}++;
    } else {die $s}
  }
  if ($add_mfs_bndry) {
    $model{$submodelname} .= "[::MFSBNDRY::]";
    $mfs_occ{"\[::MFSBNDRY::\]"}++;
    $mfs_occ{__ALL__}++;
  }
}

sub translitterate {
  my $w=shift;
  for (keys %translitt) {
    $w =~ s/$_/$translitt{$_}/g;
  }
  return $w;
}
