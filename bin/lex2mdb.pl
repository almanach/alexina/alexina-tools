#!/usr/bin/env perl
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
#use re "debug";
use utf8;
use locale;
use DBI;
#use strict;
#use warnings;
use Encode;

my $db = shift || "db.dat";


if (-r $db) {
  print STDERR "  Erasing previous database $db\n";
  `rm $db`;
}

my $dbh = DBI->connect("dbi:SQLite:$db", "", "", {RaiseError => 1, AutoCommit => 0});
$dbh->do("CREATE TABLE data(wordform,wordform_nodia,lemma,tag,inflclass);");
my $sth=$dbh->prepare('INSERT INTO data(wordform,wordform_nodia,lemma,tag,inflclass) VALUES (?,?,?,?,?)');
my $l = 0;
print STDERR "  Loading data...";
my %already_inserted;
while (<>) {
  $l++;
  if ($l % 10000 == 0) {
    print STDERR "\r  Loading data...$l";
    $dbh->commit;
  }
  chomp;
  /^([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*)(?:\t([^\t]*))?$/ || next;
  my $wordform = $1;
  my $cat = $2;
  my $lemma = $3;
  my $ms = $4;
  my $inflclass = $5;
  $lemma =~ s/__.*$//;
  if ($db =~ /fr$/) {
    next if ($cat =~ /^cf/);
    $cat="adv" if ($cat eq "advm" or $cat eq "advp");
  }
  $cat = uc($cat);
  if (!defined($already_inserted{$cat}{$ms}{$inflclass}{$lemma}{$wordform})) {
    $sth->execute($wordform,remove_diacritics($wordform),$lemma,$cat."_".$ms,$inflclass);
    $already_inserted{$cat}{$ms}{$inflclass}{$lemma}{$wordform} = 1;
  }
}
print STDERR "\r  Loading data...$l\n";
$sth->finish;
$dbh->commit;
print STDERR "  Creating index...";
$dbh->do("CREATE INDEX wordform ON data(wordform);");
$dbh->do("CREATE INDEX wordform_nodia ON data(wordform_nodia);");
$dbh->do("CREATE INDEX lemma ON data(lemma);");
$dbh->do("CREATE INDEX tag ON data(tag);");
$dbh->do("CREATE INDEX lemma_tag ON data(lemma,tag);");
$dbh->commit;
print STDERR "done\n";
$dbh->disconnect;

sub remove_diacritics {
  my $s = shift;
  $s =~ s/[áàâäąãăå]/a/g;
  $s =~ s/[ćčç]/c/g;
  $s =~ s/[ďðđ]/d/g;
  $s =~ s/[éèêëęě]/e/g;
  $s =~ s/[ğ]/g/g;
  $s =~ s/[ìíîĩĭıï]/i/g;
  $s =~ s/[ĺľł]/l/g;
  $s =~ s/[ńñň]/n/g;
  $s =~ s/[òóôõöøő]/o/g;
  $s =~ s/[ŕř]/r/g;
  $s =~ s/[śšş]/s/g;
  $s =~ s/[ťţ]/t/g;
  $s =~ s/[ùúûũüǔű]/u/g;
  $s =~ s/[ỳýŷÿ]/y/g;
  $s =~ s/[źẑżž]/z/g;
  $s =~ s/[ÁÀÂÄĄÃĂÅ]/A/g;
  $s =~ s/[ĆČÇ]/C/g;
  $s =~ s/[ĎÐ]/D/g;
  $s =~ s/[ÉÈÊËĘĚ]/E/g;
  $s =~ s/[Ğ]/G/g;
  $s =~ s/[ÌÍÎĨĬİÏ]/I/g;
  $s =~ s/[ĹĽŁ]/L/g;
  $s =~ s/[ŃÑŇ]/N/g;
  $s =~ s/[ÒÓÔÕÖØŐ]/O/g;
  $s =~ s/[ŔŘ]/R/g;
  $s =~ s/[ŚŠŞ]/S/g;
  $s =~ s/[ŤŢ]/T/g;
  $s =~ s/[ÙÚÛŨÜǓŰ]/U/g;
  $s =~ s/[ỲÝŶŸ]/Y/g;
  $s =~ s/[ŹẐŻŽ]/Z/g;
  $s =~ s/Ĳ/IJ/g;
  return $s;
}
