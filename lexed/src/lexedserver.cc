//<head>//////////////////////////////////////////////////////////////
//
//	
//	Lexed version 3.1
//	Copyright (C) 1999 2000 2001 2002 2003, 2005.
//	Universit� Paris 7
//	INRIA Rocquencourt
//
//	Conception & programmation :
//	Lionel Cl�ment
//	e-mail : lionel.clement@linguist.jussieu.fr
//
//	vendredi 18 janvier 2002, 13h41
//
//
////////////////////////////////////////////////////////////////</head>


#include "lexedserver.h"

Server::Server(int port)
{
  this->port = htons(port);
  this->connectsock = -1;
  this->listensock = -1;
}

void
Server::start()
{
  
  /* This ignores the SIGPIPE signal.  This is usually a good idea, since
   the default behaviour is to terminate the application.  SIGPIPE is
   sent when you try to write to an unconnected socket.  You should
   check your return codes to make sure you catch this error! */
  struct sigaction sig;
  
  sig.sa_handler = SIG_IGN;
  sig.sa_flags = 0;
  sigemptyset(&sig.sa_mask);
  sigaction(SIGPIPE, &sig, NULL);
  
  struct sockaddr_in address;
  int listening_socket;
  int connected_socket = -1;
  int new_process;
  int reuse_addr = 1;
  
  /* Setup internet address information.  
     This is used with the bind() call */
  memset((char *) &address, 0, sizeof(address));
  address.sin_family = AF_INET;
  address.sin_port = this->port;
  address.sin_addr.s_addr = htonl(INADDR_ANY);
  
  listening_socket = socket(AF_INET, SOCK_STREAM, 0);
  if (listening_socket < 0) {
    perror("socket");
    exit(EXIT_FAILURE);
  }
  
  if (this->listensock != NULL) {
    this->listensock = listening_socket;
  }
  
  setsockopt(listening_socket, SOL_SOCKET, SO_REUSEADDR, (const char *) &reuse_addr, 
	     sizeof(reuse_addr));
  
  // setsockopt (listening_socket, SOL_SOCKET, SO_REUSEADDR, "1",
  //  sizeof (reuse_addr));
  
  if (bind(listening_socket, (struct sockaddr *) &address, 
	   sizeof(address)) < 0) {
    perror("bind");
    close(listening_socket);
    exit(EXIT_FAILURE);
  }
  
  /* Queue up to five connections before having them automatically rejected. */
  listen(listening_socket, 5);

  while(connected_socket < 0) {
    connected_socket = accept(listening_socket, NULL, NULL);
    if (connected_socket < 0) {
      /* Either a real error occured, or blocking was interrupted for
	 some reason.  Only abort execution if a real error occured. */
      if (errno != EINTR) {
	perror("accept");
	close(listening_socket);
	exit(EXIT_FAILURE);
      } else {
	continue;    /* don't fork - do the accept again */
      }
    }
    
    new_process = fork();
    if (new_process < 0) {
      perror("fork");
      close(connected_socket);
      connected_socket = -1;
    } else {
      /* We have a new process... */
      if (new_process == 0) {
	/* This is the new process. */

	/* Close our copy of this socket */
	close(listening_socket);

	if (this->listensock != NULL)
	  /* Closed in this process.  We are not responsible for it. */
	  this->listensock = -1;
      } else {
	/* This is the main loop.  Close copy of connected socket, and
	   continue loop. */
	close(connected_socket);
	connected_socket = -1;
      }
    }
  }
  this->connectsock = connected_socket;
  
  fputs ("CONNECTION OPEN\n", stderr);
}  

void
Server::stop()
{
  close(connectsock);
  fputs ("CONNECTION CLOSED\n", stderr);
}

/* This function writes a character string out to a socket.  It will 
   return -1 if the connection is closed while it is trying to write. */
int
Server::put_data(char* string)
{
  size_t bytes_sent = 0;
  int count = strlen(string);
  int this_write;
  
  while (bytes_sent < count) {
    do {
      this_write = write(this->connectsock, string, count - bytes_sent);
    } while ( (this_write < 0) && (errno == EINTR) );

    if (this_write <= 0) {
      return this_write;
    }
    bytes_sent += this_write;
    string += this_write;
  }
  return count;
}

/* This function reads from a socket, until it recieves a linefeed
   character.  It fills the buffer "str" up to the maximum size "count".
   This function will return -1 if the socket is closed during the read
   operation.
   Note that if a single line exceeds the length of count, the extra data
   will be read and discarded!  You have been warned. */
int
Server::get_data()
{
  int bytes_read;
  unsigned int count = 1020;
  unsigned int total_count = 0;
  char *current_position;
  char last_read = 1;
  
  current_position = this->buffer;
  
  while (last_read != 0) {
      bytes_read = read(this->connectsock, &last_read, 1);
      if (bytes_read <= 0) {
	  /* The other side may have closed unexpectedly */
	  return -1; /* Is this effective on other platforms than linux? */
	}
      if ( (total_count < count)) {
	  *current_position = last_read;
	  current_position++;
	  total_count++;
	}
    }
  
  if (count > 0) {
    *current_position = 0;
  }

  return total_count;
}

int
Server::get_port()
{
  return port;
}

char*
Server::get_buffer()
{
  return buffer;
}
