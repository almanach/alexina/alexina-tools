#!/usr/bin/env perl

binmode STDIN, ":utf8";
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";
use utf8;

my $lang = shift || "fra";

print "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
print "<lexicalresource>\n";
print "<lexicon>\n";
print "<feat att=\"language\" val=\"$lang\"/>\n";

while (<>) {
  chomp;
  next if (/^#.*/);
  next if (/^\s*$/);
  s/^([^\t]+)\t([^\t]*)\t([^\t]+)// || die $_;
  $lemma = $1;
  $inflclass = $2;
  $synt = $3;
  $inflclass = "(unknown)" if $inflclass eq "";
  s/^[\t#]+//;
  $remainder = $4;
  $remainder = join("", sort {$a cmp $b} split (/(?<=>)(?=<)/));
  if ($lemma =~  s/___(.*)$//) {
    $semid = $1;
  } else {
    $semid = "";
  }
  $lemma = "__COLON__" if $lemma eq ";";
  $synt =~ s/Lemma/$lemma/;
  $synt =~ /^([^;]*);([^;]*);([^;]*);([^;]*);([^;]*);([^;]*)$/ || die $synt;
  $subcat = $4;
  $sinfos = $5;
  $redistr = $6;
  print "<lexeme>";
  print "<id>${lemma}__".(++$lemma2maxid{$lemma})."</id>";
  print "<entry>$2</entry>";
  print "<lemma><feat att=\"writtenForm\" val=\"$lemma\"/></lemma>";
  print "<semid>$semid</semid>" unless $semid eq "";
  print "<feat att=\"partOfSpeech\" val=\"$3\"/>";
  print "<weight>$1</weight>";
  die $_ unless ($subcat =~ />$/ || $subcat eq "");
  $argid = "0";
  if ($subcat ne "") {
    print "<subcategorizationframe>";
    $subcat =~  s/^<(.*)>$/\1/ || die $subcat;
    for $arg (split /,/, $subcat) {
      next if $arg =~ /^arg\d$/;
      $arg =~ s/^(.*?):// || die "\"$arg\"";
      $sf = $1;
      print "<syntacticargument";
      if ($arg =~  s/^\((.*)\)$/\1/) {
	print " mandatory=\"0\"";
      } else {
	print " mandatory=\"1\"";
      }
      print " id=\"arg".($argid++)."\">";
      print "<feat att=\"syntacticfunction\" val=\"$sf\"/>";
      print "<feat att=\"syntacticConstituent\" val=\"$arg\"/>";
      print "</syntacticargument>";
    }
    print "</subcategorizationframe>";
  }
  if ($sinfos ne "") {
    print "<syntactic_properties>";
    for (split /,/, $sinfos) {
      print "<syntactic_property>$_</syntactic_property>";
    }
    print "</syntactic_properties>";
  }
  print "<redistributions>";
  for (split /,/, $redistr) {
    s/^\%//;
    print "<redistribution>$_</redistribution>";
  }
  print "</redistributions>";
  my $didsomething = 1;
  while ($didsomething) {
    $didsomething = 0;
    while ($remainder =~ s/(="[^"<>]*)</$1&lt;/g) {$didsomething = 1}
    while ($remainder =~ s/(="[^"<>]*)>/$1&gt;/g) {$didsomething = 1}
    while ($remainder =~ s/(="[^"<>]*)'/$1&apos;/g) {$didsomething = 1}
    while ($remainder =~ s/(="[^"<>]*)"(?![ \/])/$1&quot;/g) {$didsomething = 1}
    while ($remainder =~ s/(="[^"<>]*)"( \|\|)/$1&quot;$2/g) {$didsomething = 1}
    while ($remainder =~ s/(="[^"<>]*)&(?![a-z]+;)/$1&amp;/g) {$didsomething = 1}
  }
  $remainder =~ s/ src="/ resource="/g;
  $remainder =~ s/ id="/ subid="/g;
  print "$remainder";
  print "</lexeme>\n";
}
print "</lexicon>\n";
print "</lexicalresource>\n";
