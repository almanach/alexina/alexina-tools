#!/usr/bin/env perl
#perl ilex2edylex_data.pl > lefff.edylex_data

use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

$lang = "fr";

while (1) {
  $_ = shift;
  if (/^-l$/) {$lang = shift}
  elsif (/^$/) {last}
  else {die "Unknown option '$_'"}
}
require "morpho.$lang.check.pl";

print STDERR "Reading ilex files\n";
for $ilexfile (<*.ilex>) {
#  next unless $ilexfile =~ /^(adj|adv|interj|neg|conj|coord|det|nom|nomp|nompred|ponct|pref-suff|prep|pro|v_new)\.ilex$/;
#  next unless $ilexfile =~ /^(v*)\.ilex$/;
  print STDERR "  $ilexfile\n";
  open LEFFF, "<$ilexfile" || die "Could not open $ilexfile: $!";
  binmode LEFFF, ":utf8";
  while (<LEFFF>) {
    chomp;
    s/(^|[^\\])#.*//;
    next if (/^\s*$/);
    /^(.*?)\t(.*?)\t.*?;.*?;(.*?);/ || die $_;
    $lemma = $1;
    $inflclass = $2;
    $cat = $3;
    $cat =~ s/^adv.*/adv/;
    next if ($lemma =~ / /);
    $cat_suff_inflclass{$cat}{$lemma}{$inflclass}++ unless (length($lemma) == 0);
    while ($lemma =~ s/^.//) {
      $cat_suff_inflclass{$cat}{$lemma}{$inflclass}++ unless (length($lemma) == 0);
    }
  }
  close LEFFF;
}

print STDERR "Inflecting suffixes\n";
open PIPE, '| perl ./morpho.$lang.dir.pl -nosep | grep -v "<error" | perl -pe "s/^\*//; s/\t\*/\t/; s/^(.*?)\t(.*?)\t.*?\t(.*?)\t(.*)/\4\t\2\t\1\t\3/"' || die "Could not open pipe";
binmode PIPE, ":utf8";
for $cat (keys %cat_suff_inflclass) {
  for $lemma (keys %{$cat_suff_inflclass{$cat}}) {
    @array = sort {$cat_suff_inflclass{$cat}{$lemma}{$b} <=> $cat_suff_inflclass{$cat}{$lemma}{$a}} keys %{$cat_suff_inflclass{$cat}{$lemma}};
    $inflclass = $array[0];
    $inflclass2 = "";
    $next_will_not_be_ignored = 2;
    if (length($lemma) > 1) {
      $lemma =~ /^.(.*)$/;
      $lemma2 = $1;
      @array2 = sort {$cat_suff_inflclass{$cat}{$lemma2}{$b} <=> $cat_suff_inflclass{$cat}{$lemma2}{$a}} keys %{$cat_suff_inflclass{$cat}{$lemma2}};
      $next_will_not_be_ignored = 0 if ($cat_suff_inflclass{$cat}{$lemma2}{$array2[0]} < (64-length($lemma2)*30) || $cat_suff_inflclass{$cat}{$lemma2}{$array2[0]} <= 1);
      $inflclass2 = $array2[0] if (check($cat,$lemma2,$array2[0]));
    }
    print PIPE "*$lemma\t$inflclass\t$cat\t".$cat_suff_inflclass{$cat}{$lemma}{$inflclass}."\n" unless (($inflclass eq $inflclass2 && $next_will_not_be_ignored) || $cat_suff_inflclass{$cat}{$lemma}{$inflclass} < (64-length($lemma)*30) || $cat_suff_inflclass{$cat}{$lemma}{$inflclass} <= 1);
  }
}

sub check {
  my $c = shift;
  my $l = shift;
  my $i = shift;
  $l = translitterate($l);
  $array2[0] =~ /^([^:]*?(?:[-0-9][^:]*?)?)(:.*)?$/ || die $array2[0];
  my $cl = $1;
  my $vars = $3;
  my $toto = ();
  my @lemmas = lemmatize("*".translitterate($lemma2),$c,$cl,$vars,0,1,\$toto);
  return ($lemmas[0] !~ /\t<error/);
}
