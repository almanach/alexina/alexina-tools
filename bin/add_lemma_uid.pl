#!/usr/bin/env perl

while (<>) {
    chomp;
    /^([^\t]+)\t/;
    $lemma=$1; $seenlemma{$lemma}++;
    s/\t/___\t/ unless $lemma=~/___/;
    s/\t/__$seenlemma{$lemma}\t/;
    print "$_\n";
}
