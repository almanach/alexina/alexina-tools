//<head>//////////////////////////////////////////////////////////////
//
//	
//	Lexed version 3.1
//	Copyright (C) 1999 2000 2001 2002 2003, 2005.
//	Universit� Paris 7
//	INRIA Rocquencourt
//
//	Conception & programmation :
//	Lionel Cl�ment
//	e-mail : lionel.clement@linguist.jussieu.fr
//
//	vendredi 18 janvier 2002, 13h41
//
//
////////////////////////////////////////////////////////////////</head>////

#ifndef LEXEDSERVER_H
#define LEXEDSERVER_H

#include "lexedtypes.h"

#include <sys/types.h>
#include <sys/socket.h>

#include <signal.h>
#include <unistd.h>
#include <netinet/in.h>

class Server
{
  
private:
  int listensock;
  int connectsock;
  int port;
  char buffer[1024];

public:
  Server(int port);
  void start();
  void stop();
  int get_data();
  int get_port();
  char* get_buffer();
  int put_data(char *string);
};


#endif
