#!/usr/bin/env perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

$in_lemmatized_form = 0;

while (<>) {
    chomp;
    s/\r//g;
    if (/<lemmatizedForm>/) {
      $in_lemmatized_form = 1;
      $has_inflected_forms = 0;
      $lemma_mst = "";
    } elsif (/<\/lemmatizedForm>/) {
      $in_lemmatized_form = 0;
    } elsif (/<orthography>(.*?)</ && $in_lemmatized_form) {
      $lemma = $1;
    } elsif (/<grammaticalCategory.*?>(.*?)</ && $in_lemmatized_form) {
      $cat = $1;
    } elsif (/<grammatical.*?>(.*?)</ && $in_lemmatized_form) {
      $lemma_mst .= ":$1";
    } elsif (/<inflectedForm>/) {
      $mst = "";
    } elsif (/<orthography>(.*?)</) {
      $form = $1;
    } elsif (/<grammatical.*?>(.*?)</) {
      $mst .= ":".$1;
    } elsif (/<\/inflectedForm>/) {
      $both_mst = $lemma_mst.$mst;
      $both_mst =~ s/^://;
      print $form."\t".$lemma."\t".$cat."#".$both_mst."\n";
      $has_inflected_forms = 1;
    } elsif (/<\/lexicalEntry>/ && $has_inflected_forms == 0) {
      $lemma_mst =~ s/^://;
      print $lemma."\t".$lemma."\t".$cat."#".$lemma_mst."\n";
    }
}
