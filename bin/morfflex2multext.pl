#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";
binmode utf8;


while (<>) {
  $l++;
  print STDERR "$l\r" if $l % 1000 == 0;
  chomp;
  s/^([^\t]+\t[^\t]+\t)([^\t]+)\tSubPOS=(.*?)(\||$)/\1\2\3\t/;
  while (s/^([^\t]+\t[^\t]+\t[^\t]+\t[^\t]*?)\|/\1./) {}
  next if /Var=[2-46-9]/;
  s/Gen=Y/ma|mi/;
  s/Gen=Z/ma|mi|neu/;
  s/Gen=F/fem/;
  s/Gen=N/neu/;
  s/Gen=H/fem|neu/;
  s/Gen=I/mi/;
  s/Gen=M/ma/;
  s/Gen=Q/altg/;
  s/Gen=T/mi|fem/;
  s/PGe=F/pfem/;
  s/PGe=M/pma/;
  s/PGe=Z/pfem|pmi|pma|pneu/;
  s/PNu=P/ppl/;
  s/PNu=S/psg/;
  s/Num=S/sg/;
  s/Num=P/pl/;
  s/Num=D/du/;
  s/Num=W/altn/;
  s/Gra=/g/;
  s/Neg=N/neg/;
  s/Neg=A/pos/;
  s/Cas=1/nom/;
  s/Cas=2/gen/;
  s/Cas=3/dat/;
  s/Cas=4/acc/;
  s/Cas=5/voc/;
  s/Cas=6/loc/;
  s/Cas=7/ins/;
  s/Per=1/1/;
  s/Per=2/2/;
  s/Per=3/2/;
  s/Ten=F/fut/;
  s/Ten=R/pst/;
  s/Ten=P/prs/;
  s/Ten=H/pst|prs/;
  s/Gra=1/std/;
  s/Gra=2/comp/;
  s/Gra=3/sup/;
  s/Voi=A/act/;
  s/Voi=P/pass/;
  s/[A-Z][a-z][a-z]=X\.//g;
  s/\.[A-Z][a-z][a-z]=X//g;
  s/[A-Z][a-z][a-z]=X//g;
  s/Sem=[^\.]+\.//;
  s/\.Sem=[^\.]+//;
  s/Sem=[^\.]+//;
  s/Var=1\.//;
  s/\.Var=1+//;
  s/Var=1//;
  s/Var=/v/;
  print $_."\n";
}
