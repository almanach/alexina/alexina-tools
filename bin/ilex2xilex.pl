#!/usr/bin/env perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

my $all=0;
my $noopt=0;
my $noobl=0;
my $noatt=0;

while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-all$/i) {$all=1;}
    elsif (/^-noopt$/i) {$noopt=1;}
    elsif (/^-noobl$/i) {$noobl=1;}
    elsif (/^-noatt$/i) {$noatt=1;}
}

$linenumber=0;
while (<>) {
    if (++$linenumber % 1000 == 0) {print STDERR "Loading lexicon: line $linenumber\r";}
    chomp;
    if (/SeMoyen/) {
      s/(pred=\')([aeiouéè])/$1s\'$2/ || s/(pred=\')/$1se /;
      s/^([aeiouéè])/s\'$1/ || s/^/se /;
    }
    $orig=$_;
    if ((s/,\@Km?s?([ ,>\]])/$1/ && s/\]\t.*\%actif/\]/) || $all) {
	s/:\(([^\)]*)\)/:$1\|__NULL__/g;
	$cat=$1;
	s/>.*?;/>;/;
	s/\@pron,\@(impers|pers)/\@\1,\@pron/;
	s/;$/;\%actif/;

	if ($noobl) {
	  s/,(Obl2|Obl|Loc|Dloc):[^,>]*//g;
	}

	if ($noatt) {
	  s/,(Att):[^,>]*//g;
	}

#	# on supprime les macros qu'on ne sait pas traiter
#	s/\@Ca/\@êa/;
#	s/\@[^pêaNIi][^,>\] ]+//g;
#	s/\@êa/\@Ca/;
#	# on supprime aussi pour le moment les macros qui donnent l'auxiliaire
#	s/\@(être|avoir),//;
	s/\s*,\s*\@W//g;
	s/\s*;\s*$//;
	push(@lexicon,$_);
    }
    $_=$orig;
}


$linenumber=0;
$i=0;
while ($i<=$#lexicon) {
    @newentries=();
    push(@newentries,$lexicon[$i]);
    $finished=0;
    while (!$finished) {
	$finished=1;
	$j=0;
	while ($j<=$#newentries) {
	    if ($newentries[$j]=~/^(.*?;<[^\|]*:)([^,>]*\|[^,>]*)([,>][^\t]*)/) {
		$finished=0;
		$debut=$1;
		$milieu=$2;
		$fin=$3;
		@tmpentries=();
		for (split(/\|/,$milieu)) {
		    push(@tmpentries,$debut.$_.$fin);
		}
		splice(@newentries,$j,1,@tmpentries);
		$j+=$#newentries;
	    }
	    $j++;
	}
    }
    splice(@lexicon,$i,1,@newentries);
    $i+=$#newentries+1;
}
for (@lexicon) {
  if (!$noopt || $_!~/__NULL__/) {
    s/(?<=[<,])[^:]+:__NULL__//g;
    s/,,+/,/g;
    s/<,+/</;
    s/,+>/>/;
    s/,$//;
    s/^(.*)\@pron_possible(.*)$/\1\2\n\1\@pron\2/;
    /^(.*);(\%.*)$/;
    $content = $1;
    $redistr = $2;
    for (split (/,/, $redistr)) {
      print "$content;$_\n";
    }
  }
}
