#!/usr/bin/perl

binmode STDIN, ":utf8";
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";
binmode utf8;

$dir = shift || die "Please provide the location of the bunamo directory";

for $subdir (<$dir/*>) {
  for $file (<$subdir/*.xml>) {
    open FILE, "<$file" || die $!;
    binmode FILE, ":utf8";
    while (<FILE>) {
      chomp;
      if (/<(adjective|noun|nounPhrase|preposition|verb) /) {
	$longcat = $1;
	$longcat =~ /^(.)/;
	$cat = uc($1);
	if (/ default="(.+?)"/) {
	  $lemma = $1;
	} else {
	  die $_;
	}
      } elsif (/^ +<(?:tense|mood)Form default="([^"]+?)"([^\/]+)/) {
	$form = $1;
	$tags = $2;
	my $otags = "";
	if ($tags =~ /tense=\"Past(?:Cont)?\"/) {
	  $otags .= "|Tense=Past";
	} elsif ($tags =~ /tense=\"Pres(?:Cont)?\"/) {
	  $otags .= "|Tense=Pres";
	} elsif ($tags =~ /tense=\"Fut\"/) {
	  $otags .= "|Tense=Fut";
	} elsif ($tags =~ /tense=\"Cnd\"/) {
	  $otags .= "|Mood=Cond";
	} elsif ($tags =~ /mood=\"Subj\"/) {
	  $otags .= "|Mood=Sub";
	} elsif ($tags =~ /mood=\"Imper\"/) {
	  $otags .= "|Mood=Imp";
	}
	if ($tags =~ /tense=\"(?:Past|Pres)Cont\"/) {
	  $otags .= "|Aspect=Imp";
	}
	if ($tags =~ /person=\"pl([123])\"/i) {
	  $otags .= "|Number=Plur|Pers=$1";
	} elsif ($tags =~ /person=\"sg([123])\"/i) {
	  $otags .= "|Number=Sing|Pers=$1";
	}
	$otags =~ s/^\|//;
	print $form."\t".$lemma."\t".$cat."#".$otags."\n";
      } elsif (/^ +<verbalNoun default="(.+?)"/) {
	print $form."\t".$lemma."\t".$cat."#verbal_adj\n";
      } elsif (/^ +<verbalAdjective default="(.+?)"/) {
	print $form."\t".$lemma."\t".$cat."#verbal_noun\n";
      } elsif (/^ +<([^ ]+) default="(.+?)"/) {
	$tag = $1;
	$form = $2;
	my $otags="";
	if ($tag =~ /^sg([1-3])/) {
	  $otags = "Number=Sing|Pers=$1";
	  if ($tag =~ /(Masc|Fem)$/) {
	    $otags .= "|Gender=$1";
	  }
	} elsif ($tag =~ /^pl([1-3])/) {
	  $otags = "Number=Plur|Pers=$1";
	  if ($tag =~ /(Masc|Fem)$/) {
	    $otags .= "|Gender=$1";
	  }
	} elsif ($tag =~ /^sg(Nom|Gen|Voc|Dat)/) {
	  $otags = "Number=Sing|Case=$1";
	  if ($tag =~ /(Masc|Fem)$/) {
	    $otags .= "|Gender=$1";
	  }
	} elsif ($tag =~ /^pl(Nom|Gen|Voc|Dat)/) {
	  $otags = "Number=Plur|Case=$1";
	  if ($tag =~ /(Masc|Fem)$/) {
	    $otags .= "|Gender=$1";
	  }
	} else {
	  $otags = $tag;
	  $tag =~ s/([A-Z])/".".lc($1)/ge;
	}
	print $form."\t".$lemma."\t".$cat."#".$otags."\n";
      }
    }
    close FILE;
  }
}
